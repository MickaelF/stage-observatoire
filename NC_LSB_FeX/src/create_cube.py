#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## create_cube.py
#Usage : python create_cube.py filenames
#Description : Given n fits files, create one fits file with n hdu containing each images
#Author : Mickaël FERRERI
#Date : June 25, 2019

import os
import sys
from astropy.io import fits

n = len(sys.argv)
new_hdul = fits.HDUList()

#Reading the images
for i in range(1,n-1):
	hdulist = fits.open(sys.argv[i])
	hdu = hdulist[-1]
	new_hdul.append(hdu)

#Writting the cube
new_hdul.writeto(sys.argv[-1])
