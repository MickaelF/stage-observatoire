#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## trunc_file_name.py
#Usage : python trunc_file_name.py filename symbol
#Description : Return the string "filename", without all of the characters before the last "symbol", and without the five last characters,
#+ because the filename usually used end in ".fits"
#+ Exemple : python trunc_file_name.py ~/Work/Data/NGC0474/fits/NGC0474.l.g.Mg004.fits /  will return NGC0474.l.g.Mg004
#Author : Mickaël FERRERI
#Date : July 18, 2019

filename = sys.argv[1]
symbol = sys.argv[2]

i = len(filename)-1
while filename[i] != symbol[0] and i > 0:
	i-=1
print(filename[(i+1):-5])
