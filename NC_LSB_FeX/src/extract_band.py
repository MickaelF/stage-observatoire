#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## extract_band.py
#Usage : python extract_band.py filename
#Description : Extract the waveleght of a file name (format : dir/*.band.* )
#Note : band must be g, r, i or u
#Author : Mickaël FERRERI
#Date : July 11, 2019

import sys

def search_band(filename):
	i = 0
	l = [".g.",".r.",".i.",".u."]
	while filename[i:i+3] not in l and i < len(filename)-3:
		i+=1
	f = filename[i+1]
	return(f)

n = len(sys.argv)
for i in range(1,n):
	f = search_band(sys.argv[i])
	print(f)
