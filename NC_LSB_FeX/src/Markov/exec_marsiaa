#!/bin/bash

######################################
# exec_marsiaa
# Description : Execute marsiaa. Can be run alone, but it was made to be executed by loop_marsiaa 
# Usage : ./exec_marsiaa [-it current iteration number] -o outputname 
#+         -g galaxy name [-c to crop the images corresponding to the given galaxy] 
#+         [-size if -c is providen, gives the size of the croped image] 
#+         [-d path of the working directory]
# Author : Mickaël FERRERI 
# Date : July 17, 2019
######################################

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Default working directory is the current path
DIR=$CUR_DIR

if [ "$1" == "-h" ]; then
  	echo -e "Usage: `basename $0` [-it current iteration number] -o outputname -g galaxy name [-c to crop the images corresponding to the given galaxy] [-size if -c is providen, gives the size of the croped image] [-d path of the working directory]"
	echo -e "\033[34;1;4mParameters : \033[0m"
	echo -e "\033[34;1m-it   :\033[0m used in loop_marsiaa, gives the current iteration in the loop"
	echo -e "\033[34;1m-o    :\033[0m the name of the output file. Mandatory argument"
	echo -e "\033[34;1m-g    :\033[0m the name of the galaxy with which the user wants to work. Mandatory argument"
	echo -e "\033[34;1m-c    :\033[0m give this option if you want the script to compute the croped images of the galaxy. Croped images are mandatory, use this option if you didn't compute it before"
	echo -e "\033[34;1m-size :\033[0m if -c is providen, gives the size of the croped image"
	echo -e "\033[34;1m-band :\033[0m  give a particular band on which you want to do the computations"
	echo -e "\033[34;1m-d    :\033[0m path of the working directory, i.e. the directory which contain the Data and src files. Default : current directory"
  	exit 0
fi

outputnamegiven=false
galnamegiven=false
crop=false
directory=false
itergiven=false
bandgiven=false

# Testing the arguments given
while test $# -gt 0
do
    case "$1" in
        -o) outputnamegiven=true
	    outputname=$2
            ;;
	-g) galnamegiven=true
	    galname=$2
	    ;;
	-c) crop=true
	    ;;
	-d) directory=true
	    DIR=$2
	    ;;
	-it) itergiven=true
	     it=$2
	    ;;
	-size) size=$2
	    ;;
	-band) bandgiven=true
	       band=$2
	    ;;
        -*) echo -e "Execution of `basename $0` aborted : Bad option error"
	    echo -e "Bad option $1"
	    exit 1
            ;;
        *) 
            ;;
    esac
    shift
done

# Outputname and galname are mandatory arguments. The script will return an error and abort if these arguments are not given
if [ "$outputnamegiven" != true ] || [ "$galnamegiven" != true ] ; then
	echo -e "Execution of `basename $0` aborted : Bad usage error"
	echo -e "Usage: `basename $0` -o outputname -g galaxy name [-c to compute the croped image] [-d path of the working directory] [-band to give a precise band]"
  	exit 1
fi

# If the user wants to crop the images, it will compute them
# Croped images are mandatory for the execution of this script, but if they are already computed, there is no need to compute it again
if [ "$crop" == true ]; then
	ls $DIR/Data/$galname/tocrop | while read imgtocrop; do
		echo -e "Croping "$imgtocrop" ..."
		python $CUR_DIR/crop.py $DIR/Data/$galname/tocrop/$imgtocrop $size $size $galname $DIR
		if [ $? -ne 0 ]; then
			echo -e "Execution of `basename $0` aborted : Error while croping image "$imgtocrop
			exit 1
		else echo -e "Croping : Done"
		fi
	done
fi

# Reading the output and input files
i=0
while read file; do
f=$(basename $file)
if [ $itergiven == true ]; then
	# tabprev gives the names of the files computed in the previous iteration
	tabprev[$i]="$DIR/Results/$galname/marsiaa/$outputname""_step"$(($it-1))"_band"$i".fits"
	# tabnext gives the names of the files computed in this iteration
	tabnext[$i]="$DIR/Results/$galname/marsiaa/$outputname""_step"$it"_band"$i".fits"
fi

# array gives the names of the initial files
array[$i]="$DIR/Data/$galname/croped/$f"
i=$(($i+1))
done << EOF
`find $DIR/Data/$galname/croped/ -maxdepth 1 -type f`
EOF


if [ $? -ne 0 ]; then
	echo -e "Execution of `basename $0` aborted : Error in find $DIR/Data/$galname/croped/ -maxdepth 1 -type f"
	exit 1	
fi

#If a band was given
if [ $bandgiven == true ]; then
i=0
while read fband; do
if [ $band != $fband ]; then
	unset tabnext[$i]
	unset tabprev[$i]
	unset array[$i]
fi
i=$(($i+1))
done << EOF
`python $DIR/extract_band.py ${array[*]}`
EOF

fi

# Computation of the output name for segmentation map given by Marsiaa
if [ $itergiven == true ]; then
	outputname=$outputname"_step"$it	
fi

# Executing Marsiaa
cd  $CUR_DIR/marsiaa/
if [ $itergiven == true ] && [ $it != 1 ]; then
	# If the script is run in loop mod, and if it is not the first iteration
	./marsiaa -f ${tabprev[*]} -o $DIR/Results/$galname/marsiaa/$outputname
else
	# If the script is run alone or for the initialisation of the loop mod
	./marsiaa -f ${array[*]} -o $DIR/Results/$galname/marsiaa/$outputname
fi

if [ $? -ne 0 ]; then
	echo -e "Execution of `basename $0` aborted : Error in Marsiaa execution"
	exit 1	
fi

# In loop mod, compute the next input image for Marsiaa, which is identical to the previous, exept that the highest class is removed
if [ $itergiven == true ]; then
	echo -e "Computing highest class mask..."
	l=$(seq 1 ${#array[*]})
	for i in $l; do
		# We compute a new image for each wave length
		j=$(($i-1))
		if [ $bandgiven == true ]; then
			python $CUR_DIR/remove_highest_class.py $DIR/Results/$galname/marsiaa/$outputname"_map.fits" ${array[*]} ${tabprev[*]} ${tabnext[*]} $it
		else
			python $CUR_DIR/remove_highest_class.py $DIR/Results/$galname/marsiaa/$outputname"_map.fits" ${array[j]} ${tabprev[j]} ${tabnext[j]} $it
		fi
		if [ $? -ne 0 ]; then
			echo -e "Execution of `basename $0` aborted : Error in remove_highest_class.py execution"
			exit 1	
		fi
	done
	echo -e "Done"
fi




