/***************************************************************************
                          emchain.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "emchain.h"
#include "data.h"
#include "noisemodel.h"
#include "segmclass.h"
#include "image.h"
#include "math.h"
#include "gauss.h"

EMChain::EMChain(){
//	cout << "\n-------Constructeur EMChain\n";
	
	//Chain::Chain();
}
EMChain::~EMChain(){
//	cout << "\n-------Destructeur EMChain\n";
}
/** No descriptions */
/*void EMChain::visit(vector<Data *> & data, vector<SegmClass *> & classes){
	if (P_forward.empty() || P_backward.empty())
		init(data,classes);
	
	// calculs de alpga et beta (P forward et backward)
	Chain::visit(data,classes);
	cout << "fin Chain::visit\n";
	
	//Chain::getP_joint_aposteriori(classes);
	
	Chain::update_apriori();
	Chain::update_dataDriven(data,classes);
}
*/

/** No descriptions 
 * 
 * Comment :
 *	Becareful : order of calls of functions are important
 */
void EMChain::update_apriori(vector<Data *> & data, vector<SegmClass *> & classes){
	const unsigned int _Nclass = classes.size();
	const unsigned int _len = data[0]->length()-1; // on s'arrete avant la fin car on regarde le site suivant
//	const unsigned int _Ndata = data.size();
	
	cout << /*Start */"update apriori with EM\n";
	
	if (P_forward.empty())
		this->init(data,classes);
		
	// somme des proba conjointes a posteriori
	dMatrix Sjoint;
	for (unsigned int k=0 ; k<_Nclass ; k++)
		Sjoint.push_back(vector<double>(_Nclass,0.));
	
	// somme des proba marginales a posteriori
	vector<double> Smarginal(_Nclass,0.);
		
	// update des proba de transitions aij
	for (unsigned int i=0 ; i<_Nclass ; i++)
	{
		for (unsigned int pos=0 ; pos<_len ; pos++)
		{
			Smarginal[i]+=this->getP_marginal_aposteriori(i,pos);//(*P_forward[i])[pos] * (*P_backward[i])[pos];
		}
//		cout << Smarginal[i] << endl;
	}

	// somme des proba conjointes sur tous les pixels
	for (unsigned int pos=0 ; pos<_len ; pos++)
	{
		dMatrix _joint=getP_joint_aposteriori(data,classes,pos);
		for (unsigned int i=0 ; i<_Nclass ; i++)
		for (unsigned int j=0 ; j<_Nclass ; j++)
				Sjoint[i][j]+=_joint[i][j];		
	}
	
	
	// l'une apres l'autre sinon, on modifie les aij utilises pour les calculs
	for (unsigned int i=0 ; i<_Nclass ; i++)
	{
		//double S=0.;
		for (unsigned int j=0 ; j<_Nclass ; j++)
		{
			if (Smarginal[i]>eps)
				classes[i]->setA(j,Sjoint[i][j]/Smarginal[i]);
			else
			{
				cerr<<"\nEMChain::update_apriori : Somme marginal aposteriori <eps classe "<<i<<endl;
				classes[i]->setA(j,1./static_cast<float>(_Nclass));
			}
			//cout << "\nnouveau a["<<i<<"]["<<j<<"] = "<<classes[i]->getA(j)<<endl<<endl;
			//S+=classes[i]->getA(j);
		}
		//cout << "somme des A[" << i<<"][*] = " << S<<endl;
	}
	
	
	// update des proba a priori de chaque classe
	double S=0.;
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		classes[k]->setP(getP_marginal_aposteriori(k,0) /*(*P_forward[k])[0] * (*P_backward[k])[0]*/);
		S+=classes[k]->getP();
		//cout << "nouveau P"<<k<<" = "<<classes[k]->getP()<<endl;
	}

	// verification de l'int�grit� des proba que l'on vient de calculer
	if (fabs(1.0-S)>0.01)
	{
		cerr << "EMChain::update_apriori : Somme des propabilites a priori incorrecte : "<<endl;
		for (unsigned int k=0 ; k<_Nclass ; k++)
		{
			cerr << "classe : "<<k<<endl;
			cerr << "alpha = "<< (*P_forward[k])[0] << " * beta = "<< (*P_backward[k])[0] << endl;
		cerr << classes[k]->getP();
		}
		cerr << S <<endl;
		ERR("");
	}
	
//	cout << "End update apriori with EM\n";

}


