/***************************************************************************
                          semchain.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "semchain.h"
#include "data.h"
#include "noisemodel.h"
#include "image.h"
#include "segmclass.h"
#include "gauss.h"

SEMChain::SEMChain(){
//	cout << "\n-------Constructeur SEMCHAIN\n";

}
SEMChain::~SEMChain(){
}
/** No descriptions 
void SEMChain::visit(vector<Data *> & data, vector<SegmClass *> & classes){
	cout << "SEMChain::visit : nothing \n";
}
*/

/** No descriptions */
void SEMChain::update_apriori(vector < Data * > & data, vector < SegmClass * > & classes)
{
	const unsigned int _Nclass = classes.size();
	const unsigned int _len = data[0]->length()-1;
	
	cout << /*Start */"update apriori with SEM\n";
	if (P_forward.empty())
		this->init(data,classes);
	
	// somme des proba conjointes a posteriori, initialis�es � zero
	dMatrix Sjoint;
	for (unsigned int k=0 ; k<_Nclass ; k++)
		Sjoint.push_back(vector<double>(_Nclass,0.));
		
	// Simulation d'un tirage al�eatoire selon les probabilit�s marginales calcul�e pr�c�demment
	// N_STOCHASTIC tirages
	Image<unsigned int> *X;
	vector<Image<double> > alpha(_Nclass);
	for (unsigned int k=0 ; k<_Nclass ; k++)
		alpha[k].createData( data[0]->getwidth(),data[0]->getheight() );

	for(unsigned int pos=0 ; pos<_len+1 ; pos++)
		for(unsigned int k=0 ; k<_Nclass ; k++)
			alpha[k][pos]=0.;
	
	for (unsigned int n=0 ; n<N_STOCHASTIC ; n++)
	{
		X=Chain::simulX(data,classes);
		// + simulation des proba forward
		for(unsigned int pos=0 ; pos<_len ; pos++)
		{
			Sjoint[ (*X)[pos] ][ (*X)[pos+1] ]+=1./N_STOCHASTIC;
			alpha[(*X)[pos]][pos]+=1./N_STOCHASTIC;
		}
		// affecte les valeurs pour la derniere position pos=_len
		alpha[(*X)[_len]][_len]+=1./N_STOCHASTIC;

		delete X;
	}
	
	// update des proba de transitions aij
	for (unsigned int i=0 ; i<_Nclass ; i++)
	{
		// somme des proba marginales a posteriori
		double Smarginal=0.;
		for (unsigned int pos=0 ; pos<_len ; pos++)
		{
			//Astuce pour remettre � jour par la fonction d�terministe
			(*P_backward[i])[pos]=1.;
			(*P_forward[i])[pos]=alpha[i][pos];
			Smarginal+=alpha[i][pos];//this->P_marginal_aposteriori(i,pos);//(*P_forward[i])[pos] * (*P_backward[i])[pos]
		}

		for (unsigned int j=0 ; j<_Nclass ; j++)
		{
			if (Smarginal>eps)
				classes[i]->setA(j,Sjoint[i][j]/Smarginal);
			else
			{
				cerr<<"\nSEMChain::update_apriori : Somme marginal aposteriori <eps classe "<<i<<endl;
				classes[i]->setA(j,1./static_cast<float>(_Nclass));
			}
//			cout << "\nnouveau a["<<i<<"]["<<j<<"] = "<<classes[i]->getA(j)<<endl<<endl;
		}
	}
	
	
	// update des proba a priori de chaque classe
	double S=0.;
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		classes[k]->setP( this->getP_marginal_aposteriori(k,0) ); //(*P_forward[k])[0] * (*P_backward[k])[0]);
		S+=classes[k]->getP();
//		cout << "nouveau P"<<k<<" = "<<classes[k]->getP()<<endl;
	}

	// verification de l'int�grit� des proba que l'on vient de calculer
	if (fabs(1.0-S)>0.01)
	{
		cerr << "SEMChain::update_apriori : Somme des propabilites a priori incorrecte : "<<endl;
		for (unsigned int k=0 ; k<_Nclass ; k++)
		{
			cerr << "classe : "<<k<<endl;
			cerr << "alpha = "<< (*P_forward[k])[0] << " * beta = "<< (*P_backward[k])[0] <<"  =  "
			 << S <<endl;
		}
		ERR("");
	}
	
//	cout << "End update apriori with SEM\n";

}


/** Update Data-driven parameters */
/*void SEMChain::update_dataDriven(vector < Data * > & data , vector < SegmClass * > & classes){
	
	Chain::update_dataDriven(data,classes);

}
*/
