/***************************************************************************
                          markovmodel.cpp  -  description
                             -------------------
    begin                : lun ao� 19 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "data.h"
#include "markovmodel.h"
#include "segmclass.h"
#include "noisemodel.h"
#include "Math.h"

MarkovModel::MarkovModel(){
//	cout << "create Markov Model\n";
}
MarkovModel::~MarkovModel(){
	/*
	if (!MarkovModel::P_forward.empty())
		for (unsigned int k=0 ; k<MarkovModel::P_forward.size() ; k++)
			delete MarkovModel::P_forward[k];
		MarkovModel::P_forward.clear();
	
	if (!MarkovModel::P_backward.empty())
		for (unsigned int k=0 ; k<MarkovModel::P_backward.size() ; k++)
			delete MarkovModel::P_backward[k];
		MarkovModel::P_backward.clear();
		*/
}
/** Initialize and allocate memory for private parameters daughter classes */
//void MarkovModel::init(vector<Data *>&, vector<SegmClass *>&){
//	cout << "MarkovModel::init : Shouldn't be here!\n";
//}
/** No descriptions */
void MarkovModel::update_apriori(vector < Data * > & , vector < SegmClass * > & ){
}
/** Update Data-driven parameters */
void MarkovModel::update_dataDriven(const vector < Data * > & data, vector < SegmClass * > & classes, bool decorr)
{ 
	const unsigned int _Nclass = classes.size();
	const unsigned int _Ndata = data.size();

	if (decorr)
	{	
		// lance la decorrelation pour chaque classe
		for (unsigned int k=0 ; k<_Nclass ; k++)
		{
			NoiseModel * ptk = classes[k]->getNoiseModel();
			ptk->update_corr(data,getP_marginal_aposteriori(k));
			ptk->evalvarcov();
			ptk->evalvarcovInv();
			ptk->decorrelate();
			
			// calcul des donn�es dans l'espace d�corr�l�
			vector<Data*> vZ;
			// pour chaque image en entr�e
			for (unsigned int i=0 ; i<_Ndata ; i++)
			{
				Data * Z = data[i]->copy();
				for (unsigned int s=0 ; s<data[i]->length() ; s++)
				{
					Z->setValue(s,0.);
	  			for (unsigned int c2=0 ; c2<i+1 ; c2++)
  					Z->setValue(s, Z->getValue(s) + ptk->decorr[i][c2] * data[i]->getValue(s) );
  			}
     		vZ.push_back(Z);
       	Z->getmin();Z->getmax();
    	}
		
			// appelle ses fonctions de mise a jour des param de bruit
			ptk->update_dataDriven(vZ, getP_marginal_aposteriori(k) );
			for (unsigned int i=0 ; i<_Ndata ; i++)
				delete vZ[i];
		}	
		
	}
	else // calcul quand meme les moments 1 et 2 
	  {
	    for (unsigned int k=0 ; k<_Nclass ; k++)
	      {
		classes[k]->getNoiseModel()->update_corr(data, getP_marginal_aposteriori(k) );
		classes[k]->getNoiseModel()->update_dataDriven(data, getP_marginal_aposteriori(k) );
	      }
	  }
	
	/*
	const unsigned int _Nclass = classes.size();

	Data * marginal = data[0];
	
	// pour chaque classe appelle ses fonctions de mise a jour des param de bruit
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		classes[k]->getNoiseModel()->update_dataDriven(data, getP_marginal_aposteriori(k,pos,0) );
	}	
*/
}
/** browse forward and backward */
void MarkovModel::browse(const vector < Data * > & , const vector < SegmClass * > &) {
}
