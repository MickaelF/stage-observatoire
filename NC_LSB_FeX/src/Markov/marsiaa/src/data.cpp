/***************************************************************************
                          data.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "data.h"
#include <iostream>

//template <typename TType>class Image;
#include "image.h"
#include "pyramid.h"

Image<bool> * Data::mask=NULL;

Data::Data(){
}
Data::~Data(){
}

/** Verify if object is really an Image<double> object. */
bool Data::isImageDouble() {
	if (dynamic_cast<Image<double>*>(this)==0)
		return false;
	else
		return true;
}


/** Read property of unsigned int Nscale. */
const unsigned char Data::getNscale() const {
		return 1;
}

/** Show with an external application the Image. */
/*void Data::show(const string _app){
	cerr << "Data::show\n";
}
*/
