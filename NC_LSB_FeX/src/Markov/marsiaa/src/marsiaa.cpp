/***************************************************************************
 marsiaa.cpp  -  description
 -------------------
 begin                : ven ao� 16 2002
 email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/

#include "all.h"

Marsiaa::Marsiaa() {
	//	cout << "\n-------Constructeur Marsiaa\n";
	config = NULL;
	pt_estim = NULL;
	map = NULL;
}

Marsiaa::~Marsiaa() {
	//	cout << "\n-------Destructeur Marsiaa\n";
	if (!data.empty()) {
		for (unsigned int i = 0; i < data.size(); i++)
			delete data[i];
		data.clear();
	}

	if (!classes.empty()) {
		for (unsigned int i = 0; i < classes.size(); i++)
			delete classes[i];
		classes.clear();
	}

	if (map != NULL)
		delete map;

	if (pt_estim != NULL)
		delete pt_estim;
	if (Data::mask != NULL)
		delete Data::mask;

	if (IO::scan != NULL)
		delete IO::scan;
	IO::scan = NULL;
	//	cout << "------- Fin Destructeur Marsiaa\n\n";

}

/********** ACCES FUNCTIONS **********/

/** Read the pointer of MarkovModel estimator. */
MarkovModel * Marsiaa::getestim() {
	return pt_estim;
}

/** Read the pointer of a segmented class. */
SegmClass * Marsiaa::getclasses(const int _position) {
	return classes[_position];
}

/** get vector of all classes */
vector<SegmClass *>& Marsiaa::getclasses() {
	return this->classes;
}

/** get data */
vector<Data*>& Marsiaa::getdata() {
	return this->data;
}

/** Read the pointer of one Data. */
Data * Marsiaa::getdata(const int _position) {
	return data[_position];
}

/** get map as an Image in full resolution */
Image<unsigned char>* Marsiaa::getImageMap() {
	if (map->isImage()) {
		Image<unsigned char> * im = dynamic_cast<Image<unsigned char> *> (map);
		return im;
	} else if (map->isPyramid()) {
		Pyramid<unsigned char> * py = dynamic_cast<Pyramid<unsigned char> *> (map);
		return py->getbase();
	} else
		return NULL;
}

/** get map */
Data* Marsiaa::getmap() {
	return this->map;
}

/********** PUBLIC FUNCTIONS **********/

/** Run Segmentation with markovian model and estimation model */
void Marsiaa::run() {

	cout << "\n-------Start marsiaa loop\n";
	pt_estim->browse(data, classes);

	fflush(NULL);

	// Mise a jour des parametres a priori (phi_X)
	pt_estim->update_apriori(data, classes);

	fflush(NULL);

	// Mise a jour des parametres d'attache aux donnees (phi_Y)
	pt_estim->update_dataDriven(data, classes, static_cast<bool> (config->getDecorr()));

	fflush(NULL);

	//	display();

	//	Segmentation::MPM( map, pt_estim, data, classes); -> dans this->compareMaps

	/* Mise a jour des parametres du bruit dans un contexte decorrele */

	cout << "-------End marsiaa loop\n\n";

}

/********** PRIVATE FUNCTIONS **********/

/** Create pointer on Markovian model used : EM/SEM/ICE with Chain/QuadTree */
MarkovModel * Marsiaa::createMarkovModel(const short _markov, const short _estim) {
	MarkovModel *sortie = NULL;
	//	cout << "MakovModel : " << _markov << endl;

	switch (_estim) {
	case EM:
		//    cout << "Estimation EM\n";
		if (_markov == CHAIN)
			sortie = new EMChain;
		else if (_markov == QUADTREE)
			sortie = new EMQuadTree;
		break;
	case SEM:
		//    cout << "Estimation SEM\n";
		if (_markov == CHAIN)
			sortie = new SEMChain;
		else if (_markov == QUADTREE)
			sortie = new SEMQuadTree;
		break;
	case ICE:
		//    cout << "Estimation ICE\n";
		if (_markov == CHAIN)
			sortie = new ICEChain;
		else if (_markov == QUADTREE)
			sortie = new ICEQuadTree;
		break;
	default:
		ERR("Probleme d'initialisation du model markovien\n")
		;
	}

	return sortie;
}

/** Create array of classes of segmentation. */
void Marsiaa::createClasses(const int _Nclass) {
	if (!data.empty()) {
		// si le nombre de classes est <min alors on utilise la method appropri�e
		if (_Nclass < MIN_NB_CLASS) {
			switch (_Nclass) {
			case AUTO_NB_CLASS: {
				int newNclass = Init::bestNclass(data);
				for (int i = 0; i < newNclass; i++)
					classes.push_back(new SegmClass);
				break;
			}
			default:
				ERR("Marsiaa::createClasses : value of number of classes not correct\n")
				;
			}
		} else
			for (int i = 0; i < _Nclass; i++)
				classes.push_back(new SegmClass);
	}
else		ERR("Marsiaa::createClasses : No data to choose best number of classes\n");

	}

	/** Create pointer on Data array as Images an initialise mask as true */
void Marsiaa::createData(const vector<string> & _files) {
	this->Ndata = _files.size();
	SegmClass::setNdata(Ndata);
	for (int i = 0; i < Ndata; i++) {
		cout << "\nReading input file " << _files[i] << endl;
		// pour l'instant on ne sait lire que des images
		data.push_back(new Image<double> );

		data[i]->load(_files[i]);
	}

	// initialise mask as true
	Image<bool> maskTmp(data[0]->getwidth(), data[0]->getheight());
	for (unsigned int i = 0; i < maskTmp.length(); i++)
		maskTmp[i] = true;
	Data::mask = new Image<bool> (&maskTmp);

}

/** Run data driven Initialization */
void Marsiaa::runInit() {
	cout << "\n***** Initialisation\n\n";

	if (this->Nclass < 0) {
		ERR("Marsiaa::runInit : Nclass must be initilized\n");
	}

	this->pt_estim->init(data, classes);

	cout << "-------NoiseModel : ";
	switch (config->getInit()) {
	case KMEAN:
		cout << "Kmeans\n";
		// evaluate gaussian parameters by the kmean method
		Init::kmean(data, classes);
		break;
	case CONDI:
		cout << "As simple as image\n";
		// give values of total image for parameters
		Init::condi(data, classes);
		break;
	case SKMEAN:
		cout << "Specific\n";
		Init::preset1(data, classes);
		break;
	default:
		ERR("Marsiaa::runInit : Bad value for data driven initialisation\n")
		;
	}

	cout << "-------Markovian Model(apriori)\n";
	Init::apriori(this->classes);

	// si on a une chaine, on reorganise les pixels de l'image suivant le parcourt de Hilbert Peano
	if (config->getMarkovModel() == CHAIN)
		IO::setscan(Init::HilbertPeano(config->getScan(), data));

	for (int k = 0; k < Nclass; k++)
		PPost.push_back(classes[k]->getP());

	//display();

}

/** Create Noise Model for the segmentation */
NoiseModel * Marsiaa::createNoiseModel(const int _flag) {
	switch (_flag) {
	case GAUSS:
		return (new Gauss());
	case GENGAUSS:
		return (new GenGauss());
	case WEIBULL:
		return (new Weibull());
	case LOGNORMAL:
		return (new LogNormal());
		//		case NOISE_AUTO : return Init::bestNoiseModel(*classes[k]);
	default:
		ERR("Bad value for noise model\n")
		;
	}
}

/** Replace an old Noise Model by a new Noise Model */
NoiseModel * Marsiaa::eraseNoiseModel(const int _flag, NoiseModel * old) {
	NoiseModel * tmp = NULL;
	switch (_flag) {
	case GAUSS:
		tmp = new Gauss(*old);
		break;
	case GENGAUSS:
		tmp = new GenGauss(*old);
		break;
	case WEIBULL:
		tmp = new Weibull(*old);
		break;
	case LOGNORMAL:
		tmp = new LogNormal(*old);
		break;
	default:
		ERR("Bad value for noise model\n")
		;
	}

	return tmp;
}

/** Read property of int Ndata. */
const int& Marsiaa::getNdata() {
	return Ndata;
}

/** Write property of int Ndata. */
void Marsiaa::setNdata(const int& _newVal) {
	SegmClass::setNdata(_newVal);
	Ndata = _newVal;
}

/** setConfig : create all objects necessary switch parameters 
 * @param _in : reference on Config object.
 * 
 * @return
 * 	true if no problem,
 *	false otherwise
 *
 * @remarks :
 *	run initialisation
 */
bool Marsiaa::setConfig(Config & _in, const string & maskfile) {
	this->config = &_in;
	this->createData(_in.getInputFiles());
	pt_estim = this->createMarkovModel(_in.getMarkovModel(), _in.getEstim());
	this->Nclass = _in.getNclass();
	this->createClasses(Nclass);

	// Loading mask is required
	if (!maskfile.empty()) {
		if (config->getMarkovModel() != CHAIN)
			setMask(maskfile);
		else
			cerr << "Warning : ignoring option mask, because of CHAIN option is incompatible\n";
	}

	// Checks dynamic range for each input data
	for (int i = 0; i < Ndata; i++)
		// verifie la dynamique de l'image
		data[i]->checkDynamicRange();

	// visualisation des images d'entree
	if (_in.getVisuInputig()) {
		cout << "Nb fichiers : " << data.size() << endl;
		for (unsigned int i = 0; i < data.size(); i++)
			this->getdata(i)->show();
	}

	// create map with data like Markov Model
	// allocate map with the same size of data
	if (_in.getMarkovModel() == CHAIN)
		map = new Image<unsigned char> (data[0]);
	else if (_in.getMarkovModel() == QUADTREE)
		map = new Pyramid<unsigned char> (data[0]);

	// RUN INITIALISATIONS
	this->runInit();

	// create (or change if necessary) noise model of classes
	// choix automatique du nombre de classes
	if (_in.getNoiseModel() == AUTO_NOISE) {
		/* On garde les bruits obtenus a l'initialisation */
		//		for (unsigned char k=0 ; k<Nclass ; k++)
		//			classes[k]->setNoiseModel(Init::bestNoiseModel(classes));
	} else // 1 lognormale + gaussiennes
	if (_in.getNoiseModel() == LOGG) {
		vector<short> noise(2, LOGNORMAL);
		noise[1] = GAUSS;
		for (unsigned char k = 0; k < Nclass; k++) {
			// s'il y avait d�j� un model de bruit
			if (classes[k]->getNoiseModel() != NULL) {
				classes[k]->setNoiseModel(this->eraseNoiseModel(noise[static_cast<short> (k != 0)], classes[k]->getNoiseModel()));
			} else
				classes[k]->setNoiseModel(this->createNoiseModel(noise[static_cast<short> (k != 0)]));
		}
	} else {
		for (unsigned char k = 0; k < Nclass; k++) {
			// s'il y avait d�j� un model de bruit
			if (classes[k]->getNoiseModel() != NULL) {
				// on cr��e le nouvel objet  partir de l'ancien
				NoiseModel * tmp = this->eraseNoiseModel(_in.getNoiseModel(), classes[k]->getNoiseModel());
				// on affecte le nouvel objet avec les anciennes valeurs
				classes[k]->setNoiseModel(tmp);
			} else
				classes[k]->setNoiseModel(this->createNoiseModel(_in.getNoiseModel()));
		}
	}

	//	display();

	// specify decorrelation method to use
	setdecorr(_in.getDecorr());

	// VERIFY COMBINATION OF OPTIONS
	// Pas EM, si GenGauss, Weibull, Lognormal
	if (_in.getEstim() == EM && (_in.getNoiseModel() == GENGAUSS || _in.getNoiseModel() == WEIBULL || _in.getNoiseModel() == LOGNORMAL))
		ERR("Error : Estimation EM can not be used with this noise model.\n");

	// visu
	if (_in.getVisuDecorr() && _in.getDecorr() == NODECORR) {
		cout << "Warning : Do not visualize decorrelated images because of no decorrelation.\n";
		_in.eraseVisu("DECORR");
	}
	if (_in.getVisuApp().empty()) {
		//		_in.setVisu("");
	}

	return true;
}

/** Insert a mask on images.
 * @param file : reference on  file name.
 *
 * @return
 *
 * @remarks {
 *		If file not found, exit programm by Image::load
 *		If configuration do not accept a mask, exit programm
 *		Description of error is send to stderr.
 *	}
 */
void Marsiaa::setMask(const string & file) {
	// chargement d'un masque dans une Image temporaire en double
	Image<double> maskTmp;
	cout << "Reading mask file " << file << "\n";
	maskTmp.load(file);

	// cast l'image du masque en bool
	Data::mask = new Image<bool> (&maskTmp);
}

/** use a Marginal a Posteriori Mode */
void Marsiaa::segmMPM(const bool reinit) {
	//cout << "PPost = " << PPost[0] << " " << PPost[1] << endl;
	if (reinit)
		pt_estim->init_aposteriori(data, classes);

	PPost = Segmentation::MPM(map, pt_estim, data, classes);

	// modifie la map avec les valeurs des classes une fois reorganisees
	const unsigned int _size = map->length();
	vector<unsigned short> Sort(sort());
	for (unsigned int s = 0; s < _size; s++) {
		const unsigned short k = static_cast<unsigned short> (map->getValue(s));
		map->setValue(s, static_cast<double> (Sort[k]));
	}

}

/** Set the decorrelation function choosed to each class 
 *
 * @pre
 *		You must have created Marsiaa::classes an their NoiseModel before
 */

void Marsiaa::setdecorr(const unsigned short flag) {
	switch (flag) {
	case CHOLESKY:
		for (unsigned char k = 0; k < Nclass; k++)
			classes[k]->getNoiseModel()->ptf_decorr = &NoiseModel::cholesky;
		break;
	case ICA:
		for (unsigned char k = 0; k < Nclass; k++)
			classes[k]->getNoiseModel()->ptf_decorr = &NoiseModel::ica;
		break;
	case NODECORR:
		for (unsigned char k = 0; k < Nclass; k++)
			classes[k]->getNoiseModel()->ptf_decorr = NULL;
		break;
	default:
		ERR("Marsiaa::setdecorr this flag "<<flag<<" is unknown\n")
		;
	}
}

inline void Marsiaa::swap(int x, int y) {
	SegmClass * tmp = classes[x];
	classes[x] = classes[y];
	classes[y] = tmp;
}

/** sort */
vector<unsigned short> Marsiaa::sort() {
	// reorganiser les classes suivants l'ordre des moments d'ordre 1

	vector<unsigned short> Sort(Nclass, 0);

	for (int k = 0; k < Nclass; k++)
		Sort[k] = k;

	// Calcul des normes des vecteurs pour chaque classe
	vector<double> Normes(Nclass, 0.);
	double min, max = 0.;
	for (int k = 0; k < Nclass; k++) {
		for (int cpt_capt = 0; cpt_capt < Ndata; cpt_capt++)
			Normes[k] += pow(classes[k]->getNoiseModel()->getm1(cpt_capt), 2.);
		if (Normes[k] > max)
			max = Normes[k];
	}

	unsigned short classMin = 0;
	for (int k = 0; k < Nclass; k++) {
		min = max + 1;
		for (int ki = 0; ki < Nclass; ki++)
			if (Normes[ki] < min) {
				min = Normes[ki];
				classMin = ki;
			}
		Sort[classMin] = k;

		Normes[classMin] = max + 1;
	}

	// recopie des parametres
	vector<SegmClass*> ancien(classes);
	for (int k = 0; k < Nclass; k++) {
		classes[Sort[k]] = ancien[k];

		vector<double> A(ancien[k]->getA());
		for (int l = 0; l < Nclass; l++) {
			classes[Sort[k]]->setA(Sort[l], A[l]);

		}
	}

	return Sort;
}

/** initComparison */
void Marsiaa::initComparison() {
	//	_oldthis = *this;
}

/** compareMaps
 */
//double Marsiaa::compareMaps(const Data * _oldMap, const Data* _newMap){
double Marsiaa::compareMaps() {
	// comparaison entre 2 maps
	const unsigned int _size = map->getwidth() * map->getheight();
	const Data * _oldMap = map->copy();
	PPost = Segmentation::MPM(map, pt_estim, data, classes);

	// modifie la map avec les valeurs des classes une fois reorganisees
	/*	vector<unsigned short> Sort(sort());
	 for (unsigned int s=0 ; s<_size ; s++)
	 {
	 const unsigned short k =	static_cast<unsigned short>(map->getValue(s));
	 map->setValue(s,static_cast<double>(Sort[k]));
	 }
	 */
	// somme des sites n'ayant pas la meme classe entre les 2 iterations
	double sum = 0.;
	Image<bool> *mask = Data::mask;
	unsigned int total = 0;
	for (unsigned int s = 0; s < _size; s++) {
		if ((*mask)[s]) {
			sum += static_cast<int> (_oldMap->getValue(s) != map->getValue(s));
			total++;
		}

	}

	delete _oldMap;
	return (sum * 100. / static_cast<float> (_size));

}

/** Evaluate pourcentage of distance between last vector of Moment and new vector of Moment */
double Marsiaa::compareClasses(const unsigned int k, vector<dMatrix> &last) {
	double pc = 0.;
	//	for (int k=0 ; k<Nclass ; k++)
	{
		for (int i = 0; i < Ndata; i++) {
			// Moment 1
			pc += 100. * fabs(classes[k]->getNoiseModel()->getm1(i) - last[k][0][i]) / fabs(data[i]->getmax() - data[i]->getmin());
			last[k][0][i] = classes[k]->getNoiseModel()->getm1(i);

			// comparaison a mi hauteur de la courbe =sqrt(m2*2*ln(2)) 
			pc += 100. * fabs(sqrt(classes[k]->getNoiseModel()->getm2(i) * 2. * log(2.)) - sqrt(last[k][1][i] * 2. * log(2.))) / fabs(data[i]->getmax()
					- data[i]->getmin());
			last[k][1][i] = classes[k]->getNoiseModel()->getm2(i);
		}
	}
	return pc;
}

/** display on standard output all statistical parameters */
void Marsiaa::display() {
	for (int k = 0; k < Nclass; k++) {
		cout << "*** class " << k << " P=" << /*classes[k]->getP()*/PPost[k] << " ***\n";
		for (int i = 0; i < Ndata; i++) {
			cout << "\t" << data[i]->filename << "\tp1 = " << classes[k]->getNoiseModel()->getParam(1)[i] << "\tp2 = "
					<< classes[k]->getNoiseModel()->getParam(2)[i] << "\tp3 = " << classes[k]->getNoiseModel()->getParam(3)[i] << endl;
		}
	}
}

// On reshifte/rescale les donn�es
void Marsiaa::unshift_unscale() {
	unsigned int i, j;
	bool update_min_max;

	// On shift/scale les bandes une par une si besoin est
	for (i = 0; i < (unsigned int) Ndata; i++) {
		update_min_max = false;

		// Donn�es shift�es?
		if (data[i]->is_shifted()) {
			// on reshifte
			for (j = 0; j < data[i]->length(); j++)
				data[i]->setDouble(j, data[i]->getValue(j) - data[i]->get_data_shift());
			update_min_max = true;
		}
		// Donn�es scal�es?
		if (data[i]->is_scaled()) {
			// on rescale
			for (j = 0; j < data[i]->length(); j++)
				data[i]->setDouble(j, ((double) data[i]->getValue(j)) / ((double) data[i]->get_data_scale()));
			update_min_max = true;
		}

		// Mise � jour valeurs min et max si besoin
		if (update_min_max) {
			data[i]->update_max();
			data[i]->update_min();
		}
	}
}

// Met � jour l'attache aux donn�es
void Marsiaa::update_dataDriven() {
	pt_estim->update_dataDriven(data, classes, static_cast<bool> (config->getDecorr()));
}

/** save STAT FILE
 *	Format :
 * 	IMAGES name1 name2 ...
 *		CLASS0 proba 2caracters_to_identify_noise Im1param1 Im1param2 Im1param3 Im2param1 ...
 */
void Marsiaa::save(const string _filename) {
	ofstream file(_filename.c_str(), ios::out);
	if (!file)
		ERR("Erreur de creation du fichier : "<<_filename<<endl);

	// nombre de lignes et colonnes
	file << Nclass << " " << 2 + Nclass + 3 * Ndata << " ";

	for (int i = 0; i < Ndata; i++)
		file << data[i]->filename << " ";

	for (int k = 0; k < Nclass; k++) {
		file << endl;
		file << "CLASS" << k << " " << PPost[k] << " ";

		for (int l = 0; l < Nclass; l++)
			file << classes[k]->getA(l) << " ";

		if (classes[k]->getNoiseModel()->isGauss())
			file << "GA ";
		else if (classes[k]->getNoiseModel()->isGenGauss())
			file << "GG ";
		else if (classes[k]->getNoiseModel()->isWeibull())
			file << "WE ";
		else if (classes[k]->getNoiseModel()->isLogNormal())
			file << "LN ";
		else
			file << "?? ";

		for (int i = 0; i < Ndata; i++) {
			// affiche 3 param du bruit
			for (unsigned short p = 1; p <= 3; p++) {
				file << classes[k]->getNoiseModel()->getParam(p)[i] << " ";
			}
		}

	}
	file.close();
}

/** trace */
void Marsiaa::trace(const string _filename) {

	// si le flux de sortie trace n'existe pas, on le cree
	ofstream trace(_filename.c_str(), ios::out | ios::app);
	if (!trace)
		ERR("Erreur d'ouverture du fichier : "<<_filename<<endl);

	// affiche 3 param du bruit
	for (unsigned short p = 1; p <= 3; p++) {
		trace << "| ";
		for (int i = 0; i < Ndata; i++) {
			trace << data[i]->filename << " ";
			for (int k = 0; k < Nclass; k++)
				trace << classes[k]->getNoiseModel()->getParam(p)[i] << " ";
		}
		trace << "| ";
	}

	trace << "proba_aposteriori ";
	for (int k = 0; k < Nclass; k++)
		trace << PPost[k] << " ";

	trace << "proba_transition ";
	for (int k = 0; k < Nclass; k++)
		for (int l = 0; l < Nclass; l++)
			trace << classes[k]->getA(l) << " ";

	trace << endl;
	trace.close();
}

/** traceInit : initialize trace file (erase if allready exists) */
void Marsiaa::traceInit(const string _filename) {
	ofstream trace(_filename.c_str(), ios::out);
	if (!trace)
		ERR("Erreur de creation du fichier : "<< _filename <<endl);

	// 	trace << "creation du fichier\n";

	// proba d'attache aux donnees
	for (unsigned short p = 1; p <= 3; p++) {
		trace << "Param" << p << " ";
		for (int i = 0; i < Ndata; i++) {
			trace << data[i]->filename << " ";
			for (int k = 0; k < Nclass; k++)
				trace << "CL" << k << " ";
			//			trace << " ; ";
		}
		trace << " | ";
	}

	// proba aposteriori du model
	trace << "proba_aposteriori ";
	for (int k = 0; k < Nclass; k++)
		trace << "CL" << k << " ";

	trace << "proba_transition ";
	for (int k = 0; k < Nclass; k++)
		for (int l = 0; l < Nclass; l++)
			trace << k << "->" << l << " ";

	trace << endl;
	trace.close();
}

/** saveDecorr
 * @pre : have a map before
 */
Image<double>* Marsiaa::getDecorr(const unsigned int i) {

	const unsigned int _w = map->getwidth();
	const unsigned int _h = map->getheight();
	const unsigned int _len = _w * _h;

	Image<double> * Z = new Image<double> (_w, _h);

	// pour tous les sites de la carte de segmentation
	for (int s = _len - 1; s >= 0; s--) {
		// recupere la classe
		unsigned short c = static_cast<unsigned short> (map->getValue(s));
		(*Z)[s] = 0.;
		// calcul des donn�es dans l'espace d�corr�l�
		for (unsigned int c2 = 0; c2 < i + 1; c2++)
			(*Z)[s] += classes[c]->getNoiseModel()->decorr[i][c2] * data[i]->getValue(s);
	}
	return Z;
}
