/***************************************************************************
                          segmclass.cpp  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "segmclass.h"
#include "noisemodel.h"

int SegmClass::Ndata=0;
int SegmClass::Nclass=0;

/*SegmClass::SegmClass(const int size)
{
	noise=NULL;
	SegmClass::setNdata(size);	
	SegmClass::SegmClass();
}*/

SegmClass::SegmClass()
{
//	cout << "\n-------Constructeur SegmClass\n";
	noise=NULL;
	if (Ndata>0)
	{
		NoiseModel::setNdata(Ndata);
		SegmClass::Nclass++;
	}
	else
		ERR("SegmClass::SegmClass : Can't create SegmClass object without size of data.\n");
}

SegmClass::~SegmClass(){
	delete noise;
	noise=NULL;
//	cout << "\n-------Destructructeur SegmClass\n";
}


/** Write property of double P. */
void SegmClass::setP( const double _newVal ){
	P = _newVal;
}
/** Read property of double P */
const double SegmClass::getP() const{
	return P;
}

/** Read property of vector<double> A. */
const double SegmClass::getA(const int _i){
	return A[_i];
}

/** Read property vector<double> A */
vector<double> SegmClass::getA(){
	return A;
}

/** Write property of vector<double> A. */
void SegmClass::setA(const int _i, const double _newVal){
	A[_i] = _newVal;
}

///** Read property of one mean. */
//const double SegmClass::getmean(const int _capteur) const
//{
//	return mean[_capteur];
//}
///** Write property of one mean. */
//void SegmClass::setmean(const double _newVal, const int _capteur){
//	mean[_capteur] = _newVal;
//}
///** Read property of double stddev. */
///*double SegmClass::getstddev() const{
//	return stddev;
//}
//*/
///** Read property of double stddev. */
//const double SegmClass::getstddev(const int _capteur) const{
//	return stddev[_capteur];
//}
///** Write property of double stddev. */
//void SegmClass::setstddev(const double _newVal, const int _capteur){
//	stddev[_capteur] = _newVal;
//}
//
//
///** Write property of the determinant. */
//void SegmClass::setdet( const double _newVal)
//{
//	det = _newVal;
//}
//
///** Read property of the determinant */
//double SegmClass::getdet() const
//{
//	return det;
//}
//
//
///** Write/Read property of covariance between 2 entries. */
//void SegmClass::setvarcov( const double _newVal, const int _i, const int _j )
//{
//	varcov[_i][_j]=_newVal;
//}
//
//const double SegmClass::getvarcov( const int _i, const int _j ) const
//{
//	return varcov[_i][_j];
//}


/** Write/Read property of int size. */
void SegmClass::setNdata( const int _newVal ){
	SegmClass::Ndata = _newVal;
}

const int SegmClass::getNdata(){
	return SegmClass::Ndata;
}

/** Read property of int Nclass.
*/
const int SegmClass::getNclass(){
	if (SegmClass::Nclass==0)
		cerr << "Warning : number of classes = 0\n";
	return SegmClass::Nclass;
}


/** Define the NoiseModel of the SegmClass
*	@param n : pointer on a (new) NoiseModel allocated.
* @remarks If NoiseModel::noise is not NULL, it is deleted.
*/
void SegmClass::setNoiseModel(NoiseModel*n){
	if (Ndata!=0)
	{
		// si noise avait d�j� �t� affect�
		if (noise!=NULL)
		{
			delete noise;
		}
		noise=n;
	}
	else 
		ERR("SegmClass::setNoiseModel : Can't create NoiseModel object without size.\n");
}
/** Give associated NoiseModel. */
NoiseModel* SegmClass::getNoiseModel(){
	return(noise);
}
/** Make dimensions for the A parameter */
void SegmClass::makeTrans(){
	A.assign(Nclass,0);
}
/** operator <  */
bool SegmClass::operator< (const SegmClass& ){
	return true;
}

/** of class SegmClass
operator <  */
bool operator< (SegmClass& _c1, SegmClass&_c2){
	// comparaison des normes des vecteurs des moments de premier ordre
	double Norme1= 0., Norme2 = 0.;
	for (int cpt_capt=0 ; cpt_capt<_c1.Ndata ; cpt_capt++)
	{
		Norme1+=pow(_c1.getNoiseModel()->getm1(cpt_capt),2.);
		Norme2+=pow(_c2.getNoiseModel()->getm1(cpt_capt),2.);
	}
	return (Norme1<Norme2);
}
