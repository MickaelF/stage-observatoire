/***************************************************************************
                          config.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "config.hpp"
#include <stdlib.h>



Config::Config(){
	
}

/** Config(string _filename)
	Open, read, and extract information of the configuration given file 
*/
Config::Config(string _filename){

	// Remplissage de la map contenant les TAGs et les valeurs associees
 		Mtags.insert(pair<string,string>("INPUT",""));
 		Mtags.insert(pair<string,string>("MASK",""));
 		Mtags.insert(pair<string,string>("SCAN",""));
 		
    Mtags.insert(pair<string,string>("ESTIM",""));
 		Mtags.insert(pair<string,string>("CLASS",""));
 		Mtags.insert(pair<string,string>("MARKOV",""));
 		Mtags.insert(pair<string,string>("NOISE",""));
 		Mtags.insert(pair<string,string>("DECORR",""));
    Mtags.insert(pair<string,string>("IT",""));
    Mtags.insert(pair<string,string>("STOP",""));
 		
//    Mtags.insert(pair<string,string>("INIT",""));
// 		Mtags.insert(pair<string,string>("INITP",""));
// 		Mtags.insert(pair<string,string>("SAVE",""));
 		Mtags.insert(pair<string,string>("OUTPUT",""));
		// Added so as to not always add a suffix to the segmentation map name
		Mtags.insert(pair<string,string>("MAP_SUFFIX",""));
 		Mtags.insert(pair<string,string>("TRACE",""));
 		Mtags.insert(pair<string,string>("MAPMS",""));
 		Mtags.insert(pair<string,string>("DISPLAY_MODE",""));
 		Mtags.insert(pair<string,string>("DISPLAY_APP",""));
	
	// Ouverture du fichier
	ifstream in(_filename.c_str(),ios::in);
	if (in.fail())
	{
		cerr << "Error : Impossible d'ouvrir le fichier " << _filename << endl;
		exit(-1);
	}
	
	// Lecture du fichier
	string buffer;
	string word;
	while (!in.eof())
	{		
	
		// Lecture d'une ligne
		getline(in,buffer);
		// si ce n'est pas un commentaire
		if (buffer[0] != '#')
		{
			unsigned int egal = 0 ;
			// Recupere le premier mot
			word = buffer.substr(0,egal=buffer.find("="));
			
			// Si ce mot n'est pas vide -> pas une ligne vierge -> a etudier
			if (!word.empty())
			{
				string TAG(word);
				string2string(TAG,TAG);
				//cout << TAG << " = ";
				
				// Recupere la valeur associee apres le '='
				word = buffer.substr(egal+1);
				// s'il n'y a rien, on met quand meme un blanc sinon
				// on ne peut pas utiliser l'objet word car NULL
				//cout << "\"" << word << "\"" << endl;
				string VALUE(word);
				
				map<string,string>::iterator p = Mtags.find(TAG);
				if (p!=Mtags.end())
					p->second = VALUE;
				else
					cerr << "Warning : Error in config file : tag " << TAG << " not found\n";
				
			} 
		}
	}
//	delete[] word;
	in.close();
}

Config::~Config(){
}



/********** ACCES FUNCTIONS **********/

/** getNclass : Read property of Number of classes in Config file.
 * Config :
 * 
 * Output :
 * 	const int	: number of classes given in config file
 * 		AUTO_NB_CLASS
 * 		FUZZY
 * 
 * Comment :
 *	Value returned is inside min and max defined : MIN_NB_CLASS MAX_NB_CLASS
*/

const int Config::getNclass()
{
	int val=0;
//	string tmp("");
	
	string answer(Mtags["CLASS"]);

	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_NB_CLASS << " for CLASS\n";
		Mtags["CLASS"]=answer=DEFAULT_NB_CLASS;
	}
	
	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on essaie d'extraire un entier
		if (this->string2int(answer,val)){
			// si on a bien un entier on verifie qu'il appartient aux bornes
			if (val<MIN_NB_CLASS){
				cout << "\nPlease give a value greater or equal to "
				<< MIN_NB_CLASS << " : ";
				cin >> answer;
			}
			else 
			if (val>MAX_NB_CLASS){
				cout << "\nPlease give a value lower or equal to " 
				<< MAX_NB_CLASS << " : ";
				cin >> answer;
			}
			else // valeur correcte
				return val;
		}
		// s'il ne s'agit pas d'un entier
		else {
			// on compare aux reponses predefinies
			if (answer=="AUTO")
				return AUTO_NB_CLASS;
			else
			if (answer=="FUZZY")
				return FUZZY;
			else {
				// si la reponse n'est pas une chaine connue
				cout << "\n\"" << answer << "\" parameter for CLASS not valid, please report to help in config file\n";
				cout << "Please give a correct argument : ";
				cin >> answer;
			}
		}
				
		Mtags["CLASS"]=answer;

	
	}// fin while

	return 0;
}


/** getEstim : Read property of Estimation in Config file.
 * Config :
 *
 * Output :
 * 	const int	: value related to Estimation method.
 *		EM
 *		SEM
 *		ICE
 *	
 * Comment :
*/
const short Config::getEstim()
{
	string answer(Mtags["ESTIM"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_ESTIM << " for ESTIM\n";
		answer=Mtags["ESTIM"]=DEFAULT_ESTIM;
	}
	
	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (answer=="EM")
			return EM;
		else
		if (answer=="SEM")
			return SEM;
		else
		if (answer=="ICE")
			return ICE;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << answer << "\" parameter for ESTIM not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> answer;
		}
		Mtags["ESTIM"]=answer;
	}// fin while
	
	// juste pour pas avoir de warnin a la compil
	return 0;
}

/** getMarkovModel : Read property of Markovian Model in Config file
 * Config :
 * 
 * Ouput :
 * 	const int	: value related to Markovian Model.
 * 		CHAIN
 * 		QUADTREE
 * 	
 * Comment :
*/
const short Config::getMarkovModel()
{
	string answer(Mtags["MARKOV"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_MARKOV << " for MARKOV\n";
		answer=Mtags["MARKOV"]=DEFAULT_MARKOV;
	}

	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	
	while (true)
	{
		// on compare aux reponses predefinies
		if (answer=="CHAIN")
			return CHAIN;
		else
		if (answer=="QUADTREE")
			return QUADTREE;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << answer << "\" parameter for MARKOV not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> answer;
		}
		Mtags["MARKOV"]=answer;
	}// fin while
	
	return 0;
}

/** getNoiseModel : Read property of Noise model in Config file
 * Config :
 *
 * Output :
 * 	const short	: value related to Noise model
 *	 	GAUSS
 * 		GENGAUSS
 *	 	WEIBULL
 * 		POISSON
 * 		AUTO_NOISE
 *		LOGG
 *
 * Comment :
*/
const short Config::getNoiseModel()
{
	string answer(Mtags["NOISE"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_NOISE << " for NOISE\n";
		answer=Mtags["NOISE"]=DEFAULT_NOISE;
	}

	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (answer=="GAUSS")
		{
			return GAUSS;
		}
		else 
		if (answer=="GENERAL_GAUSS")
		{
			return GENGAUSS;
		}
		else
		if (answer=="LOGNORMAL")
		{
			return LOGNORMAL;
		}
		else
		if (answer=="WEIBULL")
		{
			return WEIBULL;
		}
		else
		if (answer=="POISSON")
		{
			return POISSON;
		}
		else
		if (answer=="LOGG")
		{
			return LOGG;
		}
		else
		if (answer=="PEARSON")
		{
			return AUTO_NOISE;
		}
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n" << answer << " parameter for NOISE not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> answer;
		}
		Mtags["NOISE"]=answer;
	}// fin while
	
	return 0;
}


/** getDecorr : Read property of method of decorrelation method in Config file
 * Config :
 *
 * Output :
 * 	const unsigned short	: value related to decorrelation method
 *	 	NODECORR
 * 		CHOLESKY
 *	 	ICA
 *
 * Comment :
*/
unsigned short Config::getDecorr(){
	string answer(Mtags["DECORR"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_DECORR << " for DECORR\n";
		answer=Mtags["DECORR"]=DEFAULT_DECORR;
	}

	// tant qu'on n'arrive pas a avoir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (answer=="CHOLESKY")
			return CHOLESKY;
		else if (answer=="ICA")
			return ICA;
		else if (answer=="NO")
			return NODECORR;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n" << answer << " parameter for DECORR not valid, please report to help in config file";
			cout << "\nPlease give a correct argument : ";
			cin >> answer;
		}
		Mtags["DECORR"]=answer;
	}// fin while
	
	return 0;
}


/** getInit : Read property of method of ## DATA DRIVEN ## initialization in Config file
 * Config :
 *
 * Output :
 * 	const int	: value related to data driven initialization
 *	 	KMEAN
 * 		FUZZY_KMEAN
 *	 	SKMEAN
 *
 * Comment :
*/
const short Config::getInit()
{
	string answer(Mtags["INIT"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_INIT << " for INIT\n";
		answer=Mtags["INIT"]=DEFAULT_INIT;
	}

	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (answer=="KMEAN")
			return KMEAN;
		else 
		if (answer=="FUZZY_KMEAN")
			return FUZZY_KMEAN;
		else
		if (answer=="SKMEAN")
			return SKMEAN;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << answer << "\" parameter for INIT not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> answer;
		}
		Mtags["INIT"]=answer;
	}// fin while
	
	return 0;
}




/** getInitP : Read property of method of ## A PRIORI ## initialization in Config file
 * 
 *
 * @return
 * 	value related to a priori initialization
 *	 	RANDOM
 *	 	EQUIPROBA
 *
 * 
*/
const short Config::getInitP()
{
	string answer(Mtags["INITP"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,answer))
	{
		//cout << "Warning : Use default value = " << DEFAULT_INITP << " for INITP\n";
		answer=Mtags["INITP"]=DEFAULT_INITP;
	}

	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (answer=="RANDOM")
			return RANDOM;
		else 
		if (answer=="EQUIPROBA")
			return EQUIPROBA;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << answer << "\" parameter for INITP not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> answer;
		}
		Mtags["INITP"]=answer;
	}// fin while
	
}

/** getMapMS (Multi Scale map) */
bool Config::getMapMS(){
	string test;
	string answer(Mtags["MAPMS"]);
	
	// extrait la chaine correspondant au test 
	if (!string2string(answer,test))
		test = DEFAULT_MAPMS;

	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (test=="YES")
			return true;
		else 
		if (test=="NO")
			return false;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << test << "\" parameter for MAPMS not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> test;
		}
		
	}// fin while
}


/** Read function to know if option to visualize map is given in config file
 * @return true if Visualisation of the segmented map is wanted
 */
bool Config::getVisuMap()
{
	string answer(Mtags["DISPLAY_MODE"]);
	
	// si la position ou on trouve le mot MAP est + grand que la taille du mot -> il n'y est pas
	if (answer.find("MAP")>answer.size())
		return false;
	else
		return true;
}

/** getVisuMapMS */
bool Config::getVisuMapMS(){
	string answer(Mtags["DISPLAY_MODE"]);
	
	// si la position ou on trouve le mot MAP est + grand que la taille du mot -> il n'y est pas
	if (answer.find("MAPMS")>answer.size())
		return false;
	else
		return true;
}

/** Read function to know if option to visualize config images is given in config file */
bool Config::getVisuInputig(){
	string answer(Mtags["DISPLAY_MODE"]);
	
	// si la position ou on trouve le mot INPUT est + grand que la taille du mot -> il n'y est pas
	if (answer.find("INPUT")>answer.size())
		return false;
	else
		return true;
}

/** Read function to know if option to visualize decorrelated images is given in config file */
bool Config::getVisuDecorr(){
	string answer(Mtags["DISPLAY_MODE"]);
	
	// si la position ou on trouve le mot DECORR est + grand que la taille du mot -> il n'y est pas
	if (answer.find("DECORR")>answer.size())
		return false;
	else
		return true;
}


/** Read function to know which soft for visualize images */
string Config::getVisuApp()
{
	string tmp("");
	string answer(Mtags["DISPLAY_APP"]);
	// extraction d'une chaine de caractere
	if (!this->string2string(answer,tmp))
	{
//		cout << "Warning : Use default value = " << DEFAULT_DISPLAY_APP << " for DISPLAY APP\n";
		tmp=DEFAULT_DISPLAY_APP;
	}

	return tmp;	
}

/** getVisuFreq
* @return frequency of visualisation
* @param _image : string like "MAP" or "DECORR" of frequency to get
*/
unsigned int Config::getVisuFreq(const string& _image ){
	int freq=0;
	string disp;
	string answer(Mtags["DISPLAY_MODE"]);
	
	// extrait la chaine correspondant au test 
	if (!string2string(answer,disp))
		return NODISPLAY;
	else
	{
		// extrait la chaine du test de la chaine principale
		answer.erase(0,answer.find(_image)+_image.size());
		const string tmp(answer);
		// recherche s'il y a une frequence (un entier)
		if (string2int(tmp,freq))
			return freq;
		else
			return NODISPLAY;
	}
}

/** No descriptions */
void Config::eraseVisu(const string & _s){
	string answer(Mtags["DISPLAY_MODE"]);
	answer.erase(answer.find(_s),answer.find(_s)+_s.size());
}

/** No descriptions */
void Config::setVisu(const string & _newString){
	Mtags["DISPLAY_MODE"]=_newString;
}

/** Returns the MAP_SUFFIX parameter */
bool Config::getMapSuffix(){
  string answer(Mtags["MAP_SUFFIX"]);
  // extraction d'une chaine de caractere
  if (!this->string2string(answer,answer))
    answer=Mtags["MAP_SUFFIX"]=DEFAULT_MAP_SUFFIX;

  bool output;
  if (answer == "YES")
    output = true;
  if (answer == "NO")
    output = false;

  return output;
}

/** Give the name to save the MAP */
string Config::getOutMAP(){
  string outMAPname(this->getOutput());
 
  if (getMapSuffix())
    outMAPname += "_map.fits";

  return outMAPname;
}

/** getOutMapMS */
string Config::getOutMapMS(){
		string outMAPname(this->getOutput());
		outMAPname += "_mapms.fits";
		return outMAPname;
}

/** Give the name to save the STATS file */
string Config::getOutStats(){
		string outname(this->getOutput());
		outname += "_stats.txt";
		return outname;
}

/** Give the name to save the TRACE file */
string Config::getOutTrace(){
		string outname(this->getOutput());
		outname += "_trace.txt";
		return outname;
}

/** @return path name for saving files */
string Config::getOutput(){
	return(this->string2string(Mtags["OUTPUT"]));
}

/** Erase last output with the string */
bool Config::setOutput(string & _newOutput){		
	Mtags["OUTPUT"]=_newOutput;
	return true;
}

/** getInputFiles */
vector<string> Config::getInputFiles(){
	return ( string2strings(Mtags["INPUT"]) );
}

/** setInputFiles : used if there is an option for input files that is prior */
void Config::setInputFiles(const vector<string> &s){
	Mtags["INPUT"]="";
	for (unsigned int i=0 ; i<s.size() ; i++)
		Mtags["INPUT"] += s[i] + " ";
}

/** getMask */
string Config::getMask(){
	return (this->string2string(Mtags["MASK"]));
}

/** setMask : used if there is an option for input mask that is prior */
void Config::setMask(const string &s){
	Mtags["MASK"]=s;
}

unsigned int Config::getItMax(){
	int val = 0;
	if (!this->string2int(Mtags["IT"],val))
		return (static_cast<unsigned int>(DEFAULT_IT_MAX));
	return (static_cast<unsigned int>(val));
}


/** 
* @return : value  corresponding to the convergence criterium 
*/
const short Config::getStopTest(){
	string test;
	string answer(Mtags["STOP"]);
	
	// extrait la chaine correspondant au test 
	if (!string2string(answer,test))
		test = DEFAULT_STOP;
	else
	{
		// extrait la chaine du test de la chaine principale
//		string * tmp = &Mtags["STOP"];
//		tmp->erase(tmp->find(test),test.size());
	}

	// tant qu'on n'arrive pas a avoir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (test=="PC_CHANGE")
			return PC_CHANGE;
		else 
		if (test=="PC_DIST")
			return PC_DIST;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << test << "\" parameter for STOP not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> test;
		}
		
	}// fin while
	
}

float Config::getStop(){
	float val = 0.;
	string answer(Mtags["STOP"]);
	
	// extrait la chaine correspondant au test 
	vector<string> t = string2strings(answer);
	if (t.empty())
		return DEFAULT_PC_STOP;
	else
	{
		bool pc = false;
		// cherche dans tous les arguments de la ligne une valeur numerique
		for (unsigned int i=0 ; i<t.size() ; i++)
			pc |= string2float(t[i],val);
		// si on en a trouve aucune, on rend le DEFAULT
		if (!pc)
			return DEFAULT_PC_STOP;
		else
			return val;
	}
}

/** getTrace
	* @return : yes if trace is asked, no otherwise */
const bool Config::getTrace() {
	string test;
	string answer(Mtags["TRACE"]);
	
	// extrait la chaine correspondant au test 
	if (!string2string(answer,test))
		test = DEFAULT_TRACE;

	// tant qu'on arrive pas a voir une reponse correcte
	// on pose la question en interactif
	while (true)
	{
		// on compare aux reponses predefinies
		if (test=="YES")
			return true;
		else 
		if (test=="NO")
			return false;
		else
		{
			// si la reponse n'est pas une chaine connue
			cout << "\n\"" << test << "\" parameter for TRACE not valid, please report to help in config file\n";
			cout << "Please give a correct argument : ";
			cin >> test;
		}
		
	}// fin while
	
}

/** getScan */
const string Config::getScan() {
	return(this->string2string(Mtags["SCAN"]));	
}

/** @return the list of options in one string
 */
string Config::getString() {
	string options("MARSIAA CONFIGURATION PROCESS : ");
	map<string,string>::iterator p = this->Mtags.begin() ;
	while ( p != Mtags.end() )
	{
		options += p->first + " = " + p->second + " ; ";
		p++;
	}
		
//	cout << options << endl;
	return options;
}

/********** PRIVATE FUNCTIONS **********/

/** Extract only string without white space */
string Config::string2string(const string & _in){
	string tmp("");

	if (string2string(_in,tmp))
		return tmp;
	else
		return static_cast<string>("");
}

/** Extract only string without white space
*/
bool Config::string2string(const string & _in, string & newString ){
	istringstream ss("");
	
	// set flag no white space
	ss.setf(ios::skipws);
	// give string to read
	ss.str(_in);
	// extract a string
	ss >>	newString;
	
	if (ss.fail()){
	//	cerr << "Error : could not read texte in : \"" << _in << "\"" << endl;
		return false;
	}
	
	return true;
}

/** Extract only integer value without white space
 */
bool Config::string2int(const string & _in, int & newValue){
	istringstream ss("");
	
	// set flag no white space
	ss.setf(ios::skipws);
	// give string to read
	ss.str(_in);
	// extract an integer
	ss >>	newValue;
	
	if (ss.fail()) {
		//cerr << "Error : could not read integer in : \"" << _in << "\"" << endl;
		return false;
	}
	
	return true;
}


/** Extract all strings without white space */
vector<string> Config::string2strings(const string & _in){
	string tmp ;
	vector<string> vstring;	
	istringstream ss("");
 	// set flag no white space
 	ss.setf(ios::skipws);
 	// give string to read
 	ss.str(/*this->string2string(_in)*/_in);

	while(1)
	{	
		// extract a string
		ss >> tmp;
		if (!ss.fail())
			vstring.push_back(tmp);
		else
			break;
	}
	return vstring;
}

/** Extract only real value without white space
* @param _in : string to explore
* @param f : value extracted
* @return : true if no problem, false if there is no real value
*/
bool Config::string2float(const string & _in, float & f){
	istringstream ss("");
	
	// set flag no white space
	ss.setf(ios::skipws);
	// give string to read
	ss.str(_in);
	// extract a float
	ss >>	f;
	
	if (ss.fail()) {
		//cerr << "Error : could not read float in : \"" << _in << "\"" << endl;
		return false;
	}
	
	return true;
}


/** Give filenames in case of AUTO option in NOISE tag. */
vector<string> Config::getNoiseFiles(){
	vector<string> files;
	files.push_back("l1.fits");
	return files;
}

