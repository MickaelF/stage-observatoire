/***************************************************************************
                          gauss.cpp  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "gauss.h"
#include "gengauss.h"
#include "data.h"
#include "image.h"
#include "pyramid.h"
#include "segmclass.h"
#include "Math.h"
#include <math.h>
template <typename TType> class Image;


Gauss::Gauss()
{
//	cout << "-------Constructeur Gauss\n\n";
	if (Ndata>0)
	{
		// initialise les tableaux � 0
		mean.assign(Ndata,0.);
		stddev.assign(Ndata,0.);
	}
	else ERR("Gauss::Gauss : Can't create Gauss object without size of data.\n");
}

Gauss::Gauss (const Gauss& _copie ) : NoiseModel(_copie) {
//	cout << "-------Constructeur Gauss par copie Gauss!\n\n";
	mean = _copie.getmean();
	stddev = _copie.getstddev();
}

Gauss::Gauss(const GenGauss& _copie) : NoiseModel(_copie) {
//	cout << "-------Constructeur Gauss par copie GenGauss!\n\n";
	mean = _copie.getmean();
	stddev = _copie.getstddev();
}

Gauss::Gauss (const NoiseModel& _copie ) : NoiseModel(_copie) {
//	cout << "-------Constructeur Gauss par copie NoiseModel !\n\n";
	mean = _copie.getm1();
	stddev.assign(Ndata,0.);
	for (unsigned int i=0 ; i<Ndata ; i++)
		stddev[i] = sqrt(_copie.getm2(i));
}

Gauss::Gauss(const vector<double>& _mean, const vector<double>& _stddev) : NoiseModel () {
	this->mean = _mean;
	this->stddev = _stddev;
	evalvarcov();
	evalvarcovInv();
}

Gauss::~Gauss(){
//	cout << "-------Destructructeur Gauss\n\n";
}

/** operator =  */
Gauss& Gauss::operator= (const Gauss& _copie){
	if (this != &_copie)
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		mean[i]=_copie.mean[i];
		stddev[i]=_copie.stddev[i];
	}
	return *this;
}

bool Gauss::isGauss() const {
	return true;
}

//const double prefix = 1./pow(2.*PI,static_cast<float>(3)/2.);

/** Evaluate statistical likelihood for Gaussian noise */
double Gauss::likelihood(const vector<double>& _values)
{
	
	if(Ndata!=_values.size())
		ERR("Gauss::likelihood : could not evaluate likelihood, sizes of data and noise are not similar\n"); 
	
	double argu,res;
	vector<double> M(Ndata,0);

	const unsigned int lastNdata = Ndata-1;
	// centre les valeurs
	for(int cpt_capteur=lastNdata ; cpt_capteur>=0 ; cpt_capteur--)
		M[cpt_capteur]=_values[cpt_capteur]-mean[cpt_capteur];

	argu=res=0.;
	for(int cpt1=lastNdata ; cpt1>=0 ; cpt1--)
	{
		res=0.;
		for(int cpt2=lastNdata ; cpt2>=0 ; cpt2--)
		{
			res+=M[cpt2]*varcovInv[cpt2][cpt1];
		}
		argu+=M[cpt1]*res;
	}

//	double a = 1./pow(2.*PI,static_cast<float>(Ndata)/2.)/sqrt(det)*exp(-0.5*argu);
	return(1./pow(2.*PI,static_cast<float>(Ndata)/2.)
//	return(prefix
					/sqrt(det)*exp(-0.5*argu));

}

/********** ACCES FUNCTIONS **********/
/** Write/Read property of one mean. */
void Gauss::setmean(const unsigned int _capteur, const double _newVal){
	mean[_capteur] = _newVal;
	m1[_capteur] = _newVal;
}
const double Gauss::getmean(const unsigned int _capteur) const{
	return mean[_capteur];
}
/** get mean */
const vector<double>& Gauss::getmean() const {
	return mean;
}

/** Write/Read property of double stddev. */
void Gauss::setstddev(const unsigned int _capteur, const double _newVal){
  //if (_newVal>SEUIL_ECA)
  //{
  stddev[_capteur] = _newVal;
  m2[_capteur] = _newVal*_newVal;
  //}
  //else
  //{
  //stddev[_capteur] = SEUIL_ECA;
  //m2[_capteur] = SEUIL_ECA*SEUIL_ECA;
  //}
}
const double Gauss::getstddev(const unsigned int _capteur) const{
	return stddev[_capteur];
}
/** get stddev */
const vector<double>& Gauss::getstddev() const{
	return stddev;
}


/** Evaluate and modify parameter mean with Maximum Likelihood method.
 * Config	:
 * 		const Data& data : one of all config data
 *   	const int ndata : range in vector of data
 * Output	:
 * 		double value : ean evaluated by ML method.
 *
 * Comment:
 *		Summarize ALL values in the data -> if data are pyramid then evaluate mean on all scales
 */
double Gauss::evalmean(const unsigned int ndata, const Data* data){
	const long _size = data->length();
	Image<bool> *mask = data->mask;
	double sum=0,sumWeights=0;

	for (int s=0 ; s<_size ; s++)
	{
		if((*mask)[s])
		{
			sum+=static_cast<double>(data->getValue(s));
			sumWeights++;
		}
	}

	setmean(ndata, sum/sumWeights);
	return (this->mean[ndata]);
}

/** Evaluate and modify parameter mean (weighted) with Maximum Likelihood method.
 * Config	:
 * 		const Data& data : one of all config data
 *   	const int ndata : range in vector of data
 *    const Image<double>& weight : weights to use
 * Output	:
 * 		double value : mean evaluated by ML method.
 *
 * Comment :
 *		Summarize ALL values in the data -> if data are pyramid then evaluate mean on all scales
 */
double Gauss::evalmean(const unsigned int ndata, const Data* data, const Data* weight ){
	// v�rification que les donn�es sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("Error : Cannot eval mean because sizes of images and weights are not similar.\n");

	const unsigned int _len=data->length();
	Image<bool> *mask = data->mask;

	double sum=0.,sumWeights=0.;
	for (unsigned int s=0 ; s<_len ; s++)
	{
		if((*mask)[s])
		{
			sum+=static_cast<double>(weight->getValue(s))
				* static_cast<double>(data->getValue(s));
			sumWeights+=static_cast<double>(weight->getValue(s));
		}
	}

	setmean(ndata, sum/sumWeights);
	return (this->mean[ndata]);
}

/** Evaluate and modify parameter Standard deviation with Maximum Likelihood method.
 * Config	:
 *  	const int ndata				: number of the rank of the data
 *		const Data *data			: pointer on data
 * Output	:
 * 		double : ML estimated standard deviation
 */
double Gauss::evalstddev(const unsigned int ndata, const Data* data){
	const long _size = data->length();
	Image<bool> *mask = data->mask;
	double sum=0,sumWeights=0;
	for (int s=0 ; s<_size ; s++)
	{
		if((*mask)[s])
		{
			sum+=pow( mean[ndata]-static_cast<double>(data->getValue(s)) , 2. );
			sumWeights++;
		}
	}

	setstddev(ndata, sqrt(sum/(sumWeights)));
	return (this->stddev[ndata]);
}

/** Evaluate and modify Standard deviation parameter with Maximum likelihood method.
 * Config	:
 *  	const int ndata				: number of the rank of the data
 *		const Data *data			: pointer on data
 *		const Data* pt_weight	: pointer on weight data
 * Output	:
 *		double : ML estimated standard deviation
 */
double Gauss::evalstddev(const unsigned int ndata, const Data* data,const Data* weight ){
	// v�rification que les donn�es sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("Error : Cannot eval stddev because sizes of images and weights are not similar.\n");

	const unsigned int _size = data->getwidth()*data->getheight();
	Image<bool> *mask = data->mask;
	double sum=0,sumWeights=0;
	for (unsigned int s=0 ; s<_size ; s++)
	{
	  if((*mask)[s])
	    {
	      sum+=		
		static_cast<double>(weight->getValue(s))
		* pow( mean[ndata]-static_cast<double>(data->getValue(s)) , 2. );
	      sumWeights+=static_cast<double>(weight->getValue(s));
	    }
	}
	
	// modifie la stddev
	setstddev(ndata, sqrt(sum/(sumWeights)));
	return (this->stddev[ndata]);
}


/** Update data driven with weights
 * Config :
 *		const vector<Data*> data : vector of data to evaluate
 *		const Data* pt_xsi : pointer on weight data
 *
 * Output	: 
 *
 * Comment :
 */
void Gauss::update_dataDriven(const vector<Data*>& data, const Data * pt_xsi )
{
	// pour chaque image en entr�e
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
	  evalmean(i,data[i],pt_xsi);
	  evalstddev(i,data[i],pt_xsi);
	}

	for (unsigned int i=0 ; i<Ndata ; i++)
		for (unsigned int j=i+1 ; j<Ndata ; j++)
			evalcorr(i,data[i],j,data[j],pt_xsi);
	
	this->evalvarcov();
	this->evalvarcovInv();
}

/** getParam : get one parameter of this noise
 * @param _p : number corresponding to one parameter 
 */
const vector<double> Gauss::getParam(const unsigned short _p){
  
  // si les parametres sont calcules dans l'espace decorrele
  // on les estime par les moments
  
  if (this->ptf_decorr!=NULL)
    {
      switch (_p) {
	
      case 1 :
	return m1;
	break;
  	
      case 2 :
	return m2;
	break;
  	
      default :
	return vector<double>( Ndata ,0.);
      }
    }
  else // sinon, on donne les vrais parametres
    {
      switch (_p) {
	
      case 1 :
	return mean;
	break;
       
      case 2 :
	{
	  vector<double> var(stddev);
	  for (unsigned int i=0 ; i<var.size() ; i++)
	    var[i] *= var[i];
	  return var;
	  break;
	}
      default :
	return vector<double>( Ndata ,0.);
      }
    }
  return vector<double>();
}



