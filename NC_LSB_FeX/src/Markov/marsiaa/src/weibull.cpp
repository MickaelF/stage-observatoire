/***************************************************************************
                          weibull.cpp  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@cacao.u-strasbg.fr
 ***************************************************************************/


#include "weibull.h"

Weibull::Weibull(){
	ERR("Weibull::Weibull() NOT IMPLEMENTED \n");
}
Weibull::Weibull(const NoiseModel&){
	ERR("Weibull::Weibull(const NoiseModel&) NOT IMPLEMENTED \n");
}
Weibull::~Weibull(){
}
/** Update data driven */
void Weibull::update_dataDriven(const vector < Data * > & , const Data * ){
}
/** Evaluate statistical likelihood */
double Weibull::likelihood(const vector < double > & ){
	return 0.5;
}


bool Weibull::isWeibull() const{
	return true;
}

/** getParam : get one parameter of this noise
* @param _p : number corresponding to one parameter 
*/
const vector<double> Weibull::getParam(const unsigned short _p){

switch (_p) {

	case 1 :
		return min;
		break;
	
	case 2 :
		return shape;
		break;
	
	case 3 :
		return scale;
		break;
	
	}
	return vector<double>();
}
