/***************************************************************************
                          icequadtree.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "icequadtree.h"
#include "data.h"
#include "pyramid.h"
#include "noisemodel.h"
#include "segmclass.h"

ICEQuadTree::ICEQuadTree(){
}
ICEQuadTree::~ICEQuadTree(){
}

/** Update "a priori" parameters. */
void ICEQuadTree::update_apriori(vector < Data * > & data, vector < SegmClass * > & classes){
	const unsigned int _Nclass = classes.size();
//	const unsigned int _Ndata = data.size();

	cout << /*Start */"update apriori with ICE\n";	
	
	if (P_forward.empty())
		this->init(data,classes);
	
	// d'abord une passe descendante pour calculer les proba a posteriori
	browse_backward(classes);
	
	/*** idem EMQuadTree ***/
	
	const unsigned int baseScale = data[0]->getBaseScale();
	// Nscale -2 pour avoir le num de l'avant dernier etage car on utilise le pere alors les calculs ne peuvent etre sur le dernier etage
	const unsigned int lastScale = Nscale-2 ; 
	
	// update des proba de transitions aij
	for (unsigned int i=0 ; i<_Nclass ; i++)
	{
		
		// somme des proba marginales a posteriori sur toutes les �chelles
		double Smarginal=0.;
			
 		// somme des proba conjointes a posteriori
 		vector<double> Sjoint(_Nclass,0.);
	
 		for (int e=lastScale ; e>=0 ; e--){
			// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
 			const unsigned int _sizeResolution = pow(4,baseScale-e);
 			for(int pos=_sizeResolution-1 ; pos>=0 ; pos--) {
 				Smarginal += P_marginal[i]->get_father(e,pos);
 			}
 		}
		
		for(unsigned int j=0 ; j<_Nclass ; j++){
//		cout << "\tclass"<<j<<endl;
			double N=0.;
	 		for (int e=lastScale ; e>=0 ; e--){
	 			const unsigned int _sizeResolution = pow(4,baseScale-e);
 				for(int pos=_sizeResolution-1 ; pos>=0 ; pos--) {
 					N += getP_joint_aposteriori( classes,i,j,e,pos,P_marginal[i]->get_father(e,pos) );
				}
			}
			// on ne modifie pas des maintenant les proba de transitions
			// car on doit reutiliser les memes pour tout j
			Sjoint[j]=N;
		}
		
		for(unsigned int j=0 ; j<_Nclass ; j++){
			if (Smarginal>eps)
				classes[i]->setA(j,Sjoint[j]/Smarginal);
			else
			{
				cerr<<"\nICEQuadTree::update_apriori : Somme marginal aposteriori <eps classe "<<i<<endl;
				classes[i]->setA(j,1./static_cast<float>(_Nclass));
			}
//			cout << "\nnouveau a["<<i<<"]["<<j<<"] = "<<classes[i]->getA(j)<<endl;
//			cout << "avec N= " << Sjoint[j] << " et D = " << Smarginal << endl;
		}
		
	}
	
	
	// update des proba a priori de chaque classe
	// avec les valeurs tout en haut de l'arbre
	double S=0.;
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		classes[k]->setP( P_marginal[k]->get_pixel(baseScale,0) );
		S+=classes[k]->getP();
//		cout << "nouveau P"<<k<<" = "<<classes[k]->getP()<<endl;
	}

	// verification de l'int�grit� des proba que l'on vient de calculer
	if (fabs(1.0-S)>0.01)
	{
		cerr << "EMQuadtree::update_apriori : Somme des propabilites a priori incorrecte : " << S <<endl;
		for (unsigned int k=0 ; k<_Nclass ; k++)
		{
			cerr << "classe : "<<k<<endl;
			cerr << "xsi[Nscale-1][0] = "<< P_marginal[k]->get_pixel(baseScale,0) << endl;
		}
		ERR("");
	}
	
		
	/***** simulations identiques au SEMQuadTree::update_apriori *****/ 
	// mais ici, pour pr�parer le ICEQuadTree::update_dataDriven
 	
	// reinitialisation des proba marginales a zero
	for (int e=lastScale ; e>=0 ; e--)
	{
 		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
 		const unsigned int _sizeResolution = pow(4,baseScale-e);
 		for(int pos=_sizeResolution-1 ; pos>=0 ; pos--)
		for (unsigned int k=0 ; k<_Nclass ; k++)
 			P_marginal[k]->set_pixel(e,pos,0.);
	}
	
	for (unsigned char n=0 ; n<N_STOCHASTIC ; n++){
		// tirage d'un quadtree simul�
		Pyramid<unsigned char> * X = simulX(classes);
		// init � la + petite �chelle
		P_marginal[X->get_pixel(baseScale,0)]->get_pixel(baseScale,0)+=1./static_cast<float>(N_STOCHASTIC);
		for (int e=lastScale ; e>=0 ; e--){
			// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
			const unsigned int _sizeResolution = pow(4,baseScale-e);
			for(int pos=_sizeResolution-1 ; pos>=0 ; pos--)
			{
				P_marginal[X->get_pixel(e,pos)]->get_pixel(e,pos)+=1./static_cast<float>(N_STOCHASTIC);
//				P_joint[X->get_father(e,pos)][X->get_pixel(e,pos)]->get_pixel(e,pos)+=1./static_cast<float>(N_STOCHASTIC);
			}
		}
		//X->getbase()->show();
		delete X;
	}

//	cout << "End update apriori with ICE\n";	

}
