/***************************************************************************
                          chain.cpp  -  description
                             -------------------
    begin                : lun ao� 19 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/

#include "noisemodel.h"
#include "chain.h"
#include "data.h"
#include "segmclass.h"
#include "image.h"
#include "gauss.h"


Chain::Chain(){
//	cout << "\n-------Constructeur Chain\n";
//P_forward.clear();
//P_backward.clear();
//P_marginal.clear();
}

Chain::~Chain(){
//	cout << "\n-------Destructeur Chain\n";
	if (!P_marginal.empty()){
		for (unsigned int k=0 ; k<P_marginal.size() ; k++)
			delete P_marginal[k];
		P_marginal.clear();
	}
	if (!P_forward.empty()){
		for (unsigned int k=0 ; k<P_forward.size() ; k++)
			delete P_forward[k];
		P_forward.clear();

	}

	if (!P_backward.empty()){
		for (unsigned int k=0 ; k<P_backward.size() ; k++)
			delete P_backward[k];
		P_backward.clear();
	}
	
}


/** browse forward and backward */
void Chain::browse(const vector < Data * > & data, const vector < SegmClass * > & classes) {
//	init(data,classes);
	// Somme pour une normalisation commune
	vector<double> S(data[0]->length(),0.); 

	browse_forward(data,classes,S);
	browse_backward(data,classes,S);

}


/** No descriptions
 *
 *	Comment :
 */
void Chain::browse_forward(const vector<Data*> & data, const vector<SegmClass*> & classes, vector<double>& S){

	if (P_forward.empty())
		this->init(data,classes);
		//ERR("Chain::Chain : MarkovModel::P_forward no created -> lancer Chain::init\n");

cout<</*Start */"browse forward\n";

	// verification que le vecteur de Data* correspond � des Image<double>*
	if ( !data[0]->isImage() ) 
		ERR("Chain::browse_forward : vector<Data*> != vector<Image<>*>\n");

	const unsigned int _Nclass = SegmClass::getNclass();
	const unsigned int _size = data[0]->length();
	
	for(unsigned int k=0 ; k<_Nclass ; k++)
	{
		(*P_forward[k])[0]=classes[k]->getP()*classes[k]->getNoiseModel()->likelihood(data,0);
		// somme pour la normalisation de P_forward[k][0]
		S[0]+=(*P_forward[k])[0];
	}
	
	// normalisation
	if (S[0] > eps)
		for(unsigned int k=0 ; k<_Nclass ; k++)
			(*P_forward[k])[0]/=S[0];
	else
		for(unsigned int k=0 ; k<_Nclass ; k++)
			(*P_forward[k])[0]=1./static_cast<float>(_Nclass);

	// calcul de P_forward pour toutes les positions dans une image grace au P_forward pr�c�dent
	for(unsigned int pos=1 ; pos<_size ; pos++)
	{
		for(unsigned int k=0 ; k<_Nclass ; k++)
		{
			(*P_forward[k])[pos]=0.;
			for(unsigned int l=0 ; l<_Nclass ; l++)
				(*P_forward[k])[pos]+=classes[l]->getA(k)*(*P_forward[l])[pos-1];
			(*P_forward[k])[pos]*=classes[k]->getNoiseModel()->likelihood(data,pos);
		}
		
		S[pos]=0.;
		// normalisation pour chaque position
		for(unsigned int k=0 ; k<_Nclass ; k++)
			S[pos]+=(*P_forward[k])[pos];
		
		if(S[pos]>eps)
			for(unsigned int k=0 ; k<_Nclass ; k++)
				(*P_forward[k])[pos]/=S[pos];
		else
			for(unsigned int k=0 ; k<_Nclass ; k++)
				(*P_forward[k])[pos]=1./static_cast<float>(_Nclass);
		
	}

//cout << "End browse forward\n";
}


/** No descriptions */
void Chain::browse_backward(const vector<Data*> & data, const vector<SegmClass*> & classes, vector<double>& S){
cout << /*Start */"browse backward\n";
	
	if (P_backward.empty())
		this->init(data,classes);
//		ERR("Chain::Chain : MarkovModel::P_backward no created -> lancer Chain::init\n");

	// verification que le vecteur de Data* correspond � des Image<double>*
	if ( !data[0]->isImage() ) 
		ERR("Chain::browse_backward : vector<Data*> != vector<Image<T>*>\n");

	
	const unsigned int _Nclass = SegmClass::getNclass();
	const unsigned int _size = data[0]->length();
	
	// initalisation de P_backward � la derni�re position � 1
	for(unsigned int k=0 ; k<_Nclass ; k++)
		(*P_backward[k])[_size-1]=1.;

	// calcul de P_backward en fonction du P_backward suivant et des proba de transition
	// puis normalisation sur les P_forward
	
	for(int pos=_size-2 ; pos>=0 ; pos--)
	{
		for(unsigned int k=0 ; k<_Nclass ; k++)
		{
			(*P_backward[k])[pos]=0.;
			for(unsigned int l=0 ; l<_Nclass ; l++)
				(*P_backward[k])[pos]+=classes[k]->getA(l)
					* (*P_backward[l])[pos+1]
					* classes[l]->getNoiseModel()->likelihood(data,pos+1);
		}
		
		if(S[pos+1]>eps)
			for(unsigned int k=0 ; k<_Nclass ; k++)
				(*P_backward[k])[pos]/=S[pos+1];
		else
			for(unsigned int k=0 ; k<_Nclass ; k++)
			{
				(*P_forward[k])[pos]=1./static_cast<float>(_Nclass);
				(*P_backward[k])[pos]=1;
				(*P_backward[k])[pos+1]=1;
			}
	}

	// calcul des proba marginales a posteriori = P_forward * P_backward
	for(unsigned int k=0 ; k<_Nclass ; k++)
	{
		Image<double>* ptk_marginal = P_marginal[k];
		Image<double>* ptk_forward = P_forward[k];
		Image<double>* ptk_backward = P_backward[k];

		for(unsigned int pos=0 ; pos<_size ; pos++)
			ptk_marginal->setValue(pos,(*ptk_forward)[pos] * (*ptk_backward)[pos]);
		
	}
	
//cout << "End browse backward\n";
}


/** update_dataDriven : Update Data-driven parameters 
Config :
	data		: vector of data to use
	classes	:	vector of segmented classes, to update their noise parameters

Output :

Comment :
	data must be an Image<double>
*/
void Chain::update_dataDriven(const vector<Data *> & , vector<SegmClass*> & )
{
	cout << "!!!! NON Chain::update_dataDriven\n";
/*
	const unsigned int _size = data[0]->length();
	
	cout << "update_dataDriven\n";
	
	if ( !data[0]->isImage() )
		ERR("Error : Chain::update_dataDriven() with data different of Image\n"); 
		
	const unsigned int _Nclass = classes.size();
//	Data *marginal = new Image<double>(data[0]->getwidth(),data[0]->getheight());
	
	// pour chaque classe appelle ses fonctions de mise a jour des param de bruit
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
//		cout << "class" << k << " ";
		// creation de xsi en Image car on est en chain
		Data *xsi = new Image<double>(data[0]->getwidth(),data[0]->getheight());
		
		// calcul des proba marginales pour tous les sites de l'image
		for (unsigned int pos=0 ; pos<_size ; pos++){
			xsi->setValue(pos,this->getP_marginal_aposteriori(k,pos));
		}		
		classes[k]->getNoiseModel()->update_dataDriven(data, xsi);
//		cout << "nouv mean"<<k<<" = "<<dynamic_cast<Gauss*>(classes[k]->getNoiseModel())->getmean(0)<<endl;
//		cout << "nouv stdev"<<k<<" = "<<dynamic_cast<Gauss*>(classes[k]->getNoiseModel())->getstddev(0)<<endl;
		delete xsi;
	}
cout << "fin update_dataDriven\n";
//	delete marginal;
*/
}


/** No descriptions */
void Chain::update_apriori(){
}
/** Simulate a solution */
void Chain::calcul_X(){
}

/** Initialize and allocate memory for private parameters
	Config :
		Data[] data : tableau des donn�es entr�es
		SegmClass[] classes : tableau des classes de segmentation
	Output : none
 */
void Chain::init(const vector<Data *> & data, const vector<SegmClass*> & ){
	/* Creation des objets P_forward et P_backward parents */
	for (int k=0 ; k<SegmClass::getNclass() ; k++)
	{
		P_forward.push_back(new Image<double>(data[0]));
		P_backward.push_back(new Image<double>(data[0]));
		P_marginal.push_back(new Image<double>(data[0]));
	}
	
/*	// creation de vecteurs identiques qui pointent sur des Image
	for (unsigned int k=0 ; k<MarkovModel::P_backward.size() ; k++)
		P_backward.push_back(dynamic_cast<Image<double>*>(MarkovModel::P_backward[k]));

	for (unsigned int k=0 ; k<MarkovModel::P_forward.size() ; k++)
		P_forward.push_back(dynamic_cast<Image<double>*>(MarkovModel::P_forward[k]));
	*/
}


/** Transition a posteriori = akl*f_l(pos)*P_backward_l_pos */
void Chain::getTrans_aposteriori(vector<double>& trans, const vector<Data*>& data, vector < SegmClass * > & classes,const unsigned int _k,const unsigned int _pos)
{
	const unsigned int _Nclass = classes.size();
	
	double S=0.;
	
	for(unsigned int l=0 ; l<_Nclass ; l++){
		S+=(trans[l]=
			classes[_k]->getA(l)
			* classes[l]->getNoiseModel()->likelihood(data,_pos)
			* (*this->P_backward[l])[_pos]);
	}
			
	if(S<eps)
		for(unsigned int l=0 ; l<_Nclass ; l++)
			trans[l]=1./_Nclass;
	else
		for(unsigned int l=0 ; l<_Nclass ; l++)
			trans[l]/=S;
}


/** After use, you have to remove output Image X */
Image<unsigned int>* Chain::simulX(const vector < Data * > & data, vector < SegmClass * > & classes)
{
	const unsigned int _Nclass = classes.size();
	const unsigned int _size = data[0]->length();
	Image<unsigned int>* X = new Image<unsigned int>(data[0]->getwidth(),data[0]->getheight());
	
	// initialisation � la position 0
	vector<double> Init(_Nclass,0.);
	double S=0.;
	for(unsigned int k=0 ; k<_Nclass ; k++)
	{
		S+=(Init[k]=
			classes[k]->getP()
			* classes[k]->getNoiseModel()->likelihood(data,0)
			* (*this->P_backward[k])[0]);
	}
	// puis normalisation
	if(S>eps)
	for(unsigned int k=0 ; k<_Nclass ; k++)
			Init[k]/=S;
	else
	for(unsigned int k=0 ; k<_Nclass ; k++)
			Init[k]=1./static_cast<float>(_Nclass);
	
	// tirage al�atoire entre 0 et 1
	double rnd=(double)(rand()/(double)RAND_MAX);
	
	vector<double> G(_Nclass,0.);
	for(unsigned int k=0 ; k<_Nclass ; k++)
	{
		for(unsigned int l=0 ; l<k+1 ; l++)
			G[k]+=Init[l];

		(*X)[0]=_Nclass-1;

		for(int l=_Nclass-1 ; l>-1 ; l--)
			if(rnd<G[l])
				(*X)[0]=l;
	}

	// pour toutes les autres positions
	vector<double> T(_Nclass,0.);
	for(unsigned int pos=1 ; pos<_size ; pos++)
	{
		for(unsigned int k=0 ; k<_Nclass ; k++)
		{
			getTrans_aposteriori(T,data,classes,k,pos);
			G[k]=0;
			for(unsigned int l=0 ; l<k+1 ; l++)
				G[k]+=T[l];

			(*X)[pos]=_Nclass-1;
			rnd=(double)(rand()/(double)RAND_MAX);
				
			for(int l=_Nclass-1 ; l>-1 ; l--)
				if(rnd<G[l])
					(*X)[pos]=l;
		}
	}

	return X;	
}

/** Evaluate "a posteriori" joint probability : P(Xn=wk,Xn+1=wl | Y=y) */
dMatrix Chain::getP_joint_aposteriori(const vector < Data * > & data, vector < SegmClass * > & classes, const unsigned int _pos)
{
	const unsigned int _Nclass = classes.size();
	dMatrix _joint;
	for (unsigned int k=0 ; k<_Nclass ; k++)
		_joint.push_back(vector<double>(_Nclass,0.));
	
	double S=0.;		
	for (unsigned int i=0 ; i<_Nclass ; i++)
	for (unsigned int j=0 ; j<_Nclass ; j++)
		S+=(_joint[i][j]=getP_joint_aposteriori(data,classes,i,j,_pos));

	// somme pour la normalisation des proba conjointes
	for (unsigned int i=0 ; i<_Nclass ; i++)
	for (unsigned int j=0 ; j<_Nclass ; j++)
	{
		if (S>eps)
			_joint[i][j]/=S;
		else
			_joint[i][j]=1./(static_cast<float>(_Nclass)*static_cast<float>(_Nclass));
	}
	return _joint;
}

/** Evaluate "a posteriori" joint probability : P(Xn=wk,Xn+1=wl | Y=y) */
/*Image<double> * Chain::getP_joint_aposteriori(const vector < Data * > & data, vector < SegmClass * > & classes, const unsigned int _k, const unsigned int _l)
{
	Image<double>* psi = new Image<double>(data[0]->getwidth(),data[0]->getheight());
	
	const unsigned int _len = data[0]->length() -1;
	
	for (unsigned int pos=0 ; pos<_len ; pos++)
		(*psi)[pos]=this->getP_joint_aposteriori(data,classes,_k,_l,pos);

	return psi;
}
*/

/** Evaluate "a posteriori" joint probability : P(Xn=wk,Xn+1=wl | Y=y) for a position 
@remarks : Becarful, those values are not normalised Sum(k,l) must be = 1! -> use
getP_joint_aposteriori(data,classes,pos) */
double Chain::getP_joint_aposteriori(const vector < Data * > & data, vector < SegmClass * > & classes, const unsigned int _k, const unsigned int _l, unsigned int _pos)
{

	//const unsigned int _Nclass = classes.size();
	// pour la position demand�e
	double psi_k_l =
		(*P_forward[_k])[_pos]
		* classes[_k]->getA(_l)
		* classes[_l]->getNoiseModel()->likelihood(data,_pos+1)
		* (*P_backward[_l])[_pos+1];

/*		
	// somme pour la normalisation
	double S=0;
	for (unsigned int kk=0 ; kk<_Nclass ; kk++)
	for (unsigned int ll=0 ; ll<_Nclass ; ll++)
	{
		double psi =
			(*P_forward[kk])[_pos]
			* classes[kk]->getA(ll)
			* classes[ll]->getNoiseModel()->likelihood(data,_pos+1)
			* (*P_backward[ll])[_pos+1];
		S+=psi;
	}
	return ( (S>eps)?psi_k_l/S:1./(static_cast<float>(_Nclass)*static_cast<float>(_Nclass)) ) ;
	*/ return psi_k_l;
}


/** return marginal=P_forward*P_backward for one position */
double Chain::getP_marginal_aposteriori(const unsigned int k, const unsigned int _pos, const
unsigned int )const{
	return (*P_marginal[k])[_pos];
}

/** Browse minimum to initialise an a posteriori probability */
void Chain::init_aposteriori(const vector<Data*> &data, const vector<SegmClass*> &classes){
	this->browse(data,classes);
}
/** return a marginal Data object (Image) */
Data * Chain::getP_marginal_aposteriori(const unsigned short k){
	const Image<double>* const  ptk_forward = P_forward[k];
	const Image<double>* const ptk_backward = P_backward[k];
	Image<double>* const ptk_marginal = P_marginal[k];
	const unsigned int _size = P_marginal[k]->length();
	for(unsigned int pos=0 ; pos<_size ; pos++)
		ptk_marginal->setValue(pos,(ptk_forward->getValue(pos) * ptk_backward->getValue(pos)));
		
	return dynamic_cast<Data*>(P_marginal[k]);
}
/** browse forward and backward using external likelihood */
void Chain::browse(const vector < Data * > & ){
}
