/***************************************************************************
 init.cpp  -  description
 -------------------
 begin                : ven aoû 16 2002
 email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/

#include "init.h"
#include "segmclass.h"
#include "allNoiseModel.h"
#include "image.h"
#include "Math.h"

#include <fstream>

Init::Init() {
}
Init::~Init() {
}

/** Conditional Initalization with hypothesis of a Gaussian Noise */
void Init::condi(vector<Data*> _data, vector<SegmClass*> classes) {
	// verification que le vecteur de Data* correspond à des Image<double>*
	if (dynamic_cast<Image<double>*> (_data[0]) == 0)
		ERR("Init::condi : vector<Data*> != vector<Image<double>*>\n");

	// vecteur de pointeur d'objets Image
	vector<Image<double>*> data;
	for (unsigned int i = 0; i < _data.size(); i++)
		data[i] = dynamic_cast<Image<double>*> (_data[i]);

	// creation du bruit pour chaque classe
	for (int i = 0; i < SegmClass::getNclass(); i++)
		classes[i]->setNoiseModel(new Gauss);

	//cout << "\n-------Init condi\n";

	// pour chaque image i
	for (unsigned int i = 0; i < data.size(); i++) {
		// calcule les parametres de l'image entiere
		double moy = moyLocal(data[i], 0, data[i]->getwidth(), 0, data[i]->getheight());
		double ecart = ecartLocal(data[i], moy, 0, data[i]->getwidth(), 0, data[i]->getheight());

		// pose la moyenne de cette image à toutes les classes
		for (int j = 0; j < SegmClass::getNclass(); j++)
			dynamic_cast<Gauss *> (classes[j]->getNoiseModel())->setmean(i, moy);
		// pose l'ecarts-types de cette image à toutes les classes
		for (int j = 0; j < SegmClass::getNclass(); j++)
			dynamic_cast<Gauss *> (classes[j]->getNoiseModel())->setstddev(i, ecart);
	}

	// pose le déterminant à 1
	for (int j = 0; j < SegmClass::getNclass(); j++)
		classes[j]->getNoiseModel()->setdet(1);

	// pose la matrice de varcov à 1/0
	for (int i = 0; i < SegmClass::getNclass(); i++)
		for (int j = 0; j < SegmClass::getNdata(); j++)
			for (int k = 0; k < SegmClass::getNdata(); k++)
				classes[i]->getNoiseModel()->setvarcov(static_cast<int> (j == k), j, k);

	//cout << "-------Fin Init condi\n\n";

}

/** Preset initialization for one Weibull noise and all other Gaussian noise */
void Init::preset1(vector<Data*> , vector<SegmClass *> ) {
	/*	// verification que le vecteur de Data* correspond à des Image<double>*
	 if ( dynamic_cast<Image<double>*>(_data[0])==0 )
	 { ERR("Init::preset1 : vector<Data*> != vector<Image<double>*>\n");}

	 // vecteur de pointeur d'objets Image
	 vector<Image<double>*> data;
	 for (unsigned int i=0 ; i<_data.size() ; i++)
	 data[i]=dynamic_cast<Image<double>*>(_data[i]);

	 // creation du bruit pour chaque classe
	 for (int i=0 ; i<SegmClass::getNclass() ; i++)
	 classes[i]->setNoiseModel(new Gauss);

	 cout << "\n-------Init preset1\n";
	 classes[0]->setNoiseModel(dynamic_cast<NoiseModel*>(new Weibull));
	 for (int i=1 ; i<SegmClass::getNclass() ; i++)
	 classes[i]->setNoiseModel(dynamic_cast<NoiseModel*>(new Gauss));
	 // Affecter des valeurs d'initialization
	 cout << "-------Init preset1\n\n";
	 */
}

/** moyLocal : Used especially for kmeans function : evaluate mean on a local area
 * 
 * Config		:	
 * 	data					: pointer on image
 * 	idebut, ifin	: number of the line at the begging and at the end
 * 	jdebut, jfin	: number of the column at the begging and at the end
 *
 * Output	:
 *	mean
 */
double Init::moyLocal(const Image<double>* data, const int idebut, const int ifin, const int jdebut, const int jfin) {
	int nb = 0;
	double moy = 0;
	nb = 0;

	if (Data::mask != NULL) {
		// somme toutes les valeurs dans l'imagette, si non masquées
		for (int i = idebut; i < ifin; i++)
			for (int j = jdebut; j < jfin; j++) {
				if (Data::mask->get_pixel(i, j)) {
					nb++;
					moy += data->get_pixel(i, j);
				}
			}
		moy = moy / double(nb);
	} else {
		// somme toutes les valeurs dans l'imagette
		for (int i = idebut; i < ifin; i++)
			for (int j = jdebut; j < jfin; j++) {
				nb++;
				moy += data->get_pixel(i, j);
			}
		moy = moy / double(nb);
	}

	return (moy);
}

/** ecartLocal : Fonction propre a la fonction des kmeans : calcul l'écart-type dans une imagette
 Config		:
 const Image<double>* data : pointeur sur l'image d'entree
 const double moy : moyenne correspondante pour l'imagette
 const int	idebut, ifin : numéros de la ligne de départ et de fin
 const int jdebut, jfin : numéros de la colonne de départ et de fin
 Output :	double
 l'écart-type calculé
 */
double Init::ecartLocal(const Image<double>* data, const double moy, const int idebut, const int ifin, const int jdebut, const int jfin) {
	int nb = 0;
	double ecart = 0;

	if (Data::mask != NULL) {
		for (int i = idebut; i < ifin; i++)
			for (int j = jdebut; j < jfin; j++) {
				// somme des écarts à la moyenne au carré
				if (Data::mask->get_pixel(i, j)) {
					nb++;
					ecart += pow(moy - data->get_pixel(i, j), 2.);
				}
			}
		ecart /= double(nb);
		ecart = sqrt(ecart);
	} else {
		for (int i = idebut; i < ifin; i++)
			for (int j = jdebut; j < jfin; j++) {
				// somme des écarts à la moyenne au carré
				nb++;
				ecart += pow(moy - data->get_pixel(i, j), 2.);
			}
		ecart /= double(nb);
		ecart = sqrt(ecart);
	}

	return (ecart);
}

/**
 *
 * Comment :
 *	All must be allocated
 */
void Init::createSmall(vector<Image<double>*> data, vector<Image<double>*> small_moy, vector<Image<double>*> small_ecart, Image<bool> *small_mask/*=NULL*/) {
	const int width = data[0]->getwidth(), height = data[0]->getheight();
	const int pixW = static_cast<int> (width / small_moy[0]->getwidth()), pixH = static_cast<int> (height / small_moy[0]->getheight());
	const int NW = small_moy[0]->getwidth(), NH = small_moy[0]->getheight();
	const long _small_size = NW * NH;
	const int _Ndata = SegmClass::getNdata();

	// s'il y a un masque
	if (small_mask != NULL) {
		// Creation	du masque à la taille des images composées en imagettes
		// et initialisation à true
		//vector< vector< bool > > small_mask(N,vector<bool> (N,false));
		//		small_mask = new Image<bool>(NW,NH);
		for (int i = 0; i < _small_size; i++)
			small_mask->set_pixel(i, true);

		// On rejette l'imagette dès lors qu'un pixel lui appartenant est masqué
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++) {
				small_mask->set_pixel(static_cast<int> (i / pixH), static_cast<int> (j / pixW), small_mask->get_pixel(static_cast<int> (i / pixH),
						static_cast<int> (j / pixW)) && (Data::mask)->get_pixel(i, j));
			}
		//	vector<double> d(_Ndata,0);
		//	vector<double> e(_Ndata,0);

		for (int i = 0; i < width; i += pixH)
			for (int j = 0; j < height; j += pixW) {
				const int smalli = static_cast<int> (i / pixH), smallj = static_cast<int> (j / pixW);
				if (small_mask->get_pixel(smalli, smallj)) {
					for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
						double moy = 0;

						small_moy[cpt_Ndata]->set_pixel(smalli, smallj, moy = moyLocal(data[cpt_Ndata], i, i + pixH, j, j + pixW));
						small_ecart[cpt_Ndata]->set_pixel(smalli, smallj, ecartLocal(data[cpt_Ndata], moy, i, i + pixH, j, j + pixW));
					}
				}
			}
	} else {
		for (int i = 0; i < width; i += pixH)
			for (int j = 0; j < height; j += pixW) {
				const int smalli = static_cast<int> (i / pixH), smallj = static_cast<int> (j / pixW);
				for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
					double moy = 0;

					small_moy[cpt_Ndata]->set_pixel(smalli, smallj, moy = moyLocal(data[cpt_Ndata], i, i + pixH, j, j + pixW));
					small_ecart[cpt_Ndata]->set_pixel(smalli, smallj, ecartLocal(data[cpt_Ndata], moy, i, i + pixH, j, j + pixW));
				}
			}
	}

}

/** mindist : Search which centroide is the nearest of the local parameters
 * Config :
 * 	imagetteM 	: vector of local means
 * 	imagetteE		: vector of local std-devs
 * 	l						: number of the line
 * 	c 					: number of the column
 *
 * Output : int
 * 	numéro de la classe correspondant à la + petite distance
 */
int Init::mindist(const vector<Image<double>*> imagetteM, const vector<Image<double>*> imagetteE, const dMatrix centroideM, const dMatrix centroideE,
		const int l, const int c) {
	const unsigned int _Ndata = imagetteM.size();
	double *VImagette = new double[2 * _Ndata];
	double *VCentroide = new double[2 * _Ndata];
	double distmax = 1.e10;
	int k0 = 0;

	// création du vecteur imagette à comparer
	for (unsigned int i = 0; i < _Ndata; i++) {
		VImagette[2 * i] = imagetteM[i]->get_pixel(l, c);
		VImagette[2 * i + 1] = imagetteE[i]->get_pixel(l, c);
	}

	// comparaison avec les parametres des classes	
	for (int k = 0; k < SegmClass::getNclass(); k++) {
		// création du vecteur à comparer
		for (unsigned int i = 0; i < _Ndata; i++) {
			VCentroide[2 * i] = centroideM[k][i];
			VCentroide[2 * i + 1] = centroideE[k][i];
		}

		double dist = Mdist(VImagette, VCentroide, _Ndata * 2);

		// Recherche de la plus petite distance
		if (dist < distmax) {
			k0 = k;
			distmax = dist;
		}
	}

	delete[] VImagette;
	delete[] VCentroide;

	return k0;
}

/** k-mean method.
 Config :
 _data			: vecteur of config data
 classes		: vector of pointers on classes
 pixW =4		: nombre de pixels en longueur de l'imagette
 pixH =4		: nombre de pixels en hauteur de l'imagette
 Output :

 Comment :

 */

void Init::kmean(vector<Data*> _data, vector<SegmClass*> classes, const int pixW, const int pixH) {

	// verification que le vecteur de Data* correspond à des Image<double>*
	if (!_data[0]->isImageDouble()) {
		ERR("Init::kmean : vector<Data*> != vector<Image<double>*>\n");
	}

	// vecteur de pointeur d'objets Image
	vector<Image<double>*> data;
	for (unsigned int i = 0; i < _data.size(); i++)
		data.push_back(dynamic_cast<Image<double>*> (_data[i]));

	//cout << "\n-------Init kmean\n";

	const int _Ndata = data.size();
	const int K = SegmClass::getNclass();

	// nouvelles tailles des Images composées d'imagettes
	const int NW = data[0]->getwidth() / pixW;
	const int NH = data[0]->getheight() / pixH;

	vector<Image<double>*> small_moy(_Ndata);
	for (int i = 0; i < _Ndata; i++)
		small_moy[i] = new Image<double> (NW, NH);

	vector<Image<double>*> small_ecart(_Ndata);
	for (int i = 0; i < _Ndata; i++)
		small_ecart[i] = new Image<double> (NW, NH);

	Image<bool> *small_mask = NULL;

	// tableaux des centroides
	dMatrix centreM(K, vector<double> (_Ndata, 0));
	dMatrix centreE(K, vector<double> (_Ndata, 0));
	dMatrix centreOldM(K, vector<double> (_Ndata, 0));
	dMatrix centreOldE(K, vector<double> (_Ndata, 0));
	vector<int> nbre(K, 0);
	vector<double> dist(K, 0);

	// Au d�part, calcul des param�tres gaussiens de chaque imagette
	// recherche des extrema pour calculer un seuil d'arret

	// initialisation des premiers centroides
	vector<double> minM(_Ndata);
	vector<double> minE(_Ndata);
	vector<double> maxM(_Ndata);
	vector<double> maxE(_Ndata);

	if (Data::mask != NULL) {
		small_mask = new Image<bool> (NW, NH);
		Init::createSmall(data, small_moy, small_ecart, small_mask);


		// initialisation des premiers centroides (premier pixel non masqué)
		for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
			int i=0;
			int j=0;
			while(!(small_mask->get_pixel(i, j)) && i<NH-1){
				j=0;
				while(!(small_mask->get_pixel(i, j)) && j<NW-1){
					j++;
				}
				if(!(small_mask->get_pixel(i, j)))
					i++;
			}
			minM[cpt_Ndata] = maxM[cpt_Ndata] = small_moy[cpt_Ndata]->get_pixel(i, j);
			minE[cpt_Ndata] = maxE[cpt_Ndata] = small_ecart[cpt_Ndata]->get_pixel(i, j);
		}

		for (int i = 0; i < NH; i++)
			for (int j = 0; j < NW; j++) {
				if (small_mask->get_pixel(i, j)) {
					for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
						//Recherche le min et max sur les moyennes des imagettes pour chaque bande
						if (small_moy[cpt_Ndata]->get_pixel(i, j) < minM[cpt_Ndata]){
							minM[cpt_Ndata] = small_moy[cpt_Ndata]->get_pixel(i, j);
						}
						else if (small_moy[cpt_Ndata]->get_pixel(i, j) > maxM[cpt_Ndata])
							maxM[cpt_Ndata] = small_moy[cpt_Ndata]->get_pixel(i, j);

						//Recherche le min et max sur les ecarts-type des imagettes pour chaque bande
						if (small_ecart[cpt_Ndata]->get_pixel(i, j) < minE[cpt_Ndata]){
							minE[cpt_Ndata] = small_ecart[cpt_Ndata]->get_pixel(i, j);
						}
						else if (small_ecart[cpt_Ndata]->get_pixel(i, j) > maxE[cpt_Ndata])
							maxE[cpt_Ndata] = small_ecart[cpt_Ndata]->get_pixel(i, j);
					}
				}
			}
	} else {
		Init::createSmall(data, small_moy, small_ecart);

		// initialisation des premiers centroides
		for (int i = 0; i < _Ndata; i++) {
			minM[i] = maxM[i] = small_moy[i]->get_pixel(0, 0);
			minE[i] = maxE[i] = small_ecart[i]->get_pixel(0, 0);
		}

		for (int i = 0; i < NH; i++)
			for (int j = 0; j < NW; j++) {
				for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
					//Recherche le min et max sur les moyennes des imagettes pour chaque bande
					if (small_moy[cpt_Ndata]->get_pixel(i, j) < minM[cpt_Ndata])
						minM[cpt_Ndata] = small_moy[cpt_Ndata]->get_pixel(i, j);
					else if (small_moy[cpt_Ndata]->get_pixel(i, j) > maxM[cpt_Ndata])
						maxM[cpt_Ndata] = small_moy[cpt_Ndata]->get_pixel(i, j);

					//Recherche le min et max sur les ecarts-type des imagettes pour chaque bande
					if (small_ecart[cpt_Ndata]->get_pixel(i, j) < minE[cpt_Ndata])
						minE[cpt_Ndata] = small_ecart[cpt_Ndata]->get_pixel(i, j);
					else if (small_ecart[cpt_Ndata]->get_pixel(i, j) > maxE[cpt_Ndata])
						maxE[cpt_Ndata] = small_ecart[cpt_Ndata]->get_pixel(i, j);
				}
			}
	}


	// r�partition proportionnelle des centro�des initiaux
	for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++){
		for (int k = 0; k < K; k++) {
			centreOldM[k][cpt_Ndata] = centreM[k][cpt_Ndata] = minM[cpt_Ndata] + k * (maxM[cpt_Ndata] - minM[cpt_Ndata]) / float(K);
			centreOldE[k][cpt_Ndata] = centreE[k][cpt_Ndata] = minE[cpt_Ndata] + k * (maxE[cpt_Ndata] - minE[cpt_Ndata]) / float(K);
		}
	}

	//Calcul d'un seuil a 0.01% pour l'arret des kmoyennes
	double seuilM = 0;
	double seuilE = 0;
	for (int i = 0; i < _Ndata; i++) {
		seuilM += fabs(maxM[i] - minM[i]) * 0.0001;
		seuilE += fabs(maxE[i] - minE[i]) * 0.0001;
	}

	// D�placement des centroides vers des imagettes représentatives

	// variations des centres
	double centreVarM = seuilM + 1.;
	double centreVarE = seuilE + 1.;
	bool converge = false;

	// tant que les centroides n'ont pas beaucoup bougé ou qu'on n'a pas déjà fait 100 itérations
	int it = 0;
	while ((converge == false) && it < 100) {
		//  while((centreVarM>seuilM || centreVarE>seuilE) && it<100){
		it++;
		nbre.assign(K, 1); /// Initialise toute les valeurs à 1

		if (small_mask != NULL) {
			//Pour chaque imagette non masquée
			for (int l = 0; l < NH; l++)
				for (int c = 0; c < NW; c++) {
					if (small_mask->get_pixel(l, c)) {
						// On evalue la distance euclidienne (sur la moyenne et l'ecart-type)
						// entre l'imagette et chaque classe, on garde la classe la + proche
						int k0 = mindist(small_moy, small_ecart, centreM, centreE, l, c);

						// R�initialisation des parametres de la moyenne et Ecart-type
						// en ajoutant la nouvelle imagette dans la classe choisie
						for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
							centreM[k0][cpt_Ndata] = (small_moy[cpt_Ndata]->get_pixel(l, c) + nbre[k0] * centreM[k0][cpt_Ndata]) / (nbre[k0] + 1.);
							centreE[k0][cpt_Ndata] = (small_ecart[cpt_Ndata]->get_pixel(l, c) + nbre[k0] * centreE[k0][cpt_Ndata]) / (nbre[k0] + 1.);
						}
						nbre[k0]++;

					} // fin si dans le masque
				} // fin pour chaque imagette
		} else {
			for (int l = 0; l < NH; l++)
				for (int c = 0; c < NW; c++) {
					//On evalue la distance euclidienne (sur la moyenne et l'ecart-type)
					//entre l'imagette avec chaque classe, on a garde la classe la + proche
					int k0 = mindist(small_moy, small_ecart, centreM, centreE, l, c);

					//Réinitialisation des parametres de la moyenne et Ecart-type
					//en ajoutant la nouvelle imagette dans la classe choisie
					for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
						centreM[k0][cpt_Ndata] = (small_moy[cpt_Ndata]->get_pixel(l, c) + nbre[k0] * centreM[k0][cpt_Ndata]) / (nbre[k0] + 1.);
						centreE[k0][cpt_Ndata] = (small_ecart[cpt_Ndata]->get_pixel(l, c) + nbre[k0] * centreE[k0][cpt_Ndata]) / (nbre[k0] + 1.);
					}
					nbre[k0]++;

				} // fin pour chaque imagette
		}

		// calcul de l'evolution des centres
		centreVarM = 0.;
		centreVarE = 0.;
		for (int k = 0; k < K; k++)
			for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
				centreVarM += fabs(centreOldM[k][cpt_Ndata] - centreM[k][cpt_Ndata]);
				centreVarE += fabs(centreOldE[k][cpt_Ndata] - centreE[k][cpt_Ndata]);
				centreOldM[k][cpt_Ndata] = centreM[k][cpt_Ndata];
				centreOldE[k][cpt_Ndata] = centreE[k][cpt_Ndata];
			}

		// Obtention du crit�re de convergence :
		converge = (centreVarM <= seuilM && centreVarE <= seuilE);
	}

	if (small_mask != NULL)
		delete small_mask;

	for (int i = 0; i < _Ndata; i++)
		delete small_moy[i];
	for (int i = 0; i < _Ndata; i++)
		delete small_ecart[i];

	// creation du bruit pour chaque classe
	for (int i = 0; i < SegmClass::getNclass(); i++)
		classes[i]->setNoiseModel(new Gauss);

	// Affectation des valeurs trouvées aux variables de classes
	for (int k = 0; k < K; k++) {
		for (int cpt_Ndata = 0; cpt_Ndata < _Ndata; cpt_Ndata++) {
			dynamic_cast<Gauss*> (classes[k]->getNoiseModel())->setmean(cpt_Ndata, centreM[k][cpt_Ndata]);
			dynamic_cast<Gauss*> (classes[k]->getNoiseModel())->setstddev(cpt_Ndata, centreE[k][cpt_Ndata]);
		}
	}

	for (int k = 0; k < K; k++)
		classes[k]->getNoiseModel()->init_corr();
}

/** Evaluate if the selected class fits more of each known NoiseModel.  */
NoiseModel* Init::bestNoiseModel(vector<SegmClass*> ) {
	// pour l'instant uniquement des objets Gaussien
	return (new Gauss);
}

/** Initialize 'a priori' parameters (P_k, a_ij)
 Config :
 SegmClass classes[] : tableau des classes dont dépendent les parmetres a priori
 Output : none
 */
void Init::apriori(vector<SegmClass*> classes) {
	const int _Nclass = classes.size();

	// meme proba pour toutes les classes : 1/n
	for (int i = 0; i < _Nclass; i++) {
		classes[i]->setP(1. / static_cast<float> (_Nclass));
		classes[i]->makeTrans();
		//		PPost[i]=P[i];
	}

	// initialisation des proba de passage
	// 3/4 sur la diagonale, 1/4n ailleurs
	for (int i = 0; i < _Nclass; i++)
		for (int j = 0; j < _Nclass; j++)
			classes[i]->setA(j, 0.25 / (_Nclass - 1.));

	for (int i = 0; i < _Nclass; i++)
		classes[i]->setA(i, 0.75);

}

/** Use of the Fuzzy k-mean method and estimate number of classes */
void Init::FuzzyKmean(vector<Data*> , vector<SegmClass*> , const int, const int) {
	cout << "FuzzyKmean\n";
}

/** bestNclass : Evaluate best number of classes for images 
 * @param	images		: images to study 
 *
 * @return 	number of classes evaluated
 */
int Init::bestNclass(vector<Data *> ) {
	//	static_cast<Image<double> *>(images[i])
	return MAX_NB_CLASS;
}

/** HilbertPeano Chain scan for images */
Image<unsigned int> * Init::HilbertPeano(const string _path, vector<Data*>& data) {
	stringstream pathfile("");
	pathfile << _path << "/scan/X" << data[0]->getwidth() << ".bin";

	const int fin = data[0]->length();

	Image<unsigned int> * scan = new Image<unsigned int> (data[0]);

	ifstream file;
	file.open(pathfile.str().c_str(), ios::in);

	if (!file) {
		cerr << "Warning : Init::HilbertPeano : Could not open file " << pathfile.str() << endl;
		cerr << "\t Do not use special scanning, read images line by line\n";
		return NULL;
	}

	for (int pos = 0; pos < fin; pos++) {
		unsigned int val;
		file.read((char *) &val, sizeof(int));
		(*scan)[pos] = val - 1;
	}

	file.close();

	for (unsigned int i = 0; i < data.size(); i++) {
		Image<double> d(data[i]);
		for (int pos = 0; pos < fin; pos++)
			data[i]->setValue((*scan)[pos], d.getValue(pos));
		//		d.show();
	}

	//	scan.show("xv -preset 4");
	return scan;
}
