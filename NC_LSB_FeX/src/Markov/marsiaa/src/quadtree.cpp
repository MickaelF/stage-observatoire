/***************************************************************************
                          quadtree.cpp  -  description
                             -------------------
    begin                : lun ao� 19 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "quadtree.h"
#include "data.h"
#include "segmclass.h"
#include "pyramid.h"
#include "noisemodel.h"
#include "gauss.h"




QuadTree::QuadTree() : Nscale(0) {
}

QuadTree::~QuadTree(){
	if (!P_forward.empty()){
		for (unsigned char k=0 ; k<P_forward.size() ; k++)
			delete P_forward[k];
		P_forward.clear();
	}
/*	
	if (!P_joint.empty()){
		for (unsigned char k=0 ; k<P_joint.size() ; k++)
			for (unsigned char l=0 ; l<P_joint[k].size() ; l++)
				delete P_joint[k][l];
		P_joint.clear();
	}
*/
	if (!P_marginal.empty()){
		for (unsigned char k=0 ; k<P_marginal.size() ; k++)
			delete P_marginal[k];
		P_marginal.clear();
	}
	
}


/** Update "a priori" parameters. */
void QuadTree::update_apriori(){
}


/** init : Initialize and allocate memory for private parameters 
 * @param data : tableau des donn�es entr�es
 * @param	classes : tableau des classes de segmentation
 *
*/
void QuadTree::init(const vector < Data * > & data, const vector < SegmClass * > & classes){
	
	const unsigned char _Nclass = classes.size();
	/* Creation des objets Pyramid locales avec une base telle l'image d'entree */
//	for (unsigned char k=0 ; k<_Nclass ; k++)
//		P_joint.push_back( vector<Pyramid<double>*>(_Nclass) );
		
	for (unsigned char k=0 ; k<_Nclass ; k++)
	{
		P_forward.push_back(new Pyramid<double>(data[0]));
		P_marginal.push_back(new Pyramid<double>(data[0]));
//		for (unsigned char j=0 ; j<_Nclass ; j++)
//			P_joint[k][j] = new Pyramid<double>(data[0]);
	}
	
	Nscale=P_forward[0]->getNscale();
	
	for (unsigned char k=0 ; k<_Nclass ; k++)
		P_apriori.push_back(vector<double>(Nscale,0.));
	
}


/** browse forward and backward */
void QuadTree::browse(const vector < Data * > & data, const vector < SegmClass * > & classes) {
//	init(data,classes);
	browse_forward(data,classes);
}


/** browse_forward : evaluate forward probability for a quadtree : evaluate likelihood in full resolution
  	and propagate the probability along the quadtree going up  */
void QuadTree::browse_forward(const vector < Data * > & data, const vector < SegmClass * > & classes){

	if (!data[0]->isImage())
		ERR("QuadTree::browse_forward : could not word on data != Image\n");
	if (P_forward.empty())
		this->init(data,classes);

cout<</*"Start */"browse forward\n";

	const unsigned char _Nclass = classes.size();
	const unsigned char lastClass = _Nclass-1;
	const unsigned char lastScale = Nscale-1;
	
	// taille de l'image de base
	const unsigned int _len = data[0]->length();
	
//	for(char k=lastClass ; k>=0 ; k--)
//			cout << "p"<<k<<" = "<<classes[k]->getP()<<endl;
	
	/* calcul des proba apriori */
	// initialisation pour la + petite r�solution (Nscale-1) ou il n'y a plus qu'un pixel
	for(char k=lastClass ; k>=0 ; k--)
			P_apriori[k][lastScale]=classes[k]->getP();

	for (char e=lastScale-1 ; e>=0 ; e--)
	{
		for(char k=lastClass ; k>=0 ; k--)
		{
			P_apriori[k][e]=0.;
			for (char l=lastClass ; l>=0 ; l--) 
				P_apriori[k][e] += classes[l]->getA(k) * P_apriori[l][e+1];
//		cout <<"P"<<(int)k<<" = "<<P_apriori[k][e]<<endl;
		}
	}

	/* calcul des proba forward */
	// initialisation sur la base de la pyramid des P_forward (echelle=0)
	for(int pos=_len-1 ; pos>=0 ; pos--){
		double S=0.;
		for(char k=lastClass ; k>=0 ; k--)
			S+=(
			(*P_forward[k])[pos]=P_apriori[k][0]*classes[k]->getNoiseModel()->likelihood(data,pos)
			);

		// normalisation
		if (S>eps)
//			for(unsigned int k=0 ; k<_Nclass ; k++)
			for(char k=lastClass ; k>=0 ; k--)
				(*P_forward[k])[pos]/=S;
		else
//			for(unsigned int k=0 ; k<_Nclass ; k++)
			for(char k=lastClass ; k>=0 ; k--)
			{
				(*P_forward[k])[pos]=1./static_cast<float>(_Nclass);
//				cout << "Limit : P_forward = 1/N\n";
			}
	}

		
	// pour les autres positions
//	unsigned int posPyramid=_len-1;
	for (unsigned int e=1 ; e<Nscale ; e++) {
		vector<double> p(_Nclass,0.);
		for(char k=lastClass ; k>=0 ; k--)
			p[k]=P_apriori[k][e];
		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
		const unsigned int _sizeResolution = static_cast<unsigned int>(pow(4,lastScale-e));
		for(int pos=_sizeResolution-1 ; pos>=0 ; pos--){
//			posPyramid++;
			double S=0.;
//			for(unsigned int k=0 ; k<_Nclass ; k++) 
			for(char k=lastClass ; k>=0 ; k--)
			{
			if (p[k]>eps)
			{
				double t1=0.,t2=0.,t3=0.,t4=0.; // les 4 fils
//				for (unsigned int l=0 ; l<_Nclass ; l++)
				for(char l=lastClass ; l>=0 ; l--)
				{
					// calcul de la transition inverse (identique pour les 4 fils)
					// pointeur sur la pyramid de P_forward courante
					Pyramid<double> const * const P_forward_l = P_forward[l];
					double Ti = getInv(classes,k,l,e-1);
					// calculs des fils
					t1 += P_forward_l->get_child1(e,pos) * Ti;
					t2 += P_forward_l->get_child2(e,pos) * Ti;
					t3 += P_forward_l->get_child3(e,pos) * Ti;
					t4 += P_forward_l->get_child4(e,pos) * Ti;
				}
				P_forward[k]->set_pixel(e,pos , pow(p[k],-3.)*t1*t2*t3*t4);
//				(*P_forward[k])[posPyramid] = pow(P_apriori[k][e],-3)*t1*t2*t3*t4;
			}
			else // if priori trop petit
				P_forward[k]->set_pixel(e,pos,0.);
//				(*P_forward[k])[posPyramid]=0.;
				
			// somme sur k pour 1 pos pour la normalisation
			//S+=(*P_forward[k])[posPyramid];
			S+=P_forward[k]->get_pixel(e,pos);
			}
			
			for(char k=lastClass ; k>=0 ; k--)
//			for(unsigned int k=0 ; k<_Nclass ; k++)
			{
				if (S>eps)
					P_forward[k]->get_pixel(e,pos)/=S;
//					(*P_forward[k])[posPyramid]/=S;
				else
					ERR("Error : QuadTree::browse_forward : value too low, may be problem in algorithm\n");
			}
		}
	}
	
//for(unsigned int k=0 ; k<_Nclass ; k++)
//	P_forward[k]->getbase()->show();
/*	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		cout << "nouveau P"<<k<<" = "<<P_forward[k]->get_pixel(Nscale-1,0)<<endl;
	}
*/

//cout << "End browse forward\n";
}

/** browse_backward : evaluate joint probability for a quadtree */
void QuadTree::browse_backward(const vector < SegmClass * > & classes) {
	
	const unsigned int _Nclass = classes.size();
	const unsigned int lastClass = _Nclass-1;
cout << /*Start */"browse backward\n";
	
	const unsigned int baseScale = P_forward[0]->getBaseScale();
	// Nscale -2 pour avoir le num de l'avant dernier etage car on utilise le pere alors les calculs ne peuvent etre sur le dernier etage
	const unsigned int lastScale = Nscale-2 ; 
	

	// initialisation des marginales avec les forwards (partielles) a la pointe de la pyramide
	for(char k=lastClass ; k>=0 ; k--)
	{
		const unsigned int pic = lastScale+1;
		P_marginal[k]->set_pixel(pic,0,P_forward[k]->get_pixel(pic,0));
//		cout << P_marginal[k]->get_pixel(lastScale,0) << endl;
	}

	for (int e=lastScale ; e>=0 ; e--){
		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
		const unsigned int _sizeResolution = pow(4,baseScale-e);
		for(int pos=_sizeResolution-1 ; pos>=0 ; pos--)
		{
			vector<double> S(_Nclass,0.);
			for (char l=lastClass ; l>=0 ; l--) 
			  {
				double p = P_marginal[l]->get_father(e,pos);
				for(char k=lastClass ; k>=0 ; k--) 
				{
					S[k]+=getP_joint_aposteriori(classes,l,k,e,pos,p);
				}
			}
			for(char k=lastClass ; k>=0 ; k--)
				P_marginal[k]->set_pixel(e,pos,S[k]);

		}
	}

//	for(unsigned int k=0 ; k<_Nclass ; k++)
//		delete P_marginal[k];

//cout << "End browse backward\n";

}


/** update_dataDriven : Update Data-driven parameters
Config :
	data		: vector of data to use
	classes	:	vector of segmented classes, to update their noise parameters

Output :

Comment :
	data must be an Image<double>
*/
void QuadTree::update_dataDriven(const vector<Data *> & data, vector<SegmClass*> & classes)
{
ERR( "QuadTree::update_dataDriven : ERROR : shoulnot be here!\n");
}


/** getP_apriori :get a priori probability
 *
 * Config :
 *		_k					: number of the class to evaluate probability
 *		_scale			: scale of the apriori
 *
 * Comment :
 *		the apriori probability is the same at each pixel of one scale because of Quadtree.
 *		the last scale (where there is only 1 pixel) is initialiszed with Pk
 */
double QuadTree::getP_apriori(const unsigned int& _k, const unsigned int& _scale) const
{
	return P_apriori[_k][_scale];
}


/** Evaluate Inverse Transition */
double QuadTree::getInv(const vector<SegmClass*>& classes, const unsigned int &_k, const unsigned int &_l,const unsigned int &_i) const{
	double p = getP_apriori(_l,_i);
	if (p>eps)
		return ( classes[_k]->getA(_l) * getP_apriori(_k,_i+1) / p );
	else
		return ( 1./static_cast<float>(classes.size()) );
}


/** getP_joint_aposteriori :get joint probability a posteriori
 *
 * @param	classes			: vector of classes of segmentation
 * @param		_k, _l			: couple of classes to evaluate probability
 * @param		_e, _pos		: position in the quadtree (scale, and position in the image in this scale)
 * @param		_fatherMarginal : marginal probability aposteriori of the father (scale+1)
 *
 * @return joint probability a posteriori for this position
 *
 *
 */
double QuadTree::getP_joint_aposteriori(const vector<SegmClass*>& classes, const unsigned int _k, const unsigned int _l, const unsigned int _e, const unsigned int _pos, 
const double _fatherMarginal) const{ 	// on a besoin de proba � l'�chelle sup�rieur
	if (_e>=Nscale)
		ERR("QuadTree::getP_joint_aposteriori : could not get joint probability for this scale (the last or more) = " << _e << "\n");
	const unsigned int _Nclass = classes.size();

	// proba a priori � l'�chelle suivante
	double pnext = getP_apriori(_k,_e+1);
	// pointeur vers la classe interress�e
	SegmClass * class_k = classes[_k];

	double S=0.;
	vector<double> psi(_Nclass,0.);
	for (unsigned int kk=0 ; kk<_Nclass ; kk++){
		double p = getP_apriori(kk,_e);
		S+=(
		psi[kk] = (p>eps)?(
			class_k->getA(kk)
			* pnext
			* P_forward[kk]->get_pixel(_e,_pos)
			/ p) : 0.
		);
	}
	
//	cout << "psi "<<_k<<"->"<<_l<<" = "<<psi[_l] << endl;
//	cout << "/S(" <<S<<") = " << psi[_l]*_fatherMarginal/S<<endl;
	
	if (S>eps)
		return(psi[_l]*_fatherMarginal/S);
	else
		return 0.;
}


/** getP_joint_aposteriori :get joint probability a posteriori
 *
 * Config :
 *		classes			: vector of classes of segmentation
 *		_k, _l			: couple of classes to evaluate probability
 *
 * Output :
 *		Pyramid<double>* which is allocated and contains the joint probability a posteriori
 *
 * Comment :
 *		if you call this function, you have to delete the return object after used
 *
 */
/*Pyramid<double>* QuadTree::getP_joint_aposteriori(const vector < SegmClass * > & classes, const unsigned int _k, const unsigned int _l){
	// creation de l'objet retourn� de la meme taille que les autres Pyramid : P_forward
	Pyramid<double>* Pjoint = new Pyramid<double>( P_forward[0] );

	for (unsigned int e=0 ; e<Nscale-1 ; e++) {
		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
		const unsigned int _sizeResolution = static_cast<unsigned int>(pow(4.,Nscale-1-e));
		for(unsigned int pos=0 ; pos<_sizeResolution ; pos++)
			Pjoint->set_pixel(e,pos,getP_joint_aposteriori(classes,_k,_l,e,pos,P_marginal[_k]->get_father(e,pos)));
	}

	return Pjoint;
}
*/

/** return marginal for one position (in a Pyramid) */
/*double QuadTree::getP_marginal_aposteriori(const unsigned int k, const unsigned int _pos)const{
	return (*P_marginal[k])[_pos];
}
*/

/** No descriptions */
Pyramid<unsigned char> * QuadTree::simulX(vector < SegmClass * > & classes)
{
	const unsigned char _Nclass = classes.size();
	const unsigned char lastClass = _Nclass-1;
	const unsigned int baseScale = P_forward[0]->getBaseScale();
	// Nscale -2 pour avoir le num de l'avant dernier etage car on utilise le pere alors les calculs ne peuvent etre sur le dernier etage
	const unsigned int lastScale = Nscale-2 ; 

	Pyramid<unsigned char>* X = new Pyramid<unsigned char>(P_forward[0]->getwidth(),P_forward[0]->getheight());

	// tirage al�atoire entre 0 et 1
	double rnd=(double)(rand()/(double)RAND_MAX);

	vector<double> G(_Nclass,0.);
	// initialisation � la classe 0 avec la position 0
	G[0] = P_forward[0]->get_pixel(lastScale+1,0);
	for(unsigned char k=1 ; k<_Nclass ; k++)
		G[k]=G[k-1]+P_forward[k]->get_pixel(lastScale+1,0);
		
	// initialisation de la carte de segmentation tout en haut de la pyramid
	X->set_pixel(lastScale+1,0,lastClass);
	for(char k=lastClass-1 ; k>=0 ; k--)
		if(rnd<G[k])
			X->set_pixel(lastScale+1,0,k);

	
	// pour toutes les autres positions
	for (int e=lastScale ; e>=0 ; e--)
	{
		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
		const unsigned int _sizeResolution = pow(4,baseScale-e);
		for(int pos=_sizeResolution-1 ; pos>=0 ; pos--)
		{
			// Etiquette p�re de cette position
			unsigned char Xfather = X->get_father(e,pos);
			
			rnd=(double)(rand()/(double)RAND_MAX);
			// initialisation comme si la classe 0
			G[0] = 
				P_forward[0]->get_pixel(e,pos)
				* getInv(classes,Xfather,0,e) ;
			// pour les autres classes
			for (unsigned char k=1 ; k<_Nclass ; k++)
				G[k] = 
					G[k-1] 
					+ P_forward[k]->get_pixel(e,pos)
					* getInv(classes,Xfather,k,e) ;
			
			X->set_pixel(e,pos,lastClass);
			for (char k=lastClass-1 ; k>=0 ; k--)
			{
				// normalisation
				if (G[lastClass]>eps)
					G[k] /= G[lastClass];
				else
					G[k] = 1./static_cast<float>(_Nclass);
					
				// choix de l'�tiquette
				if (rnd<G[k])
					X->set_pixel(e,pos,k);
				
			}
		}
	}
	
//	X->getbase()->show("xv");
	return X;	
}

/** getP_marginal_aposteriori : Evaluate the marginal a posteriori : P(Xs=wk/Y) = Sumj P_joint 
 * @param	_k : class of the probability
 * @param	_scale : scale in the Pyramid
 * @param	_pos : position in the Image at the scale _scale
 *
 * @return
 *		double value of the marginal a posteriori probability
 *
 * @remarks
 * 		used by EMQuadTree, ICEQuadTree and segmentation 
 *
*/
double QuadTree::getP_marginal_aposteriori(const unsigned int _k, const unsigned int _pos, const unsigned int _scale) const {
	return P_marginal[_k]->get_pixel(_scale,_pos);
}

/** return a marginal probability Data Object (Image) */
Data * QuadTree::getP_marginal_aposteriori(const unsigned short k){
	return P_marginal[k]->getbase();
}


/** Browse minimum to initialise an "a posteriori" probability */
void QuadTree::init_aposteriori(const vector < Data * > & data, const vector < SegmClass * > & classes){
	this->browse_forward(data,classes);
	this->browse_backward(classes);
}

/** browse forward and backward using external likelihood */
void QuadTree::browse(const vector < Data * > & ){
}
