/***************************************************************************
                          segmentation.cpp  -  description
                             -------------------
    begin                : mar jan 14 2003
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "segmentation.h"
#include "data.h"
#include "image.h"
#include "pyramid.h"
#include "segmclass.h"
#include "markovmodel.h"
#include "noisemodel.h"
#include "gauss.h"
#include <exception>
#include "quadtree.h"


Segmentation::Segmentation(){
}
Segmentation::~Segmentation(){
}


/** MPM : Marginal a Posteriori Mode 
 * Config :
 *	_data			: vector of a posterirori data (xsi=alpha*beta)
 *	classes		: vector of SegmClass
 *
 * Output :
 *	true if no problem
 *	false else
 *
 * Comment :
 *		map must be allocated
*/

vector<double> Segmentation::MPM(Data* map, MarkovModel* _markov,const vector<Data*>& data, const vector<SegmClass*>& classes)
{
	const unsigned int _Nclass = classes.size();

	double max;
	unsigned int Kmax=0;

	unsigned int len=map->length();
	int totalpos=0;

	const unsigned int baseScale = map->getBaseScale();
	const unsigned int lastScale = map->getNscale();
  	
	for (unsigned int e=0 ; e<lastScale ; e++)
	{
		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
		const unsigned int _sizeResolution = pow(4,baseScale-e);
		for(unsigned int pos=0 ; pos<_sizeResolution ; pos++, totalpos++)
	 	{
 			// init avec la classe 0
	 		max=_markov->getP_marginal_aposteriori(0,pos,e);
 			Kmax=0;
	 		// pour toutes les autres classes
	 		for(unsigned int k=1 ; k<_Nclass ; k++){
 				double current = _markov->getP_marginal_aposteriori(k,pos,e);
		   
 				// recherche de la classe qui maximise
 				if(current>max)
	 			{
 					max=current;
 					Kmax=k;
 				}
 			}
			
	 		map->setValue(totalpos,Kmax); // MP[pos]=Kmax+1;
 		}
	}
	
	// calcul des proba a postriori au pied de la pyramid (ou image)
	len=data[0]->length();

	vector<double> PPost(_Nclass,0.);
	for(unsigned int k=0 ; k<_Nclass ; k++)
	{
		double P=0;
		for(unsigned int pos=0 ; pos<len ; pos++){
			if(static_cast<unsigned int>(map->getValue(pos))==k)
				P++;
		}
//		cout << P/static_cast<float>(len) << endl;
//		classes[k]->setP(P/static_cast<float>(len));
		PPost[k]=P/static_cast<float>(len);
	}
	
	return PPost;
	
}




