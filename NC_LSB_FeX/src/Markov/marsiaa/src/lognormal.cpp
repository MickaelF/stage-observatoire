/***************************************************************************
                          lognormal.cpp  -  description
                             -------------------
    begin                : lun mar 24 2003
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "lognormal.h"
#include "data.h"
#include "image.h"
#include "pyramid.h"
#include "segmclass.h"
#include "Math.h"
#include <math.h>
template <typename TType> class Image;

bool LogNormal::MinLocal = false;

LogNormal::LogNormal(){
	if (Ndata>0)
	{
		// initialise les tableaux à 0
		min.assign(Ndata,0.);
		shape.assign(Ndata,0.);
		scale.assign(Ndata,0.);
	}
	else ERR("LogNormal::LogNormal : Can't create LogNormal object without size of data.\n");
}

LogNormal::~LogNormal(){
}
 
LogNormal::LogNormal (const NoiseModel& _copie) : NoiseModel(_copie) {
	min.assign(Ndata,0.);
	scale.assign(Ndata,0.);
	shape.assign(Ndata,0.);
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
	//cout << "G->Log : " << m1[i] << " " << m2[i] << endl;
		scale[i] = log(m1[i]*m1[i]/sqrt(m1[i]*m1[i]+m2[i]));
		shape[i] = log((m1[i]*m1[i]+m2[i])/(m1[i]*m1[i]));
	//cout << "G->Log : " << min[i] << " " << scale[i] << " " << shape[i] << endl;
	}
}

LogNormal::LogNormal (const LogNormal& _copie) : NoiseModel(_copie) {
	min=_copie.min;
	shape=_copie.shape;
	scale=_copie.scale;
}

/** operator =  */
LogNormal& LogNormal::operator = (const LogNormal& _copie){
	if (this != &_copie)
	{
		min=_copie.min;
		shape=_copie.shape;
		scale=_copie.scale;
	}
	return *this;
}

bool LogNormal::isLogNormal()const{
	return true;
}

/** Evaluate statistical likelihood */
double LogNormal::likelihood(const vector < double > & _values)
{
	
	if(Ndata!=_values.size())
		ERR("LogNormal::likelihood : could not evaluate likelihood, sizes of data and noise are not similar\n");

	// calcul du vecteur des données dans l'espace décorrélé
	vector<double> Z(Ndata,0.);
	for (unsigned int i=0 ; i<Ndata ; i++)
		for (unsigned int c2=0 ; c2<i+1 ; c2++)
			Z[i] += decorr[i][c2] * _values[i];

	vector<double> M(Ndata,0);
	// recale les donnees par rapport a min
	// si un seul est inferieur au min -> vraissemblance = 0
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		M[i] = Z[i]-min[i];
		if (M[i]<=0)
			return 0.;
	}

	
	// produit des vraissemblances individuelles
	double res=1.;
	for (unsigned	int i=0 ; i<Ndata ; i++)
	{
		double c = 1./( sqrt(2*PI) * shape[i] * M[i]);
		double e = ( log(M[i]) - scale[i] ) / shape[i];
		res *= decorr[i][i]*c*exp(-0.5*e*e) ;
	}
	
	return res;
}


/** Update data driven */
void LogNormal::update_dataDriven(const vector < Data * > & data, const Data * pt_weight)
{
	/* Mise a jour des parametres de decorrelations */
	
	// pour chaque image en entrée
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		/* Mise a jour des parametres de la Lognormale */
		evalmin(i,data[i],pt_weight);
		evalscale(i,data[i],pt_weight);
		evalshape(i,data[i],pt_weight);
	}

}

/** Evaluate and modify parameter min (weighted) with Maximum Likelihood method. */
double LogNormal::evalmin(const unsigned int ndata, const Data * data, const Data * weight){
	// vérification que les données sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("LogNormal Error : Cannot eval min because sizes of images and weights are not similar.\n");

	// par securite on prend un peu en dessous (car pas definit pour y~=min)
	double step = 1.;//0.005*(data->getmax()-data->getmin());
		
	// minlocal = min global (idem pour toutes les classes)
	if (!this->MinLocal)
	{
		this->min[ndata] = static_cast<double>(data->getmin()-step);
		return (this->min[ndata]);
	}
		
	const unsigned int _len=data->length();
	double minlocal=numeric_limits<double>::max();
	Image<bool> *mask = data->mask;
	
	// pour tous les pixels non maskes et dont le poids est 
	// non nul (-> qu'ils appartiennent a cette classe)
	for (unsigned int s=0 ; s<_len ; s++)
	{
		if((*mask)[s] && weight->getValue(s)>0)
		{
			if ( static_cast<double>(data->getValue(s)) < minlocal )
				minlocal = static_cast<double>(data->getValue(s));
		}
	}

	this->min[ndata] = minlocal-step; 

	return (this->min[ndata]);
}

/** Evaluate and modify parameter scale (weighted) with Maximum Likelihood method. 
* @pre use evalmin BEFORE, to be sure that xi-min > 0
*/
double LogNormal::evalscale(const unsigned int ndata, const Data * data, const Data * weight){
	// vérification que les données sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("LogNormal Error : Cannot eval scale because sizes of images and weights are not similar.\n");

	const unsigned int _len=data->length();
	const double localmin = min[ndata];
	Image<bool> *mask = data->mask;
	
	double sum=0.,sumWeights=0.;
	for (unsigned int s=0 ; s<_len ; s++)
	{
		if((*mask)[s] && weight->getValue(s)>0)
		{
			sum+=static_cast<double>(weight->getValue(s))
				* log( static_cast<double>(data->getValue(s)) - localmin );
			sumWeights+=static_cast<double>(weight->getValue(s));
		}
	}

	this->scale[ndata] = (sum/sumWeights); /// ?????? normalement MV -> exp(u)

	return (this->scale[ndata]);
}

/** Evaluate and modify parameter shape (weighted) with Maximum Likelihood method. 
* @pre use evalscale BEFORE
*/
double LogNormal::evalshape(const unsigned int ndata, const Data * data, const Data * weight){
	// vérification que les données sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("LogNormal Error : Cannot eval shape because sizes of images and weights are not similar.\n");

	const unsigned int _len=data->length();
	const double localscale = scale[ndata];
	const double localmin = min[ndata];
	Image<bool> *mask = data->mask;
	
	double sum=0.,sumWeights=0.;
	for (unsigned int s=0 ; s<_len ; s++)
	{
		if((*mask)[s] && weight->getValue(s)>0)
		{
			sum+=static_cast<double>(weight->getValue(s))
				* pow( 
						log( static_cast<double>(data->getValue(s)) - localmin )
				 		- localscale
				   , 2. );
			sumWeights+=static_cast<double>(weight->getValue(s));
		}
	}

	this->shape[ndata] = sqrt(sum/(sumWeights-1.));

	return (this->shape[ndata]);
}


/** getParam : get one parameter of this noise
 * @param _p : number corresponding to one parameter 
 */
const vector < double > LogNormal::getParam(const unsigned short _p){
  
  // si les parametres sont calcules dans l'espace decorrele
  // on les estime par les moments
  
  if (this->ptf_decorr!=NULL)
    {
      switch (_p) {
	
      case 1 :
	{
	  // transformation lineaire (de decorrelation) inverse 
	  
	  // copie les valeurs dans un double tableau
	  vector<double> minY(min);
	  for (unsigned int c1=0;c1<Ndata;c1++)
	    {
	      for(unsigned int c2=0;c2<c1;c2++)
		minY[c1]-=minY[c2]*decorr[c1][c2];
	      minY[c1]/=decorr[c1][c1];
	    }
	  
	  return minY;
	  //  	return vector<double>( Ndata ,0.);
	  
	  break;
	}
      case 2 :
	{
	  vector<double> p2(Ndata,0.);
	  for (unsigned int i=0 ; i<Ndata ; i++)
	    p2[i] = log ( m1[i]*m1[i] / sqrt(m1[i]*m1[i] + m2[i]) );
	  return p2;
	  break;
	}
      case 3 :
	{
	  vector<double> p3(Ndata,0.);
	  for (unsigned int i=0 ; i<Ndata ; i++)
	    p3[i] = sqrt(log ( (m1[i]*m1[i] + m2[i]) / m1[i]*m1[i] ));
	  return p3;
	  break;
	}
      default :
	return vector<double>( Ndata ,0.);
      }
      
    }
  else // sinon, on donne les vrais parametres
    {
      switch (_p) {
	
      case 1 :
  	return min;
  	break;
  	
      case 2 :
	return scale;
	break;
	
      case 3 :
	return shape;
  	break;
      default :
  	return vector<double>( Ndata ,0.);
      }
    }
  return vector<double>();
}
