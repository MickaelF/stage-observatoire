/***************************************************************************
 main.cpp	-	description
 -------------------
 begin								: ven ao� 16 09:56:26 CEST 2002
 email								: oberto@astro.u-strasbg.fr
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#define MAIN

#include <iostream>
#include <stdlib.h>
#include <string.h>

#include "all.h"

int main(int argc, char *argv[]) {
	int return_code = EXIT_FAILURE;
	try {
		cout << "------------ MARSIAA PROCESSING ------------" << endl;

		string confFile = "Marsiaa.conf", maskFile = "", output = "";
		vector<string> files;
		bool debug = false;
		//bool conf = false;

		// Recherche des options dans les arguments d'entree
		for (int arg = 1; arg < argc; arg++) {
			// si option -conf
			if (strcmp(argv[arg], "-conf") == 0) {
			  //conf = true;
				confFile = "";
				confFile = static_cast<string> (argv[++arg]);
			}
			// si option -mask
			else if (strcmp(argv[arg], "-mask") == 0) {
				maskFile = static_cast<string> (argv[++arg]);
				if (maskFile.empty())
					cerr << "Warning : no filename given for mask\n";
			}
			// si option -f
			else if (strcmp(argv[arg], "-f") == 0) {
				files.push_back(static_cast<string> (argv[++arg]));
			}
			// si option -o
			else if (strcmp(argv[arg], "-o") == 0) {
				output = static_cast<string> (argv[++arg]);
			}
			// si option -d // debug
			else if (strcmp(argv[arg], "-d") == 0) {
				debug = true;
			}
			// si option HELP
			else if (strcmp(argv[arg], "-help") == 0 || strcmp(argv[arg], "--help") == 0) {
				cout << "Usage de " << argv[0] << " :\n" << " [-conf confFile] [-f files] [-mask maskFile] [-o output]\n";
				return return_code;
			}
			//		else if (strcmp(argv[arg],"-debug")==0)
			//			DefineDebugMarsiaa();
			else
				// si aucune de ces options
				// on suppose qu'il s'agit d'un nom de fichier
				files.push_back(static_cast<string> (argv[arg]));
		}

		// Lecture, parcours
		Config config(confFile);

		// l'option output en ligne est prioritaire sur celle du fichier
		if (!output.empty())
			config.setOutput(output);

		// Creation d'un objet Marsiaa qui g�re une segmentation markovienne
		Marsiaa marsiaa;

		// lecture des fichiers des donnees images
		if (files.empty()) {
			files = config.getInputFiles();
			if (files.empty())
				ERR("Main : no INPUT files to read, exit\n");
		} else
			config.setInputFiles(files);

		// gestion du masque
		// l'option mask en line prioritaire sur celle du fichier
		if (maskFile.empty())
			maskFile = config.getMask();

		// affecte les param�tres d'entr�e
		// v�rification de la coherence des configurations entre elles
		marsiaa.setConfig(config, maskFile);

		string outMAP = config.getOutMAP();

		const unsigned int IT = config.getItMax();
		const unsigned short stopTest = config.getStopTest();
		const float seuil_stop = config.getStop();
		const bool trace = config.getTrace();
		const string tracefile = config.getOutTrace();

		// creation du fichier trace // ecrase si deja existant
		if (trace)
			marsiaa.traceInit(tracefile);

		if (debug)
			marsiaa.display();

		double pc_change = 100.;
		vector<double> v_pc_change(IT, 0.);

		unsigned int it = 0;
		vector<dMatrix> lastMoments(config.getNclass());

		
		while (it < IT && pc_change > seuil_stop) {
			if (pc_change < seuil_stop * 2.) {
				LogNormal::MinLocal = true;
				//cout << "si Lognormal on change en min local\n";
			}

			cout << "\n***** Iteration n " << it << endl;

			marsiaa.run();

			// a la premiere iteration
			if (it == 0) {
				marsiaa.sort();

				marsiaa.segmMPM();

				// garde les 2 premiers moments pour la comparaison PC_DIST
				if (stopTest == PC_DIST) {
					for (int k = 0; k < config.getNclass(); k++) {
						lastMoments[k].push_back(marsiaa.getclasses(k)->getNoiseModel()->getm1());
						lastMoments[k].push_back(marsiaa.getclasses(k)->getNoiseModel()->getm2());
					}
				}
				v_pc_change[it] = 100.;
			} else {
				// tri suivant les moments d'ordre 1
				marsiaa.sort();

				// comparaison a l'iteration precedente
				if (stopTest == PC_CHANGE) {
					v_pc_change[it] = marsiaa.compareMaps();
					cout << "% of modified pixel labels : " << v_pc_change[it] << endl;
				} else if (stopTest == PC_DIST) {
					for (int k = 0; k < config.getNclass(); k++)
						v_pc_change[it] += marsiaa.compareClasses(k, lastMoments);

					cout << "% distances of classes parameters : " << v_pc_change[it] << endl;
				}

				// somme sur les 3 last
				pc_change = 0.;
				for (int last3 = (it < 2) ? it : 2; last3 >= 0; last3--)
					pc_change += v_pc_change[it - last3];
			}
			// Affichage a plusieurs iterations
			if (config.getVisuMap())
				if (config.getVisuFreq("MAP") != NODISPLAY && (it % (config.getVisuFreq("MAP"))) == 0) {
					stringstream ss;
					ss << "_map_it" << it << ".fits";
					marsiaa.getmap()->getdata(0)->save(config.getOutput() + ss.str());
					marsiaa.getmap()->getdata(0)->show(config.getVisuApp());

					// creation d'un header a partir du premier fichier image
					if (!IO::copy_FitsHeader(files[0], config.getOutput() + ss.str(), config.getString()))
						cerr << "Warning : Could not create a correct header in file " << config.getOutput() + ss.str() << endl;

				}

			if (config.getVisuMapMS())
				if ((it % (config.getVisuFreq("MAPMS"))) == 0) {
					stringstream ss;
					ss << "_mapms_it" << it << ".fits";
					marsiaa.getmap()->save(config.getOutput() + ss.str());
					marsiaa.getmap()->show(config.getVisuApp());

					// creation d'un header a partir du premier fichier image
					if (!IO::copy_FitsHeader(files[0], config.getOutput() + ss.str(), config.getString()))
						cerr << "Warning : Could not create a correct header in file " << config.getOutput() + ss.str() << endl;

				}

			if (config.getVisuDecorr())
				if ((it % (config.getVisuFreq("DECORR"))) == 0) {
					Image<double> * Z;
					for (int z = 0; z < marsiaa.Ndata; z++) {
						Z = marsiaa.getDecorr(z);
						stringstream it_name;
						it_name << "_z" << z << "_it" << it << ".fits";
						Z->save(config.getOutput() + it_name.str());
						Z->show(config.getVisuApp());
						delete Z;
					}
				}

			if (debug)
				marsiaa.display();

			it++;

			if (trace)
				marsiaa.trace(tracefile);
		}

		if (it == IT)
			cout << "\nMax number of iteration reached (=" << IT << ")\n\n";
		else {
			cout << "\nConvergence reached.\n";
			cout << "Sum of the 3 last % of modified labels is " << pc_change << endl << endl;
		}

		// un dernier passage et une derniere segmentation
		marsiaa.segmMPM(true);
		if (trace)
			marsiaa.trace(tracefile);

		// on enregistre TOUJOURS la map
		marsiaa.getImageMap()->save(config.getOutMAP());
		// creation d'un header a partir du premier fichier image
		if (!IO::copy_FitsHeader(files[0], config.getOutMAP(), config.getString()))
			cerr << "Warning : Could not create a correct header in file " << config.getOutMAP() << endl;

		if (config.getMapMS()) {
			marsiaa.getmap()->save(config.getOutMapMS());
			// creation d'un header a partir du premier fichier image
			if (!IO::copy_FitsHeader(files[0], config.getOutMapMS(), config.getString()))
				cerr << "Warning : Could not create a correct header in file " << config.getOutMapMS() << endl;
		}

		// on reshifte/rescale les donn�es si besoin et on recalcule
		// les param�tres du mod�le de vraisemblance
		marsiaa.unshift_unscale();
		marsiaa.update_dataDriven();

		// on enregistre TOUJOURS les stats
		marsiaa.save(config.getOutStats());

		// visualisation finale
		if (config.getVisuMap())
			marsiaa.getImageMap()->show(config.getVisuApp());
		if (config.getVisuMapMS())
			marsiaa.getmap()->show(config.getVisuApp());

		// visualidation des images decorrelees
		if (config.getVisuDecorr()) {
			for (int z = 0; z < marsiaa.Ndata; z++) {
				Image<double> * Z = marsiaa.getDecorr(z);
				stringstream z_name(config.getOutput());
				z_name << "_z" << z << ".fits";
				Z->save(z_name.str());
				Z->show(config.getVisuApp());
				delete Z;
			}
		}

		cout << "END MARSIAA\n";
		return_code = EXIT_SUCCESS;
	} catch (exception & e) {
		cerr << "Execution aborted - error during the execution of marsiaa - " << e.what() << endl;
	} catch (...) {
		cerr << "Execution aborted - error during the execution of marsiaa" << endl;
	}

	return return_code;
}

// configure : --build=i586-linux --host=i586-linux --target=i586-linux --enable-debug 
