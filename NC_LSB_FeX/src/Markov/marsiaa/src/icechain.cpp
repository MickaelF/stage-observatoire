/***************************************************************************
                          icechain.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "icechain.h"
#include "data.h"
#include "noisemodel.h"
#include "image.h"
#include "emchain.h"
#include "semchain.h"
#include "segmclass.h"


ICEChain::ICEChain(){
}
ICEChain::~ICEChain(){
}
/** No descriptions */
void ICEChain::visit(vector<Data *> & , vector<SegmClass *> & )
{
	cout << "ICEChain::visit : nothing here\n";
	/*
	Chain::visit(data,classes);
//	noise->calcul_Trans();
	(dynamic_cast<Chain*>(this))->calcul_X();
	(dynamic_cast<Chain*>(this))->update_apriori();
	(dynamic_cast<Chain*>(this))->update_dataDriven(data,classes);
	*/
}


/** update_apriori : update a priori parameters, Pk and aij 
 * Config :
 *		data		: config data
 *		classes	:	classes to work on
 *
 * Output :
 * 
 * Comment :
 *	same as EMChain::update_apriori + same simulations in SEMChain to update data driven
 */
void ICEChain::update_apriori(vector < Data * > & data, vector < SegmClass * > & classes)
{
	const unsigned int _Nclass = classes.size();
	const unsigned int _len = data[0]->length()-1;
	
	cout << /*Start */"update apriori with ICE\n";	
	
	if (P_forward.empty())
		this->init(data,classes);
	
	/***** identique EMChain::update_apriori *****/
	
	// update des proba de transitions aij
	// somme des proba conjointes a posteriori
	dMatrix Sjoint;
	for (unsigned int k=0 ; k<_Nclass ; k++)
		Sjoint.push_back(vector<double>(_Nclass,0.));
	
	// somme des proba marginales a posteriori
	vector<double> Smarginal(_Nclass,0.);
	for (unsigned int i=0 ; i<_Nclass ; i++)
	{
		for (unsigned int pos=0 ; pos<_len ; pos++)
		{
			Smarginal[i]+=this->getP_marginal_aposteriori(i,pos);
		}
//		cout << Smarginal[i] << endl;
	}
		
	// somme des proba conjointes sur tous les pixels
	for (unsigned int pos=0 ; pos<_len ; pos++)
	{
		dMatrix _joint=getP_joint_aposteriori(data,classes,pos);
		for (unsigned int i=0 ; i<_Nclass ; i++)
		for (unsigned int j=0 ; j<_Nclass ; j++)
				Sjoint[i][j]+=_joint[i][j];		
	}
	
	// l'une apres l'autre sinon, on modifie les aij utilises pour les calculs
	for (unsigned int i=0 ; i<_Nclass ; i++)
	{
//		double S=0.;
		for (unsigned int j=0 ; j<_Nclass ; j++)
		{
			if (Smarginal[i]>eps)
				classes[i]->setA(j,Sjoint[i][j]/Smarginal[i]);
			else
			{
				cerr<<"\nICEChain::update_apriori : Somme marginal aposteriori <eps classe "<<i<<endl;
				classes[i]->setA(j,1./static_cast<float>(_Nclass));
			}
//			cout << "\nnouveau a["<<i<<"]["<<j<<"] = "<<classes[i]->getA(j)<<endl<<endl;
//			S+=classes[i]->getA(j);
		}
//		cout << "somme des A[" << i<<"][*] = " << S<<endl;
	}

	// update des proba a priori de chaque classe
	double S=0.;
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		classes[k]->setP( getP_marginal_aposteriori(k,0) );
		S+=classes[k]->getP();
//		cout << "nouveau P"<<k<<" = "<<classes[k]->getP()<<endl;
	}

	// verification de l'int�grit� des proba que l'on vient de calculer
	if (fabs(1.0-S)>0.01)
	{
		cerr << "ICEChain::update_apriori : Somme des propabilites a priori incorrecte : "<<endl;
		for (unsigned int k=0 ; k<_Nclass ; k++)
		{
			cerr << "classe : "<<k<<endl;
			cerr << "alpha = "<< (*P_forward[k])[0] << " * beta = "<< (*P_backward[k])[0] <<"  =  "
			 << S <<endl;
		}
		ERR("");
	}

	
	/***** simulations identiques au SEMChain::update_apriori *****/ 
	// mais ici, pour pr�parer le ICEChain::update_dataDriven
	
	// Simulation d'un tirage al�atoire selon les probabilit�s marginales calcul�e pr�c�demment
	// N_STOCHASTIC tirages
	Image<unsigned int> *X;
	// une P_forward temporaire pour ne pas le changer � chaque tirage
	vector<Image<double> > alpha(_Nclass);
	for (unsigned int k=0 ; k<_Nclass ; k++)
		alpha[k].createData( data[0]->getwidth(),data[0]->getheight() );
	for(unsigned int pos=0 ; pos<_len+1 ; pos++)
		for(unsigned int k=0 ; k<_Nclass ; k++)
			alpha[k][pos]=0.;
	
	for (unsigned int n=0 ; n<N_STOCHASTIC ; n++)
	{
		X=simulX(data,classes);
		// + simulation des proba forward
		for(unsigned int pos=0 ; pos<_len ; pos++)
		{
			for(unsigned int k=0 ; k<_Nclass ; k++)
			{
				// simulation des proba joint aposteriori
//				for(unsigned int l=0 ; l<_Nclass ; l++)
//					if ((*X)[pos]==k && (*X)[pos+1]==l)
//						Sjoint[k][l]+=1./N_STOCHASTIC;
			}
			alpha[(*X)[pos]][pos]+=1./N_STOCHASTIC;
		}
		// affecte les valeurs pour la derniere position pos=_len
		alpha[(*X)[_len]][_len]+=1./N_STOCHASTIC;

		// + simulation des proba conjointes
		delete X;
	}
	
	for (unsigned int i=0 ; i<_Nclass ; i++)
		for (unsigned int pos=0 ; pos<_len+1 ; pos++)
		{
			//Astuce pour remettre � jour par la fonction d�terministe
			(*P_backward[i])[pos]=1.;
			(*P_forward[i])[pos]=alpha[i][pos];
		}	
	
//	cout << "End update apriori with ICE\n";	

}
