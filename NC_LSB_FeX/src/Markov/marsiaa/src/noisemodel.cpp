/***************************************************************************
                          noisemodel.cpp  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@cacao.u-strasbg.fr
***************************************************************************/


#include "noisemodel.h"
#include "data.h"
#include "segmclass.h"
#include "image.h"
#include "Math.h"

unsigned int NoiseModel::Ndata=0;

NoiseModel::NoiseModel(){
//	cout << "\n-------Constructeur NoiseModel\n";
	if (Ndata>0)
	{
		m1.assign(Ndata,0.);
		m2.assign(Ndata,0.);
		m3.assign(Ndata,0.);
		m4.assign(Ndata,0.);
		for (unsigned int i=0 ; i<Ndata ; i++)
		{
			varcov.push_back(vector<double>(Ndata,0.));
			varcovInv.push_back(vector<double>(Ndata,0.));
			corr.push_back(vector<double>(Ndata,0.));
			decorr.push_back(vector<double>(Ndata,0.));
			// par d�faut la matrice de d�corr�lation et de correlation est l'identit�
			decorr[i][i] = 1.;
			corr[i][i] = 1.;
		}
	}
}

NoiseModel::NoiseModel(const NoiseModel& _copie){
//	cout << "\n-------Constructeur NoiseModel Copie\n";
	m1 = _copie.m1;
	m2 = _copie.m2;
	m3 = _copie.m3;
	m4 = _copie.m4;
	det = _copie.det;
	varcov = _copie.varcov;
	varcovInv = _copie.varcovInv;
	corr = _copie.corr;
	decorr = _copie.decorr;
}

NoiseModel::~NoiseModel(){
}

NoiseModel& NoiseModel::operator=(const NoiseModel& _copie){
	if (this != &_copie)
	{
		for (int i=Ndata-1 ; i>=0 ; i--)
			for (int j=Ndata-1 ; j>=0 ; j--)
			{
				this->varcov[i][j] = _copie.varcov[i][j];
				this->varcovInv[i][j] = _copie.varcovInv[i][j];
				this->corr[i][j] = _copie.corr[i][j];
				this->decorr[i][j] = _copie.decorr[i][j];
			}
	}
	return *this;
}


/** Evaluate statistical likelihood */
double NoiseModel::likelihood(const vector<Data*> & data, const int _pos){
	
	Image<bool> *mask = data[0]->mask;
	if ( ! (*mask)[_pos] )
		return 1.;
	
	vector<double> v(Ndata);
	for (unsigned int i=0 ; i<Ndata ; i++)
		v[i] = static_cast<double>(data[i]->getValue(_pos));
	return(this->likelihood(v));
}

/** Evaluate statistical likelihood for Gaussian noise */
double NoiseModel::likelihood(const double * _values){

	vector<double> tmp(Ndata,0.);
	for (unsigned int i=0 ; i<Ndata ; i++)
		tmp[i] = _values[i];

	return(this->likelihood(tmp));
}

/** Read property of int Ndata. */
const unsigned int NoiseModel::getNdata(){
	return NoiseModel::Ndata;
}

/** Write property of int Ndata. */
void NoiseModel::setNdata( const unsigned int _newVal){
	NoiseModel::Ndata = _newVal;
}

/** get m1 */
const vector<double>& NoiseModel::getm1() const{
	return m1;
}
/** get m2 */
const vector<double>& NoiseModel::getm2() const {
	return m2;
}

/** get det */
double NoiseModel::getdet() const{
	return det;
}

/** get varcov */
dMatrix& NoiseModel::getvarcov(){
 return this->varcov;
}

const double NoiseModel::getvarcov( const unsigned int _i, const unsigned int _j ) const{
	return varcov[_i][_j];
}

/** Read property of vector<double> corr. */
const double NoiseModel::getcorr(const unsigned int _i, const unsigned int _j) const{
	return corr[_i][_j];
}

/** Evaluate and modify parameter corr(i,j). */
double NoiseModel::evalcorr(const unsigned int ndata1, const Data* data1, const unsigned int ndata2, const Data* data2, const Data* weight){
	// v�rification que les donn�es sont de tailles identiques
	if (data1->getwidth()!=weight->getwidth() || data1->getheight()!=weight->getheight())
		ERR("Error : Cannot eval corr because sizes of images and weights are not similar.\n");

	const unsigned int _size = data1->getwidth()*data1->getheight();
	Image<bool> *mask = data1->mask;

	double sum=0,sumWeights=0;

	for (unsigned int i=0 ; i<_size ; i++)
	{
		if ((*mask)[i]){
			sum+=
				static_cast<double>(weight->getValue(i))
				* (data1->getValue(i)-m1[ndata1])
				* (data2->getValue(i)-m1[ndata2]) ;
			sumWeights+=static_cast<double>(weight->getValue(i));
		}
	}

	// modifie la correlation
	this->corr[ndata1][ndata2] = this->corr[ndata2][ndata1] = 
		sum / ( sumWeights * sqrt(m2[ndata1] * m2[ndata2]) );
		
	if (corr[ndata1][ndata2]>1.)
		cerr << "Warning : Correlation between images " << ndata1 << " , " << ndata2 << " is >1!\n";
		
	return (this->corr[ndata1][ndata2]);

}


/** Evaluate and modify parameter varcov with Maximum Likelihood method. */
void NoiseModel::evalvarcov()
{
	// Verifie si on peut avoir une matrice de var/cov
	if (Ndata<1)
		ERR("Error : Cannot eval varcov because not enought images.\n");

	if (Ndata==1)
	{
		this->corr[0][0]=1;
		this->varcov[0][0]=m2[0];
		this->det=varcov[0][0];
	}
	else {

		for(unsigned int c1=0;c1<Ndata;c1++)
			for(unsigned int c2=0;c2<Ndata;c2++)
				varcov[c1][c2]=sqrt(m2[c1]*m2[c2])*corr[c1][c2];

		// Calcul du determinant de la matrice de varcov
		double **tmp = new double* [Ndata];
		for (unsigned int i=0 ; i<Ndata ; i++)
			tmp[i] = new double [Ndata];//&varcov[i].front();

		for(unsigned int c1=0;c1<Ndata;c1++)
			for(unsigned int c2=0;c2<Ndata;c2++)
				tmp[c1][c2]=varcov[c1][c2];

		this->det=Mdet(Ndata,tmp);

		for (unsigned int i=0 ; i<Ndata ; i++)
			delete[] tmp[i];
		delete[] tmp;

	}


	if ( fabs(det)<1.e-10)
	{
		cerr << "\nDeterminant trop petit = "<<det<<endl;
		det=1.e-10;
	}

}

/** Evaluate and modify Inverse matrix varcov */
void NoiseModel::evalvarcovInv(){

	// copie les valeurs dans un double tableau
	double **tmp = new double*[Ndata];
	for (unsigned int i=0 ; i<Ndata ; i++)
		tmp[i] = new double [Ndata];

	for(unsigned int c1=0;c1<Ndata;c1++)
		for(unsigned int c2=0;c2<Ndata;c2++)
			tmp[c1][c2]=varcov[c1][c2];

	Minverse(SegmClass::getNdata(), tmp, tmp);

	for(unsigned int c1=0;c1<Ndata;c1++)
		for(unsigned int c2=0;c2<Ndata;c2++)
			varcovInv[c1][c2]=tmp[c1][c2];

	for (unsigned int i=0 ; i<Ndata ; i++)
		delete[] tmp[i];
	delete[] tmp;

}

/** Use decorrelation choosed */
void NoiseModel::decorrelate(){
	if (this->ptf_decorr!=NULL && Ndata>1)
		(this->*ptf_decorr)();
}


/** Decorrelate with cholesky algorithm */
void NoiseModel::cholesky(){
	cout << "\n *** cholesky cholesky cholesky *** \n";
	// copie les valeurs dans un double tableau
	double **tmp = new double*[Ndata];
	double **tmpA = new double*[Ndata];
	for(unsigned int c1=0 ; c1<Ndata ; c1++)
	{
		tmp[c1] = new double [Ndata];
		tmpA[c1] = new double [Ndata];
		for(unsigned int c2=0 ; c2<Ndata ; c2++)
		{
			tmp[c1][c2] = varcov[c1][c2];
			tmpA[c1][c2] = 0.;
		}
	}

	cholds(tmp,Ndata,tmpA);
	Minverse(Ndata,tmpA,tmpA);

	// r�cup�ration des r�sultats
	for(unsigned int c1=0 ; c1<Ndata ; c1++)
	{
		for(unsigned int c2=0 ; c2<Ndata ; c2++)
			decorr[c1][c2] = tmpA[c1][c2];
	}
			

	// liberation de la m�moire
	for (unsigned int i=0 ; i<Ndata ; i++){
		delete[] tmp[i];
		delete[] tmpA[i];
	}
	delete[] tmp;
	delete[] tmpA;

}

/** Independant Component Analysis for decorrelation */
void NoiseModel::ica(){
	ERR("NoiseModel::ica : Not yet implemented\n");
}

/** set corr */
void NoiseModel::setcorr(const unsigned int i, const unsigned int j, const double _newVal){
	corr[i][j] = _newVal;
}

/** set corr */
void NoiseModel::setcorr(const dMatrix& _newcorr){
 corr = _newcorr;
}


/** set det */
void NoiseModel::setdet(const double& _newVal){
	det = _newVal;
}
/** set varcov */
void NoiseModel::setvarcov(const unsigned int i, const unsigned int j, const double _newVal){
	varcov[i][j] = _newVal;
}

/** Update correlations */
void NoiseModel::evalcorr(const vector<Data*>& data, const Data * pt_xsi ){

	for (unsigned int i=0 ; i<Ndata ; i++)
		for (unsigned int j=i+1 ; j<Ndata ; j++)
			evalcorr(i,data[i],j,data[j],pt_xsi);
}

/** Evaluate First order moment */
double NoiseModel::M1(const Data* _data, const Data* _weight){
//	_data->getdata(0)->show();
//	Image<double> Mdata(_data->getdata(0));
//	Mdata.show();
	Image<double> w(_weight);
	// pose les poids a zero la ou il y a un masque
	Image<double> poids(w * *(Data::mask));
		
	// copie les donnees pour les parametres de M1
	return ( ::M1(
		dynamic_cast<Image<double>*>(_data->getdata(0))->getPdata() 
		, poids.getPdata(), w.length()) );
}

/** Evaluate Second order moment */
double NoiseModel::M2(const Data * _data, const Data * _weight, const double _m1){
	Image<double> data(_data);
	Image<double> w(_weight);
	// pose les poids a zero la ou il y a un masque
	Image<double> poids(w * *(Data::mask));
	// copie les donnees pour les parametres de Moment(2...)
	return ( ::Moment(2,_m1,data.getPdata() , poids.getPdata(), data.length()) );
}

/** Update correlations */
void NoiseModel::update_corr(const vector < Data * > & data , const Data * pt_xsi){
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		m1[i] = M1(data[i],pt_xsi);
		m2[i] = M2(data[i],pt_xsi,m1[i]);
	}
	
	for (unsigned int i=0 ; i<Ndata ; i++)
		for (unsigned int j=i+1 ; j<Ndata ; j++)
		{
			evalcorr(i,data[i],j,data[j],pt_xsi);
//			cout << "rho" <<i<<j<<" = "<< corr[i][j] << endl;
		}
		

//	evalvarcov();
//	evalvarcovInv();
}

/** Initialise correlations */
void NoiseModel::init_corr(){
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		for (unsigned int j=0 ; j<Ndata ; j++)
			corr[i][j]=0.;
		corr[i][i]=1.;
	}
	evalvarcov();
	evalvarcovInv();
}


/** get one of the m1 vector
* @param i : number of the image you want the first order moment
* @return the 'i'eme first order moment
*/
const double & NoiseModel::getm1(unsigned int i) const{
	return m1[i];
}
/** get one of the m1 vector
* @param i : number of the image you want the second order moment
* @return the 'i'eme second order moment
*/
const double & NoiseModel::getm2(unsigned int i) const{
	return m2[i];
}

