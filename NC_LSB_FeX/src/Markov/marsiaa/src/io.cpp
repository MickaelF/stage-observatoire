/***************************************************************************
                          io.cpp  -  description
                             -------------------
    begin                : ven ao� 23 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "io.h"
#include "fitsio.h"
#include "image.h"

/** the way to scan the image (as an iterator) */
Image<unsigned int> const * IO::scan=NULL;


IO::IO(){
}

IO::~IO(){
}

/** Read given fits file and store image 
    Config		:	Image<double>* im : Pointeur sur l'objet Image qui contiendra les donn�es lues
    string filename : nom du fichier � ouvrir
    Comment	: Image doit etre en double dans le cas ou on a besoin de faire :
    valeur reelle = valeur bande * BSCALE + BZERO
    (ce qui est fait automatiquement dans fits_read_image(...).
*/

unsigned int IO::scan_get_pixel(int i){
  return scan->get_pixel(i);
}

void IO::importFits(Image<double>*im, const string & filename )
{
 	/* Ouverture et lecture des fichiers FITS */
 	char card[FLEN_CARD];
 	int  nkeys, ii, status = 0, oldStatus = 0;  // MUST initialize status

	int nulval;
	int anynul;
 	int naxis,bitpix;
 	fitsfile *fptr;
	nulval = 0;

	fits_open_file(&fptr, filename.c_str(), READONLY, &status);

	if (status) {         // print any error messages
		try { cerr << "Erreur pendant lecture\n"; throw(-1);
		fits_report_error(stderr, status);
		} catch(...){ throw; }
	}
	/* Recherche d'une eventuelle extension Image */
	fits_movnam_hdu(fptr, IMAGE_HDU,const_cast<char *>(string("SCI").c_str()), 0, &status);

	if (status)
	{
		oldStatus = status;
		status=0;
		fits_close_file(fptr, &status);
		fits_open_file(&fptr, filename.c_str(), READONLY, &status);
	}


	/* lecture de l'entete FITS */
	fits_get_hdrspace(fptr, &nkeys, NULL, &status);

	for (ii = 1; ii <= nkeys; ii++)  {
		fits_read_record(fptr, ii, card, &status); /* read keyword */
	}

	/*lecture du detail de l'entete */
	fits_get_img_type(fptr, &(bitpix), &status);

	fits_get_img_dim(fptr, &naxis, &status);
	if (naxis > 2)
		cerr<<"File : "<<filename<<"\nWarning : nombre d'axes > 2 !\n\
			seuls les 2 premiers sont etudies\n";
	else if (naxis <2)
		try { cerr<<"File : "<<filename<<"\nError : pas assez d'axes (<2)\n"; throw(-1);
		} catch(...){ throw; }

	long naxes[2];

	fits_get_img_size(fptr, 2, naxes, &status);
	im->createData(naxes[0],naxes[1]);
	long size = naxes[0]*naxes[1];

//	cout << "bitpix = " << bitpix << " size = " << naxes[0] << " " << naxes[1] << endl;
	
	fits_read_img(fptr, TDOUBLE, 1,
                size, &nulval, im->getdata(),
                &anynul, &status);

	fits_close_file(fptr, &status);
	
	double min=OUT_OF_RANGE, max=-OUT_OF_RANGE;
	// initialisation min, max
	if (fabs(im->get_pixel(0))<OUT_OF_RANGE)
		min=max=im->get_pixel(0);

  // recherche du min, max
	for(int i=0 ; i<size ; i++)
	{
		if (im->get_pixel(i)<min && fabs(im->get_pixel(i))<OUT_OF_RANGE)
			min=im->get_pixel(i);
		else if (im->get_pixel(i)>max && fabs(im->get_pixel(i))<OUT_OF_RANGE)
			max=im->get_pixel(i);
	}
	im->setmin(min);
	im->setmax(max);

	/* print any error messages */
	if (status) {
		fits_report_error(stderr, oldStatus);
		fits_report_error(stderr, status);
	}

	try {
		if (status) {
			throw(-1);
			fits_report_error(stderr, status);
		}
	} catch(...){ throw; }

}

  
/** setscan */
void IO::setscan(const Image<unsigned int> * const _newScan){
	scan=_newScan;
}


/** Read given fits file and store image in given array */
//void IO::importFits(double *array, long size, const string & filename){
//
////	Image<double> im(;
//}

/** copy_FitsHeader
@param _ifile : file name of the fits file to extract header
@param _ofile : file name of the output fits file
@param history : string to add in history header */
bool IO::copy_FitsHeader(const string & _ifile, const string & _ofile, const string &history){
	int status=0;
	
 	fitsfile *fptrW; // pointeur sur le fichier en ecriture
 	fitsfile *fptrR; // pointeur sur le fichier en lecture
  
  fits_open_file(&fptrR,_ifile.c_str(),READONLY,&status);
	if (status)
		return false;
	
  fits_open_file(&fptrW,_ofile.c_str(),READWRITE,&status);
	
	int bitpix=0;
 	// delete all old keywords to erase
  {
		fits_get_img_type(fptrW, &(bitpix), &status);
		int  nkeys=0;
	  fits_get_hdrspace(fptrW,&nkeys,NULL,&status);
		for (int ii = 1; ii <= nkeys; ii++) 
			fits_delete_record(fptrW, 1, &status); 
	}
    
	fits_copy_header(fptrR,fptrW,&status);
	// si on avait un bitpix avant, alors on le remet
	if (bitpix)
		fits_update_key(fptrW, TINT,const_cast<char *>(string("BITPIX").c_str()),&bitpix,const_cast<char *>(string("").c_str()),&status);

	fits_write_comment(fptrW, "file created by software Marsiaa, product of Observatoire Astronomique de Strasbourg, CDS", &status);
	fits_write_history(fptrW, history.c_str(), &status);
	
	if (status!=0)
	{
		cout << "\nErreurs en Ecriture :\n";
		fits_report_error(stderr,status);
	}

	fits_close_file(fptrW,&status);
	fits_close_file(fptrR,&status);
	
	return static_cast<bool>(status==0);
}
