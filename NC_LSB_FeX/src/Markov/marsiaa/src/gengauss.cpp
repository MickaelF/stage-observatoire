/***************************************************************************
                          gengauss.cpp  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@cacao.u-strasbg.fr
 ***************************************************************************/


#include "gengauss.h"
#include "gauss.h"
#include "data.h"
#include "image.h"
#include "pyramid.h"
#include "segmclass.h"
#include "Math.h"
#include <math.h>
template <typename TType> class Image;

GenGauss::GenGauss(){
	cout << "-------Constructeur GENERAL Gauss\n";
	if (Ndata>0)
	{
		// initialise les tableaux � 0
		mean.assign(Ndata,0.);
		stddev.assign(Ndata,0.);
		shape.assign(Ndata,0.);
	}
	else ERR("Gauss::Gauss : Can't create Gauss object without size of data.\n");
}

GenGauss::GenGauss (const GenGauss& _copie) : NoiseModel(_copie) {
	mean = _copie.getmean();
	stddev = _copie.getstddev();
	shape = _copie.getshape();
}

GenGauss::GenGauss(const Gauss& _copie) : NoiseModel(_copie) {
	mean = _copie.getmean();
	stddev = _copie.getstddev();
	shape.assign(Ndata,0.);
	for (unsigned int i=0 ; i<Ndata ; i++)
		shape[i] = 2.; // une gausienne, est la cas particulier d'une gaussienne generalisee ou shape=2
}

GenGauss::GenGauss(const NoiseModel & _copie) : NoiseModel(_copie) {
	mean =_copie.getm1();
	stddev.assign(Ndata,0.);
	shape.assign(Ndata,0.);
	// initialisation de shape comme une gaussienne
	// car la methode des moments pour le calcul reste trop compliquee
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		shape[i] = 2.;
		stddev[i] = sqrt(_copie.getm2(i));
	}
		
}

GenGauss::~GenGauss(){
}

/** operator =  */
GenGauss& GenGauss::operator = (const GenGauss & _copie){
	if (this != &_copie)
	{
		Ndata = _copie.Ndata;
		mean = _copie.mean;
		stddev = _copie.stddev;
		shape = _copie.shape;
	}
	return *this;
}

bool GenGauss::isGenGauss() const{
	return true;
}

/** Evaluate statistical likelihood for Generalized Gaussian noise */
double GenGauss::likelihood(const double* _values)
{
	vector<double> tmp(Ndata,0);
	for (unsigned int i=0 ; i<Ndata ; i++)
		tmp[i] = _values[i];

	return(this->likelihood(tmp));
}

/** Evaluate statistical likelihood for Generalized Gaussian noise */
double GenGauss::likelihood(const vector<double>& _values)
{
	if(Ndata!=_values.size())
		ERR("GenGauss::likelihood : could not evaluate likelihood, sizes of data and noise are not similar\n");

	// calcul du vecteur des donn�es dans l'espace d�corr�l�
	vector<double> Z(Ndata,0.);
	for (unsigned int i=0 ; i<Ndata ; i++)
//		for (Z[i] = 0., unsigned int c1=0 ; c1<Ndata ; c1++)
			for (unsigned int c2=0 ; c2<i+1 ; c2++)
				Z[i] += decorr[i][c2] * _values[i];
	
	vector<double> M(Ndata,0);

	// centre les valeurs
	for(unsigned int cpt_capteur=0 ; cpt_capteur<Ndata ; cpt_capteur++)
		M[cpt_capteur]=fabs(Z[cpt_capteur]-mean[cpt_capteur]);

	double RES=1.;
	for(unsigned int cpt_capteur=0 ; cpt_capteur<Ndata ; cpt_capteur++)
	{
		/* Pour chaque capteur de facon independante */
		double shapeInv=1./shape[cpt_capteur];
		double eta = sqrt(
					Gamma(3*shapeInv)
					/ ( stddev[cpt_capteur]*stddev[cpt_capteur]*Gamma(shapeInv) )
					);

		double r =
				eta * shape[cpt_capteur]
				/ ( 2*Gamma(shapeInv) )
				* exp( -
					pow( eta * M[cpt_capteur] , shape[cpt_capteur] )
				);

		if( (Gamma(1./shape[cpt_capteur])<eps) || (stddev[cpt_capteur]<eps) )
			{ERR("GenGauss::likelihood : calcul d'une gaussienne g�n�ralis�e avec stddev[cpt_capteur]="<<stddev[cpt_capteur]
			<< " et gamma(1/shape)="<< Gamma(1./shape[cpt_capteur]) << "\n"); }
		// else sous entendu
		
		// on multiplie aux coefficients sur la diagonale de la matrice
		// de passage des donnees Y aux donnees decorrelees Z
		RES *= decorr[cpt_capteur][cpt_capteur];

		RES*=r;//GaussG(Z[i][j][cpt_capteur][k],MOYZ[k][cpt_capteur],ECAZ[k][cpt_capteur],CZ[k][cpt_capteur]);

	}
	/*
	argu=res=0.;
	for(unsigned int cpt1=0 ; cpt1<Ndata ; cpt1++)
	{
		res=0.;
		for(unsigned int cpt2=0 ; cpt2<Ndata ; cpt2++)
		{
			res+=M[cpt2]*varcovInv[cpt2][cpt1];
		}
		argu+=M[cpt1]*res;
	}
*/
//	double a = 1./pow(2.*PI,static_cast<float>(Ndata)/2.)/sqrt(det)*exp(-0.5*argu);
//	return(1./pow(2.*PI,static_cast<float>(Ndata)/2.)
//					/sqrt(det)*exp(-0.5*argu));

return RES;
}


/********** ACCES FUNCTIONS **********/
/** Write/Read property of one mean. */
void GenGauss::setmean(const unsigned int _capteur, const double _newVal){
	mean[_capteur] = _newVal;
	m1[_capteur] = _newVal;
}
const double GenGauss::getmean(const unsigned int _capteur) const{
	return mean[_capteur];
}
const vector<double>& GenGauss::getmean() const {
	return mean;
}

/** Write/Read property of double stddev. */
void GenGauss::setstddev(const unsigned int _capteur, const double _newVal){
	stddev[_capteur] = _newVal;
	m2[_capteur] = _newVal*_newVal;
}
const double GenGauss::getstddev(const unsigned int _capteur) const{
	return stddev[_capteur];
}
const vector<double>& GenGauss::getstddev() const{
	return stddev;
}

/** Read property of one shape. */
const double GenGauss::getshape(const unsigned int _capteur) const{
	return shape[_capteur];
}
/** get shape */
const vector<double>& GenGauss::getshape() const{
	return shape;
}

/** Evaluate and modify parameter mean with Maximum Likelihood method.
 * Config	:
 * 		const Data& data : one of all config data
 *   	const int ndata : range in vector of data
 * Output	:
 * 		double value : ean evaluated by ML method.
 *
 * Comment:
 *		Summarize ALL values in the data -> if data are pyramid then evaluate mean on all scales
 */
double GenGauss::evalmean(const unsigned int ndata, const Data* data){
	const long _size = data->length();
	Image<bool> *mask = data->mask;
	double sum=0,sumWeights=0;

	for (int s=0 ; s<_size ; s++)
	{
		if((*mask)[s])
		{
			sum+=static_cast<double>(data->getValue(s));
			sumWeights++;
		}
	}

	this->mean[ndata] = sum/sumWeights;
	return (this->mean[ndata]);
}

/** Evaluate and modify parameter mean (weighted) with Maximum Likelihood method.
 * Config	:
 * 		const Data& data : one of all config data
 *   	const int ndata : range in vector of data
 *    const Image<double>& weight : weights to use
 * Output	:
 * 		double value : mean evaluated by ML method.
 *
 * Comment :
 *		Summarize ALL values in the data -> if data are pyramid then evaluate mean on all scales
 */
double GenGauss::evalmean(const unsigned int ndata, const Data* data, const Data* weight ){
	// v�rification que les donn�es sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("Error : Cannot eval mean because sizes of images and weights are not similar.\n");

	const unsigned int _len=data->length();
	Image<bool> *mask = data->mask;

	double sum=0.,sumWeights=0.;
	for (unsigned int s=0 ; s<_len ; s++)
	{
		if((*mask)[s])
		{
			sum+=static_cast<double>(weight->getValue(s))
				* static_cast<double>(data->getValue(s));
			sumWeights+=static_cast<double>(weight->getValue(s));
		}
	}

	this->mean[ndata] = sum/sumWeights;

	return (this->mean[ndata]);
}

/** Evaluate and modify parameter Standard deviation with Maximum Likelihood method.
 * Config	:
 *  	const int ndata				: number of the rank of the data
 *		const Data *data			: pointer on data
 * Output	:
 * 		double : ML estimated standard deviation
 */
double GenGauss::evalstddev(const unsigned int ndata, const Data* data){
	const long _size = data->length();
	double sum=0,sumWeights=0;
	Image<bool> *mask = data->mask;
	for (int s=0 ; s<_size ; s++)
	{
		if((*mask)[s])
		{
			sum+=pow( mean[ndata]-static_cast<double>(data->getValue(s)) , 2. );
			sumWeights++;
		}
	}

	stddev[ndata] = sqrt(sum/(sumWeights-1));
	return (this->stddev[ndata]);
}

/** Evaluate and modify Standard deviation parameter with Maximum likelihood method.
 * Config	:
 *  	const int ndata				: number of the rank of the data
 *		const Data *data			: pointer on data
 *		const Data* pt_weight	: pointer on weight data
 * Output	:
 *		double : ML estimated standard deviation
 */
double GenGauss::evalstddev(const unsigned int ndata, const Data* data, const Data* weight ){
	// v�rification que les donn�es sont de tailles identiques
	if (data->getwidth()!=weight->getwidth() || data->getheight()!=weight->getheight())
		ERR("Error : Cannot eval stddev because sizes of images and weights are not similar.\n");

	const double _shape = shape[ndata];
	const double _mean = mean[ndata];
	const unsigned int _size = data->getwidth()*data->getheight();
	Image<bool> *mask = data->mask;
	double sum=0,sumWeights=0;
	for (unsigned int s=0 ; s<_size ; s++)
	{
		if((*mask)[s])
		{
			sum+=
				weight->getValue(s)
				* pow( fabs( _mean - data->getValue(s) ) , _shape );
			sumWeights += weight->getValue(s);
		}
	}

	// modifie la stddev
	stddev[ndata] = sqrt(
		pow( _shape * sum/(sumWeights-1.) , 2./_shape )
		* Gamma(3./_shape)
		/ Gamma(1./_shape)
		);

	return (this->stddev[ndata]);
}

/** Evaluate and modify parameter shape (weighted) with Maximum Likelihood method.
 * Config :
 *  	const unsigned int ndata : number of the rank of the data
 *		const Data *data			: pointer on data
 *		const Data* weight		: pointer on weight data
 *
 * Output	:
 *		double : ML estimated shape
 *
 * Comment :
 */
double GenGauss::evalshape(const unsigned int ndata, const Data * data, const Data * weight)
{
	double R=1.,w=0.,Gx=0.,Gxp=0.;
	double u=.7,v=6.; //Borne de c
	double D=0.;

	const unsigned int _size = data->getwidth()*data->getheight();
	Image<bool> *mask = data->mask;
	
  for (unsigned int s=0 ; s<_size ; s++)
 		if((*mask)[s])
 			D+=weight->getValue(s);

  while(R>eps){
 		w=(u+v)/2.;
 		Gx=0.;
 		Gxp=0.;
		for (unsigned int s=0 ; s<_size ; s++)
		{
			if((*mask)[s]){
				Gx += weight->getValue(s) * pow( fabs(data->getValue(s) - mean[ndata]) , w );
				Gxp += Gx * log( fabs(data->getValue(s) - mean[ndata]) );
			}
		}
		double F = w + Digamma(1./w) + log(w) + log(Gx/D) - w * Gxp/Gx;

  	if(F>0.)
  		u=w;
    else
			v=w;

		R=fabs(F);

		if(fabs(u-v)<1.e-2)
			R=0.;
   }
   shape[ndata]=w;

   return this->shape[ndata];
}

/** Update data driven with weights
 * Config :
 *		const vector<Data*> data : vector of data to evaluate
 *		const Data* pt_weight : pointer on weight data
 *
 * Output	:
 *
 * Comment :
 *		pseudo-ML method : eval mean with first order moment,
 *		and (stddev,shape) with ML (assuming knowing mean)
 */
void GenGauss::update_dataDriven(const vector<Data*>& data, const Data * pt_weight )
{
//	/* Mise a jour des parametres de decorrelations */
//	this->update_corr(data,pt_weight);
//	this->evalvarcov();
//	this->evalvarcovInv();
//	this->decorrelate();
	
	// pour chaque image en entr�e
//	for (unsigned int i=0 ; i<Ndata ; i++)
//	{
//		// calcul des donn�es dans l'espace d�corr�l�
//		Image<double> Z(data[i]);
//		for (unsigned int s=0 ; s<data[i]->length() ; s++)
//		{
//			Z[s]=0.;
//			for (unsigned int c2=0 ; c2<i+1 ; c2++)
//				Z[s] += decorr[i][c2] * data[i]->getValue(s);
//		}
//		
//		/* Mise a jour des parametres de la Gaussienne G�n�ralis�e */
//		evalmean(i,&Z,pt_weight);
//		evalshape(i,&Z,pt_weight);
//		evalstddev(i,&Z,pt_weight);
//	}
	for (unsigned int i=0 ; i<Ndata ; i++)
	{
		evalmean(i,data[i],pt_weight);
		evalshape(i,data[i],pt_weight);
		evalstddev(i,data[i],pt_weight);
	}

}



/** Evaluate decorrelation matrix */
/*void GenGauss::evaldecorr(){
	// copie les valeurs dans un double tableau
	double **tmp = new double*[Ndata];
	double **tmpA = new double*[Ndata];
	for(unsigned int c1=0;c1<Ndata;c1++)
	{
		tmp[c1] = new double [Ndata];
		tmpA[c1] = new double [Ndata];
		for(unsigned int c2=0;c2<Ndata;c2++)
		{
			tmp[c1][c2]=varcov[c1][c2];
			tmpA[c1][c2]=decorr[c1][c2];
		}
	}
	
	
}
*/



/** operator = */
/*NoiseModel & GenGauss::operator=(const NoiseModel & _copie){
	if (this != &_copie)
	{
		if (_copie.isGenGauss())
		{
			const GenGauss *const _ptcopie = dynamic_cast<GenGauss*>(const_cast<NoiseModel*>(&_copie));
			Ndata = _ptcopie->Ndata;
			for (unsigned int n=0 ; n<Ndata ; n++)
			{
				mean[n] = _ptcopie->getmean(n);
				stddev[n] = _ptcopie->getstddev(n);
				shape[n] = _ptcopie->shape[n];
				for (unsigned int n2=0 ; n2<Ndata ; n2++)
				{
					corr[n][n2] =_ptcopie->getcorr(n,n2);
					varcov[n][n2] =_ptcopie->getvarcov(n,n2);
				}	
			}
		}
		else
		if (_copie.isWeibull())
		// conversion de GenGauss -> Weibull
		{
			ERR("GenGauss=Weibull !!! not implemented\n");
		}
	}
	return *this;
}
*/

/** getParam : get one parameter of this noise
 * @param _p : number corresponding to one parameter 
 */
const vector<double> GenGauss::getParam(const unsigned short _p){
  
  // si les parametres sont calcules dans l'espace decorrele
  // on les estime par les moments
  
  if (this->ptf_decorr!=NULL)
    {
      switch (_p) {
	
      case 1 :
	return m1;
	break;
	
      case 2 :
	return m2;
	break;
  	
      case 3 :
	return shape;
	break;
  	
      }	
    }
  else // sinon, on donne les vrais parametres
    {
      switch (_p) {
	
      case 1 :
	return mean;
	break;
	
      case 2 :
	{
	  vector<double> var(stddev);
	  for (unsigned int i=0 ; i<var.size() ; i++)
	    var[i] *= var[i];
	  return var;
	  break;
  	}
  	
      case 3 :
	return shape;
	break;
  	
      }	
    }	
  return vector<double>();
}
