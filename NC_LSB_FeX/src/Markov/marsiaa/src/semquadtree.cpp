/***************************************************************************
                          semquadtree.cpp  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#include "semquadtree.h"
#include "data.h"
#include "pyramid.h"
#include "noisemodel.h"
#include "segmclass.h"

SEMQuadTree::SEMQuadTree(){
}
SEMQuadTree::~SEMQuadTree(){
}

/** No descriptions */
void SEMQuadTree::update_apriori(vector < Data * > & data, vector < SegmClass * > & classes){

	const unsigned char _Nclass = classes.size();
	const unsigned char lastClass = _Nclass-1;
	// Nscale -2 pour avoir le num de l'avant dernier etage car on utilise le pere alors les calculs ne peuvent etre sur le dernier etage
	const unsigned int lastScale = Nscale-2 ; 
	const unsigned int baseScale = data[0]->getBaseScale();
	
	cout << /*Start */"update apriori with SEM\n";
	
	// allocation proba conjointe temporaire
	vector<vector<Pyramid<double>*> > P_joint;
	for(unsigned char k=0 ; k<_Nclass ; k++)
	{
		P_joint.push_back(vector<Pyramid<double>*>(_Nclass));
		for (unsigned char i=0 ; i<_Nclass ; i++)
			P_joint[k][i] = new Pyramid<double>( P_forward[0] );
	}
	
	// initialisation � 0 des proba a posteriori
	for (int e=lastScale+1 ; e>=0 ; e--){
		// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
		const unsigned int _sizeResolution = pow(4,baseScale-e);
		for(int pos=_sizeResolution-1 ; pos>=0 ; pos--){
			for(char k=lastClass ; k>=0 ; k--) 
			{
				P_marginal[k]->set_pixel(e,pos,0.);
				for(char l=lastClass ; l>=0 ; l--) 
					P_joint[k][l]->set_pixel(e,pos,0.);
			}
		}
	}

	/*** Tirages d'un quadtree d'�tiquettes selon les param�tres */
	
	
	for (unsigned char n=0 ; n<N_STOCHASTIC ; n++){
		// tirage d'un quadtree simul�
		Pyramid<unsigned char> * X = simulX(classes);
		// init � la + petite �chelle
		P_marginal[X->get_pixel(baseScale,0)]->get_pixel(baseScale,0)+=1./static_cast<float>(N_STOCHASTIC);
		for (int e=lastScale ; e>=0 ; e--){
			// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
			const unsigned int _sizeResolution = pow(4,baseScale-e);
			for(int pos=_sizeResolution-1 ; pos>=0 ; pos--)
			{
				P_marginal[X->get_pixel(e,pos)]->get_pixel(e,pos)+=1./static_cast<float>(N_STOCHASTIC);
				P_joint[X->get_father(e,pos)][X->get_pixel(e,pos)]->get_pixel(e,pos)+=1./static_cast<float>(N_STOCHASTIC);
			}
		}
		//X->getbase()->show();
		delete X;
	}

	
	// update des proba de transitions aij
	for (unsigned char i=0 ; i<_Nclass ; i++)
	{
		// somme des proba marginales a posteriori sur toutes les �chelles
		double Smarginal=0.;
			
 		// somme des proba conjointes a posteriori pour chaque classe j sur toutes les �chelles
 		vector<double> Sjoint(_Nclass,0.);
		
		for (int e=lastScale ; e>=0 ; e--) 
		{
			
 			// nombre de pixels, dans cette resolution = (2^(Nscale-1-e))^2
 			const unsigned int _sizeResolution = pow(4,baseScale-e);
 			for(int pos=_sizeResolution-1 ; pos>=0 ; pos--)
 			{
				Smarginal += P_marginal[i]->get_father(e,pos);
 				for (char j=lastClass ; j>=0 ; j--)
 					Sjoint[j] += P_joint[i][j]->get_pixel(e,pos);
 			}
 		}
		
		for (unsigned int j=0 ; j<_Nclass ; j++)
		{
			if (Smarginal>eps)
				classes[i]->setA(j,Sjoint[j]/Smarginal);
			else
			{
				cerr<<"\nSEMQuadTree::update_apriori : Somme marginal aposteriori <eps classe "<<(int)i<<endl;
				classes[i]->setA(j,1./static_cast<float>(_Nclass));
			}
//			cout << "\nnouveau a["<<i<<"]["<<j<<"] = "<<classes[i]->getA(j)<<endl;
		}
	}
	
	
	// update des proba a priori de chaque classe
	// avec les valeurs tout en haut de l'arbre
	double S=0.;
	for (unsigned int k=0 ; k<_Nclass ; k++)
	{
		classes[k]->setP(P_marginal[k]->get_pixel(baseScale,0));
		S+=classes[k]->getP();
//		cout << "nouveau P"<<k<<" = "<<classes[k]->getP()<<endl;
	}

	// verification de l'int�grit� des proba que l'on vient de calculer
	if (fabs(1.0-S)>0.01)
	{
		cerr << "SEMQuadtree::update_apriori : Somme des propabilites a priori incorrecte : " << S <<endl;
		for (unsigned int k=0 ; k<_Nclass ; k++)
		{
			cerr << "classe : "<<k<<endl;
			cerr << "xsi[Nscale-1][0] = "<< P_marginal[k]->get_pixel(baseScale,0) << endl;
		}
		ERR("");
	}

	
//	for(unsigned char k=0 ; k<_Nclass ; k++) 
//		delete P_marginal[k];
	
	if (!P_joint.empty()){
		for (unsigned char k=0 ; k<P_joint.size() ; k++)
			for (unsigned char l=0 ; l<P_joint[k].size() ; l++)
				delete P_joint[k][l];
		P_joint.clear();
	}
	
//	cout << "End update apriori with SEM\n";
	
}
