Context:
--------
MARSIAA software has been created in the framework of the IDHA (Images Distribuées Hétérogènes pour l'Astronomie) project aiming at developing methods and tools for the integration of query results on heterogeneous services distributing images in the context of the Virtual Observatory (VO). The VO groups the CDS (Centre de Données astronomiques de Strasbourg), the LSIIT (Laboratoire des Sciences de l'Image, de l'Informatique et de la Télédétection, university of Strasbourg), the Queens University (Belfast), the IAP (Institut d'Astrophysique de Paris) and the LAM (Laboratoire Astrophysique de Marseille) together.

The IDHA project has been retained in 2001 by the scientific committee of the ACI (Action Concertée Incitative) GRID (Globalisation des Ressources Informatiques et des Données) of ministry of technology and research.

Description:
------------
MARSIAA software implements an advanced segmentation method, based on Markovian statistical models, to analyze the statistical behavior of pixel values in a set of multiband images. It provides a classification of pixel data stemming from the multispectral image planes and generates a segmentation map, showing the distribution of classes in the field. 
Basic statistics, like dynamic range and moments for each class are provided too, in output extra files. The input images should of course be fully registered and have identical sampling. Choices include the algorithms for noise statistics estimations and probability estimations, number of iterations, etc. Each pixel on the sampling grid will be assigned a label (i.e a class number), depending on the pixel values at this position, among the various image planes and on the distribution of the labels in a neighbourhood. The propagation of the labels is computed according to the in-scale Markovian model and is realised using a quadtree.

Automatic installation:
-----------------------
   * Invoke 'make' on the command line to compile marsiaa.

Documentation:
--------------
   * Check 'Marsiaa.conf' for an example of a configuration file including comments;
   * detailed documentation is available, in french only, in the folder 'doc';
   * check http://lsiit-miv.u-strasbg.fr/paseo/software.php for information on softwares (including MARSIAA) developed at the LSIIT in collaboration with the astronomical community.

Contact:
--------
   * Ch. Collet (professor): c.collet@unistra.fr
