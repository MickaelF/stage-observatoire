#ifndef _MATH_H_
#define _MATH_H_

using namespace std;

#include <math.h>
#include <iostream>

double M1(const double *, const double *, const unsigned long);
double M1(const double **, const double **, const unsigned long, const unsigned long);
double Moment(const unsigned int, const double, const double *, const double *, const unsigned long);
double Moment(const unsigned int, const double, const double **, const double **, const unsigned long, const unsigned long);
double Mdist(const double *, const double *, const int);
bool Mludcmp(double **, int , int *, float *);
void Mlubksb(double **, int , int *, double []);
void Minverse(int, double **, double **);
double Mdet(int , double **);
void cholds(double **, int , double **);
float polevlf(float xx, float *coef, int N);
double Digamma(const double);
double Digamma(const unsigned int );

#endif
