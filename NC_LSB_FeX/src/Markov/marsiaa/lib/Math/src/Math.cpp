#include "Math.h"
#include <cstdlib>
#define TINY 1.0e-20;    //A small number.

/** Calcul le moment d'ordre 1 centr�, reduit, pond�r�
 * Input : 
 *  	x 			: donn�es d'entr�es
 *		p 			: poids ou probabilit� de chaque donn�e
 *		size		: longueur des donn�es
 */		
double M1(const double *x, const double *p, const unsigned long size)
{
	double sum=0., sumP=0.;
	for (long i=size-1 ; i>=0 ; i--)
	{
		sum += p[i]*x[i];
		sumP += p[i];
	}
	
	if (sumP!=0)
		sum /= sumP;
	else
		sum=0.;

	return sum;
}
double M1(const double **x, const double **p, const unsigned long w, const unsigned long h)
{	return M1(*x,*p,w*h); }

/** Calcul le moment d'ordre n centr�, reduit, pond�r�
 * Input : 
 *		n 			: ordre du moment
 *  	x 			: donn�es d'entr�es
 *		p 			: poids ou probabilit� de chaque donn�e
 *		m1			: moment d'ordre 1
 *		size		: longueur des donn�es
 */		
double Moment(const unsigned int n, const double m1, const double *x, const double *p, const unsigned long size)
{
	if (n<2)
	{
		cerr << "Can not evaluate Moment of order " << n << endl;
		exit(n);
	}
	
	double sum=0., sumP=0.;
	for (long i=size-1 ; i>=0 ; i--)
	{
		sum += p[i]*pow( x[i]-m1 ,(int)n);
		sumP += p[i];
	}

	if (sumP!=0)
		sum /= sumP;
	else
		sum=0.;

	return sum;
}
double Moment(const unsigned int n, const double m1, const double **x, const double **p, const unsigned long w,
const unsigned long h)
{ return Moment(n,m1,*x,*p,w*h); }


/** Calcul la distance euclidienne entre 2 vecteurs 
 * Input :	const double * v1 : vecteur a comparer
 * 		const double * v2 : vecteur a comparer
 * 		const int size : taille des vecteurs
 * Output :	distance euclidienne
*/
double Mdist(const double *v1, const double *v2, const int size)
{
	double sum=0.;
	
	for (int i=0 ; i<size ; i++)
		sum+=pow(v1[i]-v2[i],2);
				
	return (sqrt(sum));
}
																																							
/* D�composition en LU, m�thode du Numerical Recipes avec double 
	Entrees :
			double ** a : Matrice de var/cov (carr�e !) remplac�e par la d�composition
			int n 			: Longueur de la matrice a
			int * indx	: Tableau contenant les permutations des lignes
			float * d	: Vaut +- 1 suivant le nombre paire ou impaire de permutations
*/

bool Mludcmp(double **a, int n, int *indx, float *d)
{
	int i,imax,j,k;
	double big,dum,sum,temp;
	double vv[n];   /* vv stores the implicit scaling of each row.*/
	
	*d=1.0;   /* No row interchanges yet.*/

	/* Loop over rows to get the implicit scaling information */
	for (i=0;i<n;i++) 
	{   
		big=0.0;   
		for (j=0;j<n;j++)
			if ((temp=fabs(a[i][j])) > big) 
				big=temp;
		
		if (big == 0.0) 
		{
			cerr << "Singular matrix in routine ludcmp\n";
			return false;
		}
		
		/* No nonzero largest element.*/
		vv[i]=1.0/big;   /* Save the scaling.*/
	}
	
	/* This is the loop over columns of Crout's method.*/
	for (j=0;j<n;j++) 
	{   
		imax=j;
		/*  This is equation (2.3.12) except for i = j.*/
		for (i=0;i<j;i++) 
		{  
			sum=a[i][j];
			for (k=0;k<i;k++)
				sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
		}
		
		big=0.0;    /*Initialize for the search for largest pivot element.*/
		
		/*    This is i = j of equation (2.3.12) and i = j +1 . . . N*/
		for (i=j;i<n;i++) 
		{
			sum=a[i][j];
			for (k=0;k<j;k++)
				sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
			
			/* Is the figure of merit for the pivot better than the best so far?*/
			if ( (dum=vv[i]*fabs(sum)) >= big) 
			{
				big=dum;
				imax=i;
			}
		}

		if (j != imax) 
		{/*    Do we need to interchange rows? */
			for (k=0;k<n;k++) 
			{ /*   Yes, do so...*/
				dum=a[imax][k];
				a[imax][k]=a[j][k];
				a[j][k]=dum;
			}
			*d = -(*d);  /*  ...and change the parity of d.*/
			vv[imax]=vv[j];  /*  Also interchange the scale factor.*/
		}

		indx[j]=imax;
		
		/* If the pivot element is zero the matrix is singular (at least to the precision of the
		algorithm). For some applications on singular matrices, it is desirable to substitute
		TINY for zero.*/
		if (a[j][j] == 0.0) 
			a[j][j]=TINY;
		
		/*    Now, finally, divide by the pivot element.*/
		if (j != (n-1)) 
		{
			dum=1.0/(a[j][j]);
			for (i=j+1;i<n;i++) 
				a[i][j] *= dum;
		}
	}  /*  Go back for the next column in the reduction.*/
	return true;
}

void Mlubksb(double **a, int n, int *indx, double b[])
{
	int i,ii=-1,ip,j;
	double sum;
	
	/* When ii is set to a positive value, it will become the
	index of the first nonvanishing element of b. We now
	do the forward substitution, equation (2.3.6). The
	only new wrinkle is to unscramble the permutation
	*/

	for (i=0;i<n;i++) 
	{    
		ip=indx[i];    
		sum=b[ip];    
		b[ip]=b[i];    
		if (ii!=-1)
			for (j=ii;j<=i-1;j++) 
				sum -= a[i][j]*b[j];
		else 
			/* If a nonzero element was encountered, so from now on we
			will have to do the sums in the loop above.*/
			if (sum!=0) 
				ii=i;
		
		b[i]=sum;
	}
	/* Now we do the backsubstitution, equation (2.3.7).*/
	for (i=n-1;i>=0;i--) {   
		sum=b[i];
		for (j=i+1;j<n;j++) 
			sum -= a[i][j]*b[j];
			
		/* Store a component of the solution vector X.*/
		b[i]=sum/a[i][i];  
	}
}

void Minverse(int n, double **mat, double **y)
{
	// d�claration des tableaux n�cessaires pour la decomposition LU 
	int indx[n];
	double **a = new double *[n];
	for (int i=0 ; i<n ; i++)
		a[i]=new double[n];
		
	// copie des donnees pour les calculs 
	for (int i=0 ; i<n ; i++)
	for (int ii=0 ; ii<n ; ii++)
		a[i][ii]=mat[i][ii];
	
	// decomposition LU 
	float d;
	if(!Mludcmp(a,n,indx,&d))
	{
		// liberations de memoire 
		for (int i=0 ; i<n ; i++)
			delete[] a[i];
		delete[] a;
		return;
	}
	
	double col[n];
	for(int j=0;j<n;j++) {
		
		for(int i=0;i<n;i++) 
			col[i]=0.0;
		
		col[j]=1.0;
		Mlubksb(a,n,indx,col);
		for(int i=0;i<n;i++) 
			y[i][j]=col[i];
	
	}

	// liberations de memoire
	for (int i=0 ; i<n ; i++)
		delete[] a[i];
	delete[] a;

}

double Mdet(int n, double **mat)
{
	/* d�claration des tableaux n�cessaires pour decomposition LU */
	int indx[n];
	double **a = new double *[n];
	for (int i=0 ; i<n ; i++)
		a[i]=new double[n];
	
	/* copie des donnees pour les calculs */
	for (int i=0 ; i<n ; i++)
	for (int ii=0 ; ii<n ; ii++)
		a[i][ii]=mat[i][ii];

	/* decomposition LU */
	float d;
	double det;
	
	if (Mludcmp(a,n,&(indx[0]),&d))	 // This returns d as +- 1.
	{
		det=static_cast<double>(d);
		/* multiplication des valeurs en diagonales = determinant */
		for(int j=0;j<n;j++)
			det *= a[j][j];
	}
	else
		det=0.;
					
	/* liberations de memoire */
	for (int i=0 ; i<n ; i++)
		delete[] a[i];
	delete[] a;

	return det;
}


void cholds(double **a, int n, double **L)
{

	int i,j,k;
	float sum;
	for (i=0;i<n;i++) {
		for (j=i;j<n;j++) {
			for (sum=a[i][j],k=i-1;k>=0;k--) sum -= L[i][k]*L[j][k];
			if (i == j) {
				if (sum <= 0.0) 
				{   //a, with rounding errors, is not positive definite.
					cerr << "cholds failed\n";
					return;
				}
				L[i][i]=sqrt(sum);
			}
			else 
			{
				L[j][i]=sum/L[i][i];
				L[i][j]=0.;
			}
		}
	}

}


// Euler-Mascherioni constant
#define EUL 0.57721566490153286061
#include "mconf.h"

static float A[] = {
-4.16666666666666666667E-3,
 3.96825396825396825397E-3,
-8.33333333333333333333E-3,
 8.33333333333333333333E-2
};

float polevlf(float xx, float *coef, int N){
  float ans, x;
  float *p;
  int i;

  x = xx;
  p = coef;
  ans = *p++;
  i = N;

  do
    ans = ans * x  +  *p++;
  while(--i);

  return ans;
}

double Digamma(const unsigned int x)
{
	// check for positive integer up to 10 
	if(x <= 10.0)
	{
		double y = 0.;
		for(unsigned int i=1; i<x; i++ )
			y += 1./static_cast<float>(i);

		y -= EUL;
		return(y);
	}
	else // over 10
		return Digamma(static_cast<double>(x));
	
}

double Digamma(const double _x)
{
	double w=0., z=0., y=0., x=_x ;
	while( x < 10.0 )
	{
		w += 1.0/x;
		x += 1.0;
	}

	if( x < 1.0e8 )
	{
		z = 1.0/(x * x);
		y = z * polevlf( z, A, 3 );
	}
	else
		y = 0.0;

	y = logf(x)  -  (0.5/x)  -  y  -  w;
	
	return y;
}


