/*
 *   This file was automatically generated by version 1.7 of cextract.
 */

extern double digamma ( double x );

extern double polevl ( double x, void *P, int N );
extern double log ( double x );
extern double floor ( double x );
extern double tan ( double x );
extern int mtherr ( char *name, int code );

extern double chbevl ( double x, void *P, int n );
extern double dawsn ( double xx );
extern double expn ( int n, double x );
extern double fac ( int i );
extern int fresnl ( double xxa, double *ssa, double *cca );extern double p1evl ( double x, void *P, int N );
extern void revers ( double y[], double x[], int n );
extern double rgamma ( double x );
extern int shichi ( double x, double *si, double *ci );
extern int sici ( double x, double *si, double *ci );
extern double simpsn ( double f[], double delta );
extern double spence ( double x );
extern double zeta ( double x, double q );
extern double zetac ( double x );
extern double sqrt ( double x );
extern double fabs ( double x );
extern double pow ( double x, double y );
extern double gamma ( double x );
extern double exp ( double x );
extern double sin ( double x );
extern double cos ( double x );
extern double lgam ( double x );


