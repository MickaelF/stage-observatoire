####################################################
###        CONFIGURATION FILE FOR MARSIAA        ###
####################################################

# This file describes the settings to be used by
# the segmentation with Marsiaa
#
# To have more information about the options of marsiaa, type "marsiaa --help"
#
# All text after a hash (#) is considered a comment and will be ignored
# The format is:
#       TAG = value1 value2 ...
#
# For using Default value, let a blank :
# 	TAG = 

#---------------------------------------------------------------------------
# Input Configuration
#---------------------------------------------------------------------------

# Input files
# Give path of all input images (separated with blank, on only one line)
# Default take arguments from the command line
# Exemple : /Images/M82/2MASSJ.fits /Images/M82/DSS2B.fits

INPUT		= 

# Mask file
# You can give a mask as a fits file (0 for rejected pixels)
# For no mask, let a blank
# Default is no mask

MASK 	= 

# Path of files for optimized scanning for MARKOV=CHAIN
# Give the path where you have stored the directory named scan given with Marsiaa.
# This path must be absolut (begin by /) like : /usr/local/marsiaa/
# Default is the local directory "./"

SCAN	= ./

#---------------------------------------------------------------------------
# Configuration for processing
#---------------------------------------------------------------------------

# Estimation algorithm
# EM : Expectation Maximisation
# SEM : Stochastic EM
# ICE : Iterative Conditional Estimation
# You can choose in : EM ; SEM ; ICE
# Default is ICE

ESTIM		= EM


# Initial number of classes
# You can give a number or choose in : # not implemented  FUZZY ; ESTIM
# Default is 4

CLASS		= 8

# Markovian Model
# You can choose in : CHAIN ; QUADTREE # not implemented FIELD
# Default is QUADTREE

MARKOV		= 

# Noise Model
# You can choose in : GAUSS ; GENERAL_GAUSS ; LOGNORMAL # not implemented WEIBULL ; POISSON ; AUTO
# Default is GAUSS

NOISE		= GAUSS

# Use a decorrelattion method on images 
# You can choose in : NO ; CHOLESKY # not implemented  ICA
# Default is NO

DECORR		= 


# Maximum number of iterations
# Default is 20

IT		= 12

# Convergence criterium
# PC_CHANGE : % of sites changed during the last 3 iterations
# (not yet implemented) PC_DIST : % of the distance between vectors of parameters of 
# the last 2 iterations
# Default is PC_CHANGE 3

STOP		= PC_CHANGE 3


#---------------------------------------------------------------------------
# Output configuration
# --------------------
# MAP : the segmented map
# STATS : an ascii table file ($OUTPUT_stats.txt, each line is one class)
# 	with all informations obtained at the end of process.
# TRACE : trace file($OUTPUT_trace.txt), with evolution of statistics.
# DECORR : image decorrelated (if asked in DECORR tag) ($OUTPUT_z#.fits)
# --------------------
# Allways save MAP and STATS
#---------------------------------------------------------------------------

# Output directory and root names. It will be added "_" and name of identification.
# Default is the local directory (./)
# Exemples : = MesImages/SegmentationMARSIAA/NGC1978 ; =

OUTPUT		= test_9_cl_3245_masked_asinh_iter2

# Do you want to save the TRACE file ?
# You can choose between : YES or NO
# Default is NO

TRACE	= YES

# Do you want to save the multiscale map ? 
# This map include each scale (<8) if number of class <15
# You can choose between : YES or NO
# Default is NO

MAPMS		= 

#---------------------------------------------------------------------------
# Display configuration options
#---------------------------------------------------------------------------

# What you want to display ? 
# You can choose one or some of : MAP ; DECORR ; INPUT (input images) ; MAPMS
# 	Images ares saved as $OUTPUT_<id>_it#.fits
# You can add an integer value after the name, to specify each N iterations to display
# If no number is given, only display at the end.
# Default is a blank (no display)
# Examples : = MAP 10 DECORR ; = MAP

DISPLAY_MODE	=

# With which application ? (only for images)
# There is no limit of application choice (must be installed)
# Default is xv
# Examples : xv ; Aladin ; ds9 ; gimp ;

DISPLAY_APP	= 

