#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## compute_mask.py
#Usage : python compute_mask.py file1 file2 outputfile
#Description : Compute the mask of file1 with respect to file1, i.e. the image which is nan if 
#+             file1 or file2 is nan for that pixel, else is the value of file2
#Author : Mickaël FERRERI
#Date : June 24, 2019

import os
import sys
from astropy.io import fits
import math


hdul = []

hdulist = fits.open(sys.argv[1])
hdu = hdulist[-1]
hdul.append(hdu.data)

hdulist2 = fits.open(sys.argv[2])
hdu2 = hdulist2[-1]
hdul.append(hdu2.data)

n = len(hdul[1])
m = len(hdul[1][0])

data = [ [0 for j in range(m)] for i in range(n)]

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

# Initial call to print 0% progress
printProgressBar(0, n, prefix = 'Progress:', suffix = 'Complete', length = 50)

for i in range(n):
	for j in range(m):
		if math.isnan(hdul[0][i][j]) or math.isnan(hdul[1][i][j]):
			data[i][j] = float("nan")
		else:
			data[i][j] = hdul[1][i][j]
	printProgressBar(i, n, prefix = 'Progress:', suffix = 'Complete', length = 50)

ret = fits.PrimaryHDU(data)
os.system("rm "+sys.argv[3])
ret.writeto(sys.argv[3])






