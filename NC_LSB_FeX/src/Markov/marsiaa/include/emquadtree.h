/***************************************************************************
                          emquadtree.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef EMQUADTREE_H
#define EMQUADTREE_H

#include "DefineMarsiaa.h"

#include <quadtree.h>
class Data;
class NoiseModel;

/**
  *@author Ana�s OBERTO
  */

class EMQuadTree : public QuadTree  {
public: 
	EMQuadTree();
	~EMQuadTree();

 /** No descriptions */
  void update_apriori(vector < Data * > & , vector < SegmClass * > & );
};

#endif

