/***************************************************************************
                          weibull.h  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@cacao.u-strasbg.fr
 ***************************************************************************/


#ifndef WEIBULL_H
#define WEIBULL_H

#include "DefineMarsiaa.h"

#include <noisemodel.h>

/**
  *@author oberto
  */

class Weibull : public NoiseModel  {
public: 
	Weibull();
	virtual ~Weibull();
  /** Update data driven */
  void update_dataDriven(const vector < Data * > & , const Data * );
  /** Evaluate statistical likelihood */
	double likelihood(const vector < double > & );

  /** No descriptions */
  bool isWeibull() const;
  /** No descriptions */
   Weibull(const NoiseModel&);
  /** getParam : get one parameter of this noise
	* @param _p : number corresponding to one parameter 
	*/
   const vector<double> getParam(const unsigned short _p);
protected: // Protected attributes
  /**  */
  vector<double> min;
  /**  */
  vector<double> shape;
  /**  */
  vector<double> scale;
};

#endif
