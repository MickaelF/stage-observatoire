/***************************************************************************
													config.h	-	description
														 -------------------
		begin								: ven ao� 16 2002
		email								: oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef INPUT_H
#define INPUT_H

#include "DefineMarsiaa.h"


/**
	*@author Ana�s OBERTO
	*/

#include <fstream>
#include <map>
#include <sstream>

class Config {
public: 
	Config();
	Config(string);
	virtual ~Config();
	/** Read property of Estimation in Config file */
	const short getEstim();
	/** Read property of Markovian model in Config file */
	const short getMarkovModel();
	/** Write property of int Nclass. */
	//	void setNclass( const int _newVal);
	/** Read property of Number of classes in Config file. */
	const int getNclass();
	/** Write property of int Ndata. */
	//	void setNdata( const int _newVal);
	/** Read property of int Ndata. */
	//	const int getNdata();
	/** Read property of Noise model in Config file */
	const short getNoiseModel();
	/** Read property of method of ## DATA DRIVEN ## initialization in Config file */
	const short getInit();
	/** Read property of method of ## A PRIORI ## initialization in Config file */
	const short getInitP();
  /** Read function to know if option to visualize map is given in config file */
  bool getVisuMap();
  /** Read function to know which soft for visualize images */
  string getVisuApp();
  /** Returns the MAP_SUFFIX parameter */
  bool getMapSuffix();
  /** Give the name to save the MAP */
  string getOutMAP();
  /** Give the name to save the STATS file */
  string getOutStats();
  /** Give the name to save the TRACE file */
  string getOutTrace();
  /** Erase last output with the string */
  bool setOutput(string &);
	/** Complete vector of path of config images and return number of files */
//	virtual int getfiles(vector<string> &);
	/** Return path name for saving files */
  string getOutput();
  /** No descriptions */
  unsigned short getDecorr();
  /** No descriptions */
  string getMask();
	/** setMask */
	void setMask(const string &s);
  /** getInputFiles */
  vector<string> getInputFiles();
  /** setInputFiles */
  void setInputFiles(const vector<string> &);
  /** No descriptions */
  unsigned int getItMax();
  /** No descriptions */
  float getStop();
  /** * @return : value corresponding to the convergence criterium */
  const short getStopTest();
  /** getVisuFreq
  * @return frequency of visualisation
  * @param _image : string like "MAP" or "DECORR" of frequency to get
  */
  unsigned int getVisuFreq(const string & _image);
  /** Read function to know if option to visualize decorrelated images is given in config file */
  bool getVisuDecorr();
  /** getTrace
	* @return : yes if trace is asked, no otherwise */
  const bool getTrace() ;
  /** No descriptions */
  void eraseVisu(const string & _s);
  /** No descriptions */
  void setVisu(const string & _newString);
  /** getScan */
  const string getScan() ;
  /** Read function to know if option to visualize config images is given in config file */
  bool getVisuInputig();
  /** getVisuMapMS */
  bool getVisuMapMS();
  /** getMapMS (Multi Scale map) */
  bool getMapMS();
  /** getOutMapMS */
  string getOutMapMS();
  /** @return the list of options in one string */
  string getString();
  /** Give filenames in case of AUTO option in NOISE tag. */
  vector<string> getNoiseFiles();
	
private: // Private attributes
	/** Map of all TAGs and associate values */
	map<string,string> Mtags;
	/**	*/
	int Nclass;
	/**	*/
	int Ndata;
  /** Extract only string without white space */
  string string2string(const string &);
	/** Extract only string without white space */
	bool string2string(const string &, string &);
	/** Extract only integer value without white space */
	bool string2int(const string &, int&);
  /** Extract all strings without white space */
  vector<string> string2strings(const string & );
  /** Extract only real value without white space
* @param _in : string to explore
* @param f : value extracted
* @return : true if no problem, false if there is no real value
 */
  bool string2float(const string & _in, float & f);

};

#endif
