/***************************************************************************
                          icechain.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef ICECHAIN_H
#define ICECHAIN_H

#include "DefineMarsiaa.h"

#include <chain.h>
class Data;
class NoiseModel;

/**
  *@author Ana�s OBERTO
  */

class ICEChain : public Chain  {
public: 
	ICEChain();
	~ICEChain();
  /** No descriptions */
  void visit(vector<Data *> &, vector<SegmClass *> &);
  /** update a priori parameters, Pk and aij*/
  void update_apriori(vector < Data * > & , vector < SegmClass * > & );
};

#endif
