/***************************************************************************
                          lognormal.h  -  description
                             -------------------
    begin                : lun mar 24 2003
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef LOGNORMAL_H
#define LOGNORMAL_H

#include <noisemodel.h>

/**Log Normal Distribution
  *@author Ana�s OBERTO
  */

class LogNormal : public NoiseModel  {
public: 
	LogNormal();
	LogNormal (const NoiseModel& _copie);
	LogNormal (const LogNormal& );
	~LogNormal();
   
  /** operator =  */
  LogNormal& operator = (const LogNormal& );
  
  virtual bool isLogNormal() const;
  /** Evaluate statistical likelihood */
  double likelihood(const vector < double > & );
  /** Update data driven */
  void update_dataDriven(const vector < Data * > & , const Data * );
 
protected: // Protected attributes

  /** getParam : get one parameter of this noise
   * @param _p : number corresponding to one parameter 
   */
  const vector < double > getParam(const unsigned short _p);

  /** Evaluate and modify parameter min (weighted) with Maximum Likelihood method. */
  double evalmin(const unsigned int ndata, const Data * data, const Data * x);
  /** Evaluate and modify parameter scale (weighted) with Maximum Likelihood method. 
  * @pre use evalmin BEFORE, to be sure that xi-min > 0
  */
  double evalscale(const unsigned int ndata, const Data * data, const Data * x);
  /** Evaluate and modify parameter shape (weighted) with Maximum Likelihood method. 
  * @pre use evalscale BEFORE
  */
  double evalshape(const unsigned int ndata, const Data * data, const Data * x);


//protected: // Protected attributes
  /** minimum */
  vector<double> min;
  /** shape parameter (sigma) */
  vector<double> shape;
  /** Scale parameter (mu) */
  vector<double> scale;
public: // Public attributes
  /** MinLocal */
  static bool MinLocal;
};

#endif
