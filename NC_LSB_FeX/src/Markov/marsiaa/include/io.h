/***************************************************************************
                          io.h  -  description
                             -------------------
    begin                : ven ao� 23 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef IO_H
#define IO_H

#include "DefineMarsiaa.h"

#include "fitsio.h"
#include <unistd.h> // pour la fonction unlink()
#include <bitset>

template<typename TType> class Image;
template<typename TType> class Pyramid;

/**Config and Output functions
 *@author Ana�s OBERTO
 */

class IO {
 public: 
  IO();
  ~IO();
  /** Read given fits file and store image */
  static void importFits(Image<double>*, const string & );
  /* Get pixel values from scan image */
  static unsigned int scan_get_pixel(int);
  
  /** Write given image in fits format.
   *
   * Output :
   *		0 if no problem
   *		otherwise : exit writing errors in stderr
   */
  template<typename TType>
    static int exportFits(Image<TType>* im, string filename,bool use_scan = 1)
    {
      int statusW = 0;  // MUST initialize status
      long naxes[2];
      naxes[0]=im->getwidth();
      naxes[1]=im->getheight();
      long size = naxes[0]*naxes[1];
      
      fitsfile *fptrW;
      
      unlink(filename.c_str());
      fits_create_file(&fptrW, filename.c_str(), &statusW);
      
      fits_create_img(fptrW, -32/*DOUBLE BITPIX*/, 2, naxes, &statusW);
      
      // copie data on local double array
      double *_data;
      _data = new double [size];
      
      if (scan!=NULL && use_scan)
      	{
      	  for (int i=0 ; i<size ; i++)
	    	    _data[i] = static_cast<double>(im->get_pixel(scan_get_pixel(i)));
      	}
       else
	for (int i=0 ; i<size ; i++)
	  _data[i] = static_cast<double>(im->get_pixel(i));
      
      fits_write_img(fptrW, TDOUBLE, 1,
		     size, _data,
		     &statusW);
      
      fits_close_file(fptrW, &statusW);
      
      try{
	if (statusW) {
	  fits_report_error(stderr, statusW);
	  throw(-1);
    	}
      }
      catch (...) { throw; }
      
      return statusW;		
    };
  
  /** Write given image in fits format.
   *
   * Output :
   *		0 if no problem
   *		otherwise : exit writing errors in stderr
   */
  template<typename TType>
    static int exportFits(Pyramid<TType>* im, string filename)
    {
      int statusW = 0;  // MUST initialize status
      long naxes[2];
      naxes[0]=im->getwidth();
      naxes[1]=im->getheight();
      long size = naxes[0]*naxes[1];
      
      fitsfile *fptrW;
      
      unlink(filename.c_str());
      fits_create_file(&fptrW, filename.c_str(), &statusW);
      
      fits_create_img(fptrW, 32/*UNSIGNED LONG BITPIX*/, 2, naxes, &statusW);
      
      // copie data on local double array
      double *_data;
      const int max_ech = (im->getNscale()<8)?im->getNscale():8 ;
      _data = new double [size];
      
      // if max nclass < 15 (=2^4)
      for (int i=0 ; i<size ; i++)
	{
	  // pixel courant
	  bitset<32> bs(static_cast<unsigned long>(im->get_pixel(i)));
	  
	  // a chaque echelle on recupere la valeur du pere
	  for (unsigned char e=1 ; e<max_ech ; e++)
	    {
	      // on transforme la valeur en binaire
	      bitset<32> bsi(static_cast<unsigned int>( im->get_ancestor(e,0,i) ));//im->get_father(e,i)) );
	      
	      // decale de 4 bit
	      bsi <<= (4*(e));
	      
	      // ajoute dans le pixel courant
	      bs |= bsi;
	    }
	  _data[i] = static_cast<double>(bs.to_ulong());
	}
      
      fits_write_img(fptrW, TDOUBLE, 1,
		     size, _data,
		     &statusW);
      
      fits_close_file(fptrW, &statusW);
      
      try{
	if (statusW) {
	  fits_report_error(stderr, statusW);
	  throw(-1);
    	}
      }
      catch (...) { throw; }
      
      return statusW;		
    };
  
  /** the way to scan the image (as an iterator) */
  static Image<unsigned int> const * scan;
  static void setscan(const Image<unsigned int> * const _newScan);
  /** copy_FitsHeader
      @param _ifile : file name of the fits file to extract header
      @param _ofile : file name of the output fits file
      @param history : string to add in history header */
  static bool copy_FitsHeader(const string & _ifile, const string & _ofile, const string &history);
  
};


#endif
