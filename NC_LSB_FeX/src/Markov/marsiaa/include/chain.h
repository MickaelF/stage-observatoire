/***************************************************************************
                          chain.h  -  description
                             -------------------
    begin                : lun ao� 19 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef CHAIN_H
#define CHAIN_H

#include "DefineMarsiaa.h"

#include <markovmodel.h>
class NoiseModel;
class Data;
class SegmClass;
 
//typedef std::vector<std::vector<double> > dMatrix;
template <typename TType> class Image;

/**
  *@author Ana�s OBERTO
  */

class Chain : public MarkovModel  {
public: 
	Chain();
	virtual ~Chain();
  /** No descriptions */
  void update_apriori();
  /** Update Data-driven parameters */
  void update_dataDriven(const vector<Data *>&, vector<SegmClass *>&);
  /** Simulate a solution */
  void calcul_X();
  
  /** Evaluate "a posteriori" joint probability : P(Xn=wk,Xn+1=wl | Y=y) */
  //Image<double> * getP_joint_aposteriori(const vector < Data * > & , vector < SegmClass * > & , const unsigned int _k, const	unsigned int _l);
  dMatrix getP_joint_aposteriori(const vector < Data * > & , vector < SegmClass * > & , const	unsigned int _pos);
  /** Evaluate "a posteriori" joint probability : P(Xn=wk,Xn+1=wl | Y=y) for a position */
  double getP_joint_aposteriori(const vector < Data * > & data, vector < SegmClass * > & classes, const unsigned int _k, const
	unsigned int _l, unsigned int _pos);

  /** return marginal=P_forward*P_backward for one position */
  virtual double getP_marginal_aposteriori(const unsigned int k, const unsigned int _pos, const unsigned int _scale=0)const;
  /** browse forward and backward */
  void browse(const vector < Data * > & , const vector < SegmClass * > & ) ;
  /** Browse minimum to initialise an a posteriori probability */
  void init_aposteriori(const vector < Data * > & , const vector < SegmClass * > & );
  /** return a marginal Data object (Image) */
  virtual Data * getP_marginal_aposteriori(const unsigned short k);
  /** browse forward and backward using external likelihood */
  virtual void browse(const vector < Data * > & );

protected: // Protected attributes
  /** No descriptions */
  void browse_forward(const vector<Data *>&, const vector<SegmClass *>&, vector<double>&);
  /** No descriptions */
  void browse_backward(const vector<Data *>&, const vector<SegmClass *>&, vector<double>&);
  /** Initialize and allocate memory for private parameters */
  virtual void init(const vector<Data *>&,const vector<SegmClass *>&);
  /** No descriptions */
	Image<unsigned int>* simulX(const vector < Data * > & , vector < SegmClass * > &);
  /** Transition a posteriori = akl*f_l(pos)*P_backward_l_pos */
  void getTrans_aposteriori(vector<double>& trans,const vector<Data*>& data, vector < SegmClass * > & classes,const unsigned int
	_k,const unsigned int _pos);
  vector<Image<double>*>  P_backward;
	vector<Image<double>*>  P_forward;
	vector<Image<double>*> P_marginal;
};

#endif
