/***************************************************************************
                          segmentation.h  -  description
                             -------------------
    begin                : mar jan 14 2003
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include "DefineMarsiaa.h"
class Data;
class SegmClass;
class MarkovModel;
/**All processings for segmentation on images with SegmClass parameters.
  *@author Ana�s OBERTO
  */

class Segmentation {
public: 
	Segmentation();
	~Segmentation();
  /** Marginal a Posteriori Mode */
  static vector<double> MPM(Data*,MarkovModel*,const vector<Data*>&,const vector<SegmClass*> &);
};

#endif
