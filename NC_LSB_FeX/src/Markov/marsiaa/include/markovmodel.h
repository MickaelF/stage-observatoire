/***************************************************************************
                          markovmodel.h  -  description
                             -------------------
    begin                : lun ao� 19 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef MARKOVMODEL_H
#define MARKOVMODEL_H


#include "DefineMarsiaa.h"

class Data;
class SegmClass;
class NoiseModel;



/**
  *@author Ana�s OBERTO
  */

class MarkovModel {
public: 
	MarkovModel();
	virtual ~MarkovModel();
  /** Initialize and allocate memory for private parameters daughter classes */
  virtual void init(const vector<Data *>&,const vector<SegmClass *>&)=0;
  /** Update Data-driven parameters @param decorr true if decorrelation is asked */
  virtual void update_dataDriven(const vector < Data * > & data, vector < SegmClass * > & classes, bool decorr);
  /** return marginal for one position */
  virtual double getP_marginal_aposteriori(const unsigned int k, const unsigned int pos, const unsigned int _scale=0) const=0;/* {
		cerr << "MarkovModel::getP_marginal_aposteriori : Shoulnt be here !\n";
		return 0.;
	};*/
  /** No descriptions */
  virtual void update_apriori(vector < Data * > & , vector < SegmClass * > & );
  /** browse forward and backward */
  virtual void browse(const vector < Data * > & , const vector < SegmClass * > &) ;
  /** Browse minimum to initialise an a posteriori probability */
  virtual void init_aposteriori(const vector < Data * > & , const vector < SegmClass * > & ) = 0;
  /** return a marginal Data object (Image or Pyramid, according to the Markovian Model) */
  virtual Data * getP_marginal_aposteriori(const unsigned short k) = 0;
  /** browse forward and backward using external likelihood */
  virtual void browse(const vector < Data * > & ) = 0;

protected: // Private methods
  /** vector of Nclass Forward probability */
//  vector<Data*> P_forward;
  /** vector of Nclass Backward probability */
//  vector<Data*> P_backward;

};

#endif
