/***************************************************************************
 marsiaa.h  -  description
 -------------------
 begin                : ven ao� 16 2002
 email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/

#ifndef MARSIAA_H
#define MARSIAA_H

#include "DefineMarsiaa.h"

class Config;
class MarkovModel;
class SegmClass;
class Init;
class Data;
class NoiseModel;
class Decorrelation;

#include "init.h"

/**Main programm class
 *@author Ana�s OBERTO
 */

class Marsiaa {
public:
	Marsiaa();
	//	Marsiaa(Config &);
	virtual ~Marsiaa();
	/** Run Initialization choosed */
	void runInit();
	/** Segmentation with markovian model and estimation model */
	void run();
	/** Read one SegmClass class */
	SegmClass * getclasses(const int);
	/** Read the pointer of MarkovModel estimator. */
	MarkovModel * getestim();
	/** Read one pointer of Data object */
	Data * getdata(const int);
	/** get data */
	vector<Data*>& getdata();
	/** Write property of int Ndata. */
	virtual void setNdata(const int& _newVal);
	/** Read property of int Ndata. */
	virtual const int& getNdata();
	/** Create pointer on Data array as Images */
	void createData(const vector<string> &);
	/** Write property of int Nclass. */
	//  virtual void setNclass( const int& _newVal);
	/** Read property of int Nclass. */
	//  virtual const int& getNclass();
	/** Insert a mask on images
	 * @param file : reference on  file name.
	 *	
	 * @return
	 *
	 * @remarks 
	 *		If file not found, exit programm during Image::load
	 *		If configuration do not accept a mask, exit programm
	 *		Description of error is send to stderr.
	 *	
	 */
	void setMask(const string&);

	/** Set Config for Marsiaa
	 * @param _in : reference on Config object.
	 * 
	 * @return
	 * 	true if no problem,
	 *	false otherwise
	 *
	 * @remarks :
	 *	run initialisation
	 */
	bool setConfig(Config& _in, const string & maskfile);
	/** get vector of all classes */
	vector<SegmClass *>& getclasses();
	/** get map */
	Data* getmap();
	/** get map */
	Image<unsigned char>* getImageMap();

	/** use a Marginal a Posteriori Mode */
	void segmMPM(const bool reinit = false);
	/** Set the decorrelation function choosed to each class
	 *
	 * @pre
	 *		You must have created Marsiaa::classes an their NoiseModel before
	 */
	void setdecorr(const unsigned short flag);
	/** compareMaps
	 */
	double compareMaps();
	/** Evaluate distance between last vector of SegmClass and new vector of SegmClass
	 @param last old vector of classes' moments
	 */
	double compareClasses(const unsigned int k, vector<dMatrix> &last);
	/** display on standard output all statistical parameters */
	void display();
	// On reshifte/rescale les donn�es
	void unshift_unscale();
	// Met � jour l'attache aux donn�es
	void update_dataDriven();
	/** No descriptions */
	void save(const string _filename);
	/** trace */
	void trace(const string _filename);
	/** traceInit : initialize trace file (erase if allready exists) */
	void traceInit(const string);
	/** No descriptions */
	Image<double>* getDecorr(const unsigned int i);
	/** initComparison */
	void initComparison();
	/** sort */
	vector<unsigned short> sort();

private:
	/********** ATTRIBUTS **********/
	/** Pointer on Config configuration */
	Config * config;
	/** List of all classes of the segmentation */
	vector<SegmClass *> classes;
	/** Pointer of a MarkovModel */
	MarkovModel *pt_estim;
	/** Config Data vector */
	vector<Data*> data;
	/** Number of classes for processing corresponding at the begin to one of the values
	 (may be negtive) precised in DefineMarsiaa.h */
	int Nclass;

	/********** FUNCTIONS **********/
	/** Create pointer on Markovian model used : EM/SEM/ICE with Chain/QuadTree 
	 * @param
	 *		_markov : number corresponding to markovian model
	 *	@param
	 *		_estim : number corresponding to estimation method
	 * @sa DefineMarsiaa.h
	 */
	MarkovModel * createMarkovModel(const short _markov, const short _estim);

	/** Create array of classes of segmentation. */
	void createClasses(const int);

	/** Create Noise Model for the segmentation */
	NoiseModel * createNoiseModel(const int);

	/** Replace an old Noise Model by a new Noise Model */
	NoiseModel * eraseNoiseModel(const int, NoiseModel *);

	inline void swap(int x, int y);

public:
	// Public attributes
	/**  */
	int Ndata;

protected:
	// Protected attributes
	/** Segmented map */
	Data* map;

	vector<double> PPost;
};

#endif
