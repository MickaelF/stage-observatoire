/***************************************************************************
                          gauss.h  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef GAUSS_H
#define GAUSS_H

#include "DefineMarsiaa.h"

#include <noisemodel.h>
class Data;
class GenGauss;
template <typename TType> class Pyramid;
template <typename TType> class Image;



/**
  *@author Ana�s OBERTO
  */

class Gauss : public NoiseModel {
public: 
	Gauss();
	Gauss (const Gauss&);
  Gauss(const vector<double>&, const vector<double>&);
  Gauss(const GenGauss&);
  Gauss(const NoiseModel&);
	virtual ~Gauss();
  /** operator =  */
  Gauss& operator= (const Gauss&);
  bool isGauss() const;

	/********** ACCES FUNCTIONS **********/
  /** Write property of one mean. */
  void setmean(const unsigned int _capteur, const double _newVal);
  /** get mean */
  const double getmean(const unsigned int _capteur) const;
	/** get mean */
	const vector<double>& getmean() const;

  /** Read stddev. */
	void setstddev(const unsigned int _capteur, const double _newVal);
	/** get stddev */
	const double getstddev(const unsigned int _capteur) const;
	const vector<double>& getstddev() const;

  /** Evaluate statistical likelihood for Gaussian noise */
	double likelihood(const vector<double>& _values);
  /** Evaluate and modify Standard deviation parameter with Maximum likelihood method. */
	double evalstddev(const unsigned int ndata, const Data* data);
  /** Evaluate (and modify weighted)Standard deviation parameter with Maximum likelihood method. */
  double evalstddev(const unsigned int ndata, const Data* data, const Data* x);
  /** Evaluate and modify parameter mean with Maximum Likelihood method. */
  double evalmean(const unsigned int ndata, const Data* data);
  /** Evaluate and modify parameter mean (weighted) with Maximum Likelihood method. */
  double evalmean(const unsigned int ndata, const Data* data, const Data* x);

	/** Update data driven in Markovian Model */
  virtual void update_dataDriven(const vector<Data*>& data, const Data*);
  /** getParam : get one parameter of this noise
	* @param _p : number corresponding to one parameter 
      	*/
  const vector<double> getParam(const unsigned short _p);

  protected: // Protected attributes
  /** Mean parameter of a Gaussian noise */
	vector<double> mean;
  /** Standard deviation parameter of a Gaussian noise */
	vector<double> stddev;
  /** Size of each parameter objets. */
  //static int size;

};

#endif
