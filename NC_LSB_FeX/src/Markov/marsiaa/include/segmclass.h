/***************************************************************************
                          segmclass.h  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef SEGMCLASS_H
#define SEGMCLASS_H

#include "DefineMarsiaa.h"

//#define matrix std::vector< std::vector<double> >;
class NoiseModel;

/**Segmented class
  *@author Ana�s OBERTO
  */

class SegmClass {
public: 
	SegmClass();
	//SegmClass(const int);
	~SegmClass();
  
  /** Write property of double P. */
  void setP( const double );
  /** Read property of double P */
  const double getP() const;
   /** Write property of vector<double> A. */
  void setA(const int, const double);
  /** Read property of vector<double> A. */
  const double getA(const int);
 
  /** Write property of int size. */
  static void setNdata( const int );
  /** Read property of int size. */
  static const int getNdata();
  /** Read property of int Nclass. */
  static const int getNclass();
  /** Define the NoiseModel of the SegmClass. 
  *	@param n : pointer on a (new) NoiseModel allocated.
  * @remarks If NoiseModel::noise is not NULL, it is deleted.
  */
  void setNoiseModel(NoiseModel*);
  /** Give associated NoiseModel. */
  NoiseModel* getNoiseModel();
  /** Read property vector<double> A */
  vector<double> getA();
  /** Make dimensions for the A parameter */
  void makeTrans();
  /** operator <  */
  bool operator< (const SegmClass& );
  /** friend of class SegmClass
	operator <  */
  friend bool operator< (SegmClass& , SegmClass& );

  /** Size of each vector */
  static int Ndata;
  /** Number of object SegmClass */
  static int Nclass;
  
private: // Private attributes
  /**  */
  NoiseModel* noise;
  /** Probability apriori and a posteriori of the Segmclass */
	double P;
  /** Transition probability between 2 classes */
  vector<double> A;
	
};

#endif
