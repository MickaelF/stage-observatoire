/***************************************************************************
                          gengauss.h  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@cacao.u-strasbg.fr
 ***************************************************************************/


#ifndef GENGAUSS_H
#define GENGAUSS_H

#include "DefineMarsiaa.h"

#include <noisemodel.h>
class Data;
class Gauss;
template <typename TType> class Pyramid;
template <typename TType> class Image;

/**Generalized gaussian
  *@author oberto
  */

class GenGauss : public NoiseModel {
public: 
	GenGauss();
	virtual ~GenGauss();
  GenGauss (const GenGauss& );
  GenGauss& operator = (const GenGauss& );

	/********** ACCES FUNCTIONS **********/
  /** Write property of one mean. */
  void setmean(const unsigned int _capteur, const double _newVal);
  /** Read property of one mean. */
  const double getmean( const unsigned int _cpateur) const;
  const vector<double>& getmean() const;

  /** Write property of one stddev. */
  void setstddev( const unsigned int _capteur, const double _newVal);
  /** Read property of one stddev. */
  const double getstddev( const unsigned int _capteur) const;
  const vector<double>& getstddev() const;

  /** Read property of one shape. */
  const double getshape(const unsigned int _capteur) const;
  const vector<double>& getshape() const;


  /** Evaluate statistical likelihood for Gaussian noise */
//  virtual double likelihood(const Data[], const int );
  /** Evaluate statistical likelihood for Gaussian noise */
  virtual double likelihood(const double* _values);
  /** Evaluate statistical likelihood for Gaussian noise */
  virtual double likelihood(const vector<double> & );
  /** Evaluate and modify Standard deviation parameter with Maximum likelihood method. */
  double evalstddev(const unsigned int nata, const Data* data);
  /** Evaluate (and modify weighted)Standard deviation parameter with Maximum likelihood method. */
  double evalstddev(const unsigned int ndata, const Data* data, const Data* x);
  /** Evaluate and modify parameter mean with Maximum Likelihood method. */
  double evalmean(const unsigned int ndata, const Data* data);
  /** Evaluate and modify parameter mean (weighted) with Maximum Likelihood method. */
  double evalmean(const unsigned int ndata, const Data* data,const Data* x);

	/** Update data driven in Markovian Chain Model */
  virtual void update_dataDriven(const vector<Data*>&,const Data*);
  /** operator = */
  //NoiseModel & operator=(const NoiseModel & );
  /** No descriptions */
  bool isGenGauss() const;
  /** Evaluate and modify parameter shape (weighted) with Maximum Likelihood method. */
  double evalshape(const unsigned int ndata, const Data * data, const Data * x);
  /** No descriptions */
   GenGauss(const Gauss&);
  /** No descriptions */
   GenGauss(const NoiseModel & );
   /** getParam : get one parameter of this noise
    * @param _p : number corresponding to one parameter 
    */
   const vector<double> getParam(const unsigned short _p);

protected: // Protected attributes
  /** Mean parameter of a Gaussian noise */
  vector<double> mean;
  /** Standard deviation parameter of a Gaussian noise */
  vector<double> stddev;
  /** Shape parameter */
  vector<double> shape;
};

#endif
