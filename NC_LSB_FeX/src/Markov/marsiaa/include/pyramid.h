/***************************************************************************
                          pyramid.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef PYRAMID_H
#define PYRAMID_H

#include "DefineMarsiaa.h"
#include "image.h"
#include "data.h"


template <typename TType>
class Image;

/**Data stored as a pyramid (cube with multi-resolution)
  *@author Ana�s OBERTO
  */

#define base data[0]
  
template <typename TType>
class Pyramid : public Data {

protected: // Protected attributes
  /** vector of images with different size */
  vector<Image<TType>*> data;
  /** Number of scales used (if the Pyramid object is not complete, Nscale is not the total scales allocated */
  unsigned char Nscale;
  /** Image Base of the Pyramid */
  //Image<TType>* base;

  
public: 
  Pyramid(){
		//base=NULL;
		Nscale=0;
  }
  
  template<typename TCopie>
  Pyramid(Image<TCopie>* _copie) : Nscale(0){
//		cout << "-------Constructeur Pyramid par copie Image\n";
		data.push_back(new Image<TType>(_copie));
		unsigned int s = static_cast<unsigned int>(log(1.*_copie->getwidth())/log(2.)+0.5) +1;
		setNscale(s);
  }
  
  Pyramid(const unsigned int _w, const unsigned int _h) : Nscale(0){
//		cout << "-------Constructeur Pyramid par copie taille Image\n";
		data.push_back(new Image<TType>(_w,_h));
		unsigned int s = static_cast<unsigned int>(log(1.*_w)/log(2.)+0.5) +1;
		setNscale(s);
  }
  
  Pyramid(const unsigned int _w, const unsigned int _h, const unsigned char _scale) : Nscale(0){
//		cout << "-------Constructeur Pyramid par copie taille Image\n";
		data.push_back(new Image<TType>(_w,_h));
		setNscale(_scale);
  }
 
  template<typename TCopie>
  Pyramid(Image < TCopie > * _copie, const unsigned char _scale) : Nscale(0)	{
//		cout << "-------Constructeur Pyramid par copie Image + scale = "<< scale << endl;
		 data.push_back(new Image<TType>(_copie));
		 setNscale(_scale);
  }
  
  template<typename TCopie>
  Pyramid(const Pyramid<TCopie>* _copie) : Nscale(0)	{
//		cout << "-------Constructeur Pyramid par copie\n";
		Nscale=_copie->getNscale();
		for (unsigned int i=0 ; i<Nscale ; i++)
			data.push_back(new Image<TType>(_copie->getdata(i)));
  }
 	
  Pyramid(Data *_copie) : Nscale(0)	{
//		cout << "-------Constructeur Pyramid par copie pointeur de Data\n";
		if (_copie->isImage()){
			data.push_back(new Image<TType>(_copie));
			unsigned char s =
				static_cast<unsigned char>(
					log(1.*_copie->getwidth())
					/ log(2.)
					+ 0.5
				) +1;
			setNscale(s);
		}
		else
		if (_copie->isPyramid()){
			this->Nscale=_copie->getNscale();
			for(unsigned char i=0 ; i<Nscale ; i++)
				this->data.push_back(new Image<TType>(_copie->getdata(i)));
		}
	}

  ~Pyramid(){
//		cout << "-------Destructeur Pyramid \n";
		if (!data.empty()){
			for (unsigned char i=0 ; i<data.size() ; i++)
				delete data[i];
			data.clear();
		}

  }
  
  /** Write property of unsigned int Nscale. */
  virtual void setNscale( const unsigned char _newVal){
		// il faut une image de base avant 
		// + calculer les autres tailles

		if (base==NULL)
			ERR("Error : You should give Base Image before Nscale of Pyramid\n");
		if (Nscale>1)
		{
			for (unsigned char i=1 ; i<Nscale ; i++)
				delete data[i];
				//{ERR("Error : You have allready a scale for you're Pyramid object\n");}
		}	
	
		Nscale = _newVal;
	
		for (unsigned char i=1 ; i<Nscale ; i++)
			data.push_back(new Image<TType>(data[i-1]->getwidth()/2,data[i-1]->getheight()/2));
  }

  /** Evaluate size of x : 4^x=base->length */
  virtual const unsigned int getBaseScale() const{
    int power = 0;
    bool finished = ((int)pow(4.,power) >= (int) base->length());

    // Finding the power of 4
    while (!finished){
      power++;
      finished = ((int)pow(4.,power) >= (int) base->length());
    }

    return power;
  }
  
  /** Read property of long length. */
  const unsigned int length() const{
		long l=0;
		for (unsigned char i=0 ; i<Nscale ; i++)
			l+=data[i]->length();
		return l;
	}
	
  /** Read/Write property of first element of vector<Image*> data. */
	Image<TType>* getbase() const {	
  	return data[0];
	}
	void setbase( Image<TType>* _newVal){
		base = _newVal;
  }
  /** Read property of unsigned int Nscale. */
	const unsigned char getNscale() const {
		return Nscale;
  }
  
  /** operator[] */
	TType & operator[](const unsigned int _pos){
			return (get_pixel(_pos));
	}
	
	/* Same as get_pixel or [], but VIRTUAL !!! */	
	double getValue(const unsigned int _pos) const {
		return (static_cast<double>(get_pixel(_pos)));
	}
  void setDouble(const unsigned int _pos, const double _newVal){
		set_pixel(_pos,static_cast<TType>(_newVal));
  }

  /** Write/Read property of data as an array. */
  TType & get_pixel(const unsigned int _i)const{
		unsigned int s=_i;
		unsigned int e=0;
		if (_i>=length())
			ERR("Error get_pixel : read pixel " << _i << " out of Pyramid object(size=" << length() << ").\n");
		
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		return (*data[e])[s];
  }
  void set_pixel(const unsigned int _i, const TType _newVal){
		unsigned int s=_i;
		unsigned int e=0;
		if (_i>=length())
			{ERR("Error : write pixel " << _i << " out of object(size=" << length() << ").\n");}
	
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		(*data[e])[s]=_newVal;
  }
  
  
  TType & get_pixel(const unsigned int _scale, const unsigned int _i){
		return (*data[_scale])[_i];
  }
  
  TType & get_father(const unsigned int _pos)const{
		unsigned int s=_pos;
		unsigned int e=0;
		if (_pos>=length())
			ERR("Error get_father : read pixel " << _pos << " out of Pyramid object(size=" << length() << ").\n");
		
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		unsigned int i = s/data[e]->getwidth();
		unsigned int j = s-i*data[e]->getwidth();
		return data[e+1]->get_pixel(i/2,j/2);
  }
  /** get father of the actual position _pos in the actual scale _e */
  TType & get_father(const unsigned int _e,const unsigned int _pos)const{
		unsigned int i = _pos/data[_e]->getwidth();
		unsigned int j = _pos-i*data[_e]->getwidth();
		return data[_e+1]->get_pixel(i/2,j/2);
  }
  
  /** get father or grand father or grand grand father ...
  @param _e : scale of the ancestor 
  @param _epos : actual scale of the given param _pos (<=_e)
  @param _pos : position in the last generation (full resolution where scale=0)
  @remarks : get_ancestor(e+1,e,pos) = get_father(e,pos)
  */
  TType & get_ancestor(const unsigned int _e, const unsigned int _epos, const unsigned int _pos){
		if (_e<_epos)
			ERR("Pyramid::get_ancestor : ancestor = "<<_e<<" is younger than "<<_epos<<"\n");
			
		unsigned int i = _pos/data[_epos]->getwidth();
		unsigned int j = _pos-i*data[_epos]->getwidth();
//		for (unsigned char e=_epos ; e<_e ; e++)
//		{
//			
//		}
		i/=(pow(2,(_e-_epos)));
		j/=(pow(2,(_e-_epos)));
		return data[_e]->get_pixel(i,j);
	}
  
	/* get the first child as :
		2	4
		1	3
	*/
  TType & get_child1(const unsigned int _pos)const{
		unsigned int s=_pos;
		unsigned int e=0;
		if (_pos>=length())
			ERR("Error get_child1 : read pixel out of Pyramid object(size=" << length() << ").\n");
		
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		unsigned int i = s/data[e]->getwidth();
		unsigned int j = s-i*data[e]->getwidth();
		return data[e-1]->get_pixel(i*2,j*2);
  }
  TType & get_child1(const unsigned int _e, const unsigned int _pos)const{
		if (_e<1)
			ERR("Pyramid::get_child1 could not get child of the scale 0\n");
		
		unsigned int i = _pos/data[_e]->getwidth();
		unsigned int j = _pos-i*data[_e]->getwidth();
		return data[_e-1]->get_pixel(i*2 , j*2);
  }
		
	/* get the second child as :
		2	4
		1	3
	*/
  TType & get_child2(const unsigned int _pos)const{
		unsigned int s=_pos;
		unsigned int e=0;
		if (_pos>=length())
			ERR("Error get_child2 : read pixel out of Pyramid object(size=" << length() << ").\n");
		
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		unsigned int i = s/data[e]->getwidth();
		unsigned int j = s-i*data[e]->getwidth();
		return data[e-1]->get_pixel(i*2 +1 , j*2 );
  }
  TType & get_child2(const unsigned int _e, const unsigned int _pos)const{
		if (_e<1)
			ERR("Pyramid::get_child1 could not get child of the scale 0\n");
		
		unsigned int i = _pos/data[_e]->getwidth();
		unsigned int j = _pos-i*data[_e]->getwidth();
		return data[_e-1]->get_pixel(i*2 +1 , j*2);
  }
		
	/* get the third child as :
		2	4
		1	3
	*/
  TType & get_child3(const unsigned int _pos)const{
		unsigned int s=_pos;
		unsigned int e=0;
		if (_pos>=length())
			ERR("Error get_child3 : read pixel out of Pyramid object(size=" << length() << ").\n");
		
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		unsigned int i = s/data[e]->getwidth();
		unsigned int j = s-i*data[e]->getwidth();
		return data[e-1]->get_pixel(i*2 , j*2 +1);
  }
  TType & get_child3(const unsigned int _e, const unsigned int _pos)const{
		if (_e<1)
			ERR("Pyramid::get_child1 could not get child of the scale 0\n");
		unsigned int i = _pos/data[_e]->getwidth();
		unsigned int j = _pos-i*data[_e]->getwidth();
		return data[_e-1]->get_pixel(i*2 , j*2 +1 );
  }
		
	/* get the fourth child as :
		2	4
		1	3
	*/
  TType & get_child4(const unsigned int _pos)const{
		unsigned int s=_pos;
		unsigned int e=0;
		if (_pos>=length())
			ERR("Error get_child4 : read pixel out of Pyramid object(size=" << length() << ").\n");
		
		while (s>=data[e]->length())
		{
			s-=data[e]->length();
			e++;
		}
		unsigned int i = s/data[e]->getwidth();
		unsigned int j = s-i*data[e]->getwidth();
		return data[e-1]->get_pixel(i*2 +1 , j*2 +1);
  }
  TType & get_child4(const unsigned int _e, const unsigned int _pos)const{
		if (_e<1)
			ERR("Pyramid::get_child1 could not get child of the scale 0\n");
		unsigned int i = _pos/data[_e]->getwidth();
		unsigned int j = _pos-i*data[_e]->getwidth();
		return data[_e-1]->get_pixel(i*2 +1 , j*2 +1);
  }
		
  
  /** operator *  */
  Pyramid<TType> operator * (const Pyramid<TType>& _op){
		Pyramid<TType> mult(this);
		for (unsigned int i=0 ; i<Nscale ; i++)
			mult.setdata(i,this->data[i]*_op->getdata(i));
		return mult;
  }
  
  /** operator *  */
  Data* operator * (Data* _op){
		if (dynamic_cast<Pyramid<TType>*>(_op))
			return(dynamic_cast<Data*>(this->operator*(dynamic_cast<Pyramid<TType>*>(_op)))); 
		else
			ERR("Pyramid<T>::operator* : could not be done with object != Pyramid<T>\n"); 
  }
  
  /** No descriptions */
  void load(const string &/*, const Image<unsigned int> * const = NULL*/){}
  
  /** Verify if object is really a Pyramid object. */
  bool isPyramid() const {
		return true;
  }
  /** return a copy of object */
  Data * copy() const{
		return(new Pyramid<TType>(this));
  }
  
  void update_max(){
    for (unsigned int i = 0 ; i < Nscale ; i++)
      data[i]->searchmax();
  }

  void update_min(){
    for (unsigned int i = 0 ; i < Nscale ; i++)
      data[i]->searchmin();
  }

  /** Read property of int height of the base of pyramid. */
  const int getheight() const {
		return base->getheight();
  }
  /** Read property of int width of the base of pyramid. */
  const int getwidth() const {
		return base->getwidth();
  }

  /** Read property of unsigned int old_width of the base of pyramid. */
  unsigned int get_old_width() const{
    return base->get_old_width();
  }

  /** Read property of unsigned int old_height of the base of pyramid. */
  unsigned int get_old_height() const{
    return base->get_old_height();
  }

  /** Read property of TType shift of the base of pyramid . */
  double get_data_shift() const{
    return base->get_data_shift();
  }
  /** Read property of TType scale of the base of pyramid . */
  double get_data_scale() const{
    return base->get_data_scale();
  }
  bool is_shifted() const{
    return base->is_shifted();
  }
  bool is_scaled() const{
    return base->is_scaled();
  }

  /** get data[i] */
  Data* getdata(const unsigned int& _i) const {
		return this->data[_i];
  }
  /** set data[i]=Image */
  void setdata(const unsigned int& _i, const Image<TType>& _im){
		if (data.size()>_i){
			delete data[_i];
			data[_i]=new Image<TType>(_im);
		}
		else 
		if (data.size==_i){
			data.push_back(new Image<TType>(_im));
		}
		else
		ERR("Pyramid::setdata : position in scales is not correct\n");
  }
  
  /** No descriptions */
  void set_pixel(const unsigned int _scale, const unsigned int _i, const TType & _newVal){
    (*data[_scale])[_i]=_newVal;
    
  }
  
  /** Save the Pyramid object in a file. */
  virtual void save(const string _filename){
    filename=_filename;		
    IO io;

    io.exportFits(this,(const string)(_filename));    
  }
  
/** Show with an external application the Pyramid. */
  virtual void show(const string _app){
		stringstream file(""),cmd1(""),cmd2("");
		if ( filename.empty() )
		{
			file << this;
			save(file.str());
			cmd1 << _app << " " << file.str() << " & \n";
//			cmd2 << "YOU HAVE TO REMOVE TMPFILE " << this << endl;
//			cout << cmd2.str() << endl;
			//system(cmd2.str().c_str());
		}
		else
			cmd1 << _app << " " << filename << " & \n";
		
//		cout << cmd1.str() << endl;
		if ( system(cmd1.str().c_str()) == -1)
			cerr << "probleme d'affichage\n";

	}
  /** check if DynamicRange is sufficient */
  virtual void checkDynamicRange(){
		// pour chaque echelle de l'image, verifie la dynamique
		for (unsigned short e=0 ; e<Nscale ; e++)
			data[e]->checkDynamicRange();
	}
  /** Read property of max. */
  virtual const double getmax(const unsigned int & x=0) {
		for (unsigned short e=0 ; e<Nscale ; e++)
			data[e]->searchmax(e);
		return data[x]->getmax();
	}
  /** Read property of max. */
  virtual const double getmax(const unsigned int & x=0) const{
		return data[x]->getmax();
	}
  /** Read property of min. */
  virtual const double getmin(const unsigned int & x=0) {
		for (unsigned short e=0 ; e<Nscale ; e++)
			data[e]->searchmin(e);
		return data[x]->getmin();
	}
  /** Read property of min. */
  virtual const double getmin(const unsigned int & x=0) const{
		return data[x]->getmin();
	}

};


#endif

