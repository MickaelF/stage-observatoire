/***************************************************************************
                          semquadtree.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef SEMQUADTREE_H
#define SEMQUADTREE_H

#include "DefineMarsiaa.h"

#include <quadtree.h>
class Data;
class NoiseModel;

/**
  *@author Ana�s OBERTO
  */

class SEMQuadTree : public QuadTree  {
public: 
	SEMQuadTree();
	~SEMQuadTree();
  /** No descriptions */
  void update_apriori(vector < Data * > & , vector < SegmClass * > & );
};

#endif
