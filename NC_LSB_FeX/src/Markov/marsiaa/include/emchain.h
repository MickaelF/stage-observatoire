/***************************************************************************
                          emchain.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef EMCHAIN_H
#define EMCHAIN_H

#include "DefineMarsiaa.h"

#include "chain.h"
class NoiseModel;
class Data;
class Segmclass;
template <typename TType> class Image;

/**
  *@author Ana�s OBERTO
  */

class EMChain : public Chain  {
public: 
	EMChain();
//	EMChain(NoiseModel);
	~EMChain();
  /** No descriptions */
//  void visit(vector<Data *> &, vector<SegmClass *> &);
  /** Update Data-driven parameters */
//  void update_dataDriven(vector < Data * > & , vector < SegmClass * > & );
  /** No descriptions */
  void update_apriori(vector < Data * > & , vector < SegmClass * > & );
};

#endif
