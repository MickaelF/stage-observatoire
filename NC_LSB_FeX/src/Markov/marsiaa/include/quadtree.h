/***************************************************************************
                          quadtree.h  -  description
                             -------------------
    begin                : lun ao� 19 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef QUADTREE_H
#define QUADTREE_H

#include "DefineMarsiaa.h"

#include <markovmodel.h>
class Data;
template <typename TType> class Pyramid;

/**
  *@author Ana�s OBERTO
  */

class QuadTree : public MarkovModel  {
public: 
	QuadTree();
	virtual ~QuadTree();
  /** Update "a priori" parameters. */
  void update_apriori();
  /** Initialize and allocate memory for private parameters */
  virtual void init(const vector < Data * > & data, const vector < SegmClass * > & classes);
  /** update_dataDriven @param decorr true if method to decorrelate is asked */
  void update_dataDriven(const vector < Data * > & data, vector < SegmClass * > & classes);
  /** return marginal=P_forward*P_backward for one position */
//  double getP_marginal_aposteriori(const unsigned int, const unsigned int)const;
  /** browse forward and backward */
  void browse(const vector < Data * > & , const vector < SegmClass * > & ) ;


  /** evaluate joint probability for a quadtree */
  void browse_backward(const vector < SegmClass * > &);
  /** Browse minimum to initialise an a posteriori probability */
  void init_aposteriori(const vector < Data * > & , const vector < SegmClass * > & );
  /** return a marginal Data object (Image) */
  virtual Data * getP_marginal_aposteriori(const unsigned short k);
  /** browse forward and backward using external likelihood */
  virtual void browse(const vector < Data * > & );


protected: // Protected methods
  /** evaluate forward probability for a quadtree : evaluate likelihood in full resolution
  	and propagate the probability along the quadtree going up */
  void browse_forward(const vector < Data * > & , const vector < SegmClass * > &);
  /** get a priori probability
  * @param _k number of the class to evaluate probability
  * @param _scale scale of the apriori
  * @remarks :
	*		the apriori probability is the same at each pixel of one scale because of Quadtree.
	*		the last scale (where there is only 1 pixel) is initialiszed with Pk
  */
  double getP_apriori(const unsigned int& _k , const unsigned int& _scale) const ;
  /** 
   getP_joint_aposteriori :get joint probability a posteriori
   *
   * @param	classes			: vector of classes of segmentation
   * @param		_k, _l			: couple of classes to evaluate probability
   * @param		_e, _pos		: position in the quadtree (scale, and position in the image in this scale)
   * @param		_fatherMarginal : marginal probability aposteriori of the father (scale+1)
   *
   * @return joint probability a posteriori for this position
   *
   *
   */
   double getP_joint_aposteriori(const vector<SegmClass*>&classes, const unsigned int _k, 
   const unsigned int _l, const unsigned int _e, const unsigned int _pos, const double _fatherMarginal) const;
  /** Evaluate Inverse Transition */
  double getInv(const vector<SegmClass*>&,const unsigned int &, const unsigned int &,const unsigned int &) const;
  /** Number of scale to browse */
  unsigned int Nscale;
//  vector<Pyramid<double>*>  P_backward;
	/** P(Xs=wi/Y>s) evaluated in browse forward */
	vector<Pyramid<double>*>  P_forward;
	/** a priori probability P(Xs=k) (same for the whole scale */
	dMatrix P_apriori;
	/** P(Xs=wi/Y) evaluated in browse backwrad */
//	vector<Pyramid<double>*>  P_marginal;
	/** P(Xs=wl,Xs-=wk/Y) evaluated in browse backwrad */
	//vector<vector<Pyramid<double>*> >  P_joint;
	/** get a vector of a priori probability */
//  vector<double> getP_apriori(const vector < SegmClass * > &, const unsigned int &)const;
  /** No descriptions */
  Pyramid<unsigned char> * simulX(vector < SegmClass * > & );
  /** Read property of the marginal a posteriori : P(Xs=wk/Y) = Sumj P_joint
	 * @param	_k : class of the probability
	 * @param	_scale : scale in the Pyramid
	 * @param	_pos : position in the Image at the scale _scale
	 *
	 * @return
	 *	double value of the marginal a posteriori probability
	 *
	 * @remarks
	 * 		used by EMQuadTree, ICEQuadTree and segmentation 
   */
  virtual double getP_marginal_aposteriori(const unsigned int _k, const unsigned int _pos, const unsigned int _scale=0)const;
  /** marginal a posteriori probability */
  vector<Pyramid<double>*> P_marginal;
};

#endif
