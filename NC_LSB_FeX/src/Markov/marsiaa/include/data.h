/***************************************************************************
                          data.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef DATA_H
#define DATA_H

#include "DefineMarsiaa.h"
#include "math.h"

template <typename TType> class Image;
//template <typename TType> class Pyramid;

/**Different ways to save and use data.
  *@author Ana�s OBERTO
  */

class Data {
public: 
	Data();
	virtual ~Data();
  /** No descriptions */
  virtual void load(const string &/*, const Image<unsigned int> * const = NULL*/)=0;
  /** Read property of long length. */
  virtual const unsigned int length() const=0;
  /** Verify if object is really an Image<double> object. */
  bool isImageDouble();
  /** Verify if object is really an Image<double> object. */
  virtual bool isImage() const {
		return false;}
  /** operator* */
  virtual Data * operator*(Data*) /*{cout << "Data& operator* : PAS POSSIBLE !!!\n";}*/=0;
  /** Verify if object is really a Pyramid object. */
  virtual bool isPyramid(void) const {
		return false;}
  /** give a double value corresponding to given position in data */
	virtual double getValue(const unsigned int) const =0;
  /** No descriptions */
  template <typename T> 
  	void setValue(const unsigned int _pos, const T _newVal) {
			this->setDouble(_pos,static_cast<const double>(_newVal));
   }
  	
	virtual void setDouble(const unsigned int, const double) = 0;
  /** return a copy of object */
  virtual Data* copy(void) const = 0;
  /** Read property of int height. */
  virtual const int getheight() const  = 0;
  /** Read property of int width. */
  virtual const int getwidth() const = 0;
  /** Read property of unsigned int old_width. */
  virtual unsigned int get_old_width() const = 0;
  /** Read property of unsigned int old_height. */
  virtual unsigned int get_old_height() const = 0;
  /** Read property of TType shift. */
  virtual double get_data_shift() const = 0;
  /** Read property of TType scale. */
  virtual double get_data_scale() const = 0;
  virtual bool is_shifted() const = 0;
  virtual bool is_scaled() const = 0;

  /** Read property of unsigned int Nscale. */
  virtual const unsigned char getNscale() const;
  /** Evaluate size of x : 4^x=length */
  virtual const unsigned int getBaseScale() const = 0;
  /** Give position of the previous site along the markovian model */
//  unsigned int& prev(unsigned int&) const;
  /** Give position of the next site along the markovian model */
//  unsigned int & next(unsigned int & ) const;
  /** get data[i] */
  virtual Data* getdata(const unsigned int &) const =0;
  /** Show with an external application the Image. */
  virtual void show(const string _app="xv") = 0;
  /** Save the Data object in a file. */
  virtual void save(const string _filename) = 0;
  /** check if DynamicRange is sufficient */
  virtual void checkDynamicRange() = 0;
  /** update max values */
  virtual void update_max() = 0;
  /** update min values */
  virtual void update_min()  = 0;
  /** Read property of max. */
  virtual const double getmax(const unsigned int & x=0)  = 0;
  /** Read property of min. */
  virtual const double getmin(const unsigned int & x=0)  = 0;
  /** Read property of max. */
  virtual const double getmax(const unsigned int & x=0) const = 0;
  /** Read property of min. */
  virtual const double getmin(const unsigned int & x=0) const = 0;
/*  {
		if(this->isImage())
		{
 			if (i>1)
 				ERR("Data::getdata : could not get data of an Image > 1\n");
 			else
 				return this;
		}
			ERR("Data::getdata should not be called, return this\n");
  }
*/
public: // Public attributes
  /** For saved or initial filename. */
  string filename;

  /** Pointer on the Mask of all Images */
  static Image<bool> *mask;

};

#endif


