/***************************************************************************
                          semchain.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef SEMCHAIN_H
#define SEMCHAIN_H

#include "DefineMarsiaa.h"

#include <chain.h>
class Data;
class NoiseModel;

/**
  *@author Ana�s OBERTO
  */

class SEMChain : public Chain  {
public: 
	SEMChain();
	~SEMChain();
  /** No descriptions */
//  void visit(vector<Data *> &, vector<SegmClass *> &);
  /** Update Data-driven parameters */
//  void update_dataDriven(vector < Data * > & , vector < SegmClass * > & );
  /** No descriptions */
  void update_apriori(vector < Data * > & , vector < SegmClass * > & );
};

#endif
