
/***************************************************************************
                          - DefineMarsiaa.h  -  
                          
		all "#define" and functions necessary for all project Marsiaa
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/

#ifndef _DEFINEMARSIAA_H_
#define _DEFINEMARSIAA_H_

using namespace std;

#include <vector>
#include <string>
#include <iostream>
#include <math.h>


const double PI = 3.1415926535;
//const double SEUIL_ECA = 0.5;
const unsigned short N_STOCHASTIC = 1;
const double  eps = 1.e-15;
const unsigned short N_IT_TEST_STOP = 3;
const double OUT_OF_RANGE = 1.e20 ;
const float DYNAMIC_RANGE = 100;

/* DEFINE CONSTANTS */
const signed short CONDI = -1;
const signed short KMEAN = 0;
const signed short FUZZY_KMEAN = 1;
const signed short SKMEAN = 2;

const signed short RANDOM = 0;
const signed short EQUIPROBA = 1;

const signed short EM = 0;
const signed short SEM = 1;
const signed short ICE = 2;

const signed short CHAIN = 0;
const signed short QUADTREE = 1;
const signed short FIELD = 2;

const signed short MAX_NB_CLASS = 32;
const signed short MIN_NB_CLASS = 2;
// CLASS if not a number, must be < MIN_NB_CLASS
const signed short AUTO_NB_CLASS = -1;
const signed short FUZZY = -2;

const signed short GAUSS = 0;
const signed short GENGAUSS = 1;
const signed short WEIBULL = 2;
const signed short POISSON = 3;
const signed short LOGNORMAL = 4;
const signed short LOGG = 5; // LOGNORMAL + GAUSS(s)
const signed short AUTO_NOISE = -1;

// must be 0 to be equivanlent to false 
const signed short NODECORR = 0;
const signed short CHOLESKY = 1;
const signed short ICA = 2;

const signed short PC_CHANGE = 0;
const signed short PC_DIST = 1;

// MUST NO BE A MULTIPLE OF NUMBER OF ITERATION
const unsigned int NODISPLAY = 999;

//#define DISP_MAP 1
//#define DISP_DECORR 2

/* DEFINE FOR DEFAULT TAGS */
//#define NTAGS 6

const string DEFAULT_INIT = "KMEAN";

#define DEFAULT_INITP "EQUIPROBA"

#define DEFAULT_NB_CLASS "4"

#define DEFAULT_ESTIM "SEM"

#define DEFAULT_MARKOV "QUADTREE"

#define DEFAULT_NOISE "GAUSS"

#define DEFAULT_TRACE "NO"

#define DEFAULT_MAP_SUFFIX "YES"

#define DEFAULT_MAPMS "NO"

//#define DEFAULT_DISPLAY

#define DEFAULT_DISPLAY_APP ""

#define DEFAULT_DECORR "NO"

#define DEFAULT_IT_MAX 20

#define DEFAULT_STOP "PC_CHANGE"
#define DEFAULT_PC_STOP 3

/* OTHERS */

#define MAX_LINE_BUFFER 255
// gamma function using standard UNIX lgamma function
// is only defined for positive values
#define Gamma(x) exp(lgamma(x))

//#define NclassDEF(x) (x<2)?false:(x>32)?false:true

typedef std::vector<std::vector<double> > dMatrix;

/* Fonction d'erreur qui permet de sortir proprement */
#define ERR(msg) { try { cerr << msg; throw(-1); }	 catch(...) { throw; } };

#ifdef MAIN
	double pow(int x, int y){
		return pow(static_cast<float>(x),static_cast<float>(y));
	}
	double pow(double x, int y){
		return pow(x,static_cast<double>(y));
	}
	unsigned int pow(int x, unsigned int y){
		return static_cast<unsigned int>(pow(static_cast<float>(x),static_cast<float>(y)));
	}
	unsigned int pow(unsigned int x, unsigned int y){
		return static_cast<unsigned int>(pow(static_cast<float>(x),static_cast<float>(y)));
	}

#else
	extern double pow(int, int);
	extern double pow(double, int);
	extern unsigned int pow(int , unsigned int );
	extern unsigned int pow(unsigned int, unsigned int);
#endif


#endif
// end _DEFINEMARSIAA_H_

