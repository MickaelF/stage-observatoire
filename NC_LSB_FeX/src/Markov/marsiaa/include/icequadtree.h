/***************************************************************************
                          icequadtree.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef ICEQUADTREE_H
#define ICEQUADTREE_H

#include "DefineMarsiaa.h"

#include <quadtree.h>
class Data;
class NoiseModel;

/**
  *@author Ana�s OBERTO
  */

class ICEQuadTree : public QuadTree  {
public: 
	ICEQuadTree();
	~ICEQuadTree();
  /** Update "a priori" parameters. */
  void update_apriori(vector < Data * > &, vector < SegmClass * > &);
};

#endif
