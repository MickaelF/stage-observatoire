/***************************************************************************
                          noisemodel.h  -  description
                             -------------------
    begin                : mar ao� 20 2002
    email                : oberto@cacao.u-strasbg.fr
 ***************************************************************************/


#ifndef NOISEMODEL_H
#define NOISEMODEL_H

#include "DefineMarsiaa.h"

class Data;
class SegmClass;


/**
  *@author oberto
  */

class NoiseModel {
public: 
	NoiseModel();
	virtual ~NoiseModel();
	NoiseModel(const NoiseModel&);

	virtual NoiseModel& operator=(const NoiseModel&);

  /** What specific object is it ? */
  virtual bool isGenGauss() const { return false; };
  virtual bool isWeibull() const { return false; };
  virtual bool isGauss() const { return false; };
  virtual bool isLogNormal() const { return false; };

  /** Evaluate statistical likelihood for pixel at position in a array */
  double likelihood(const vector<Data*>&, const int);
  
  /** Evaluate statistical likelihood */
  virtual double likelihood(const vector<double> & )=0;
  /** Evaluate statistical likelihood */
  virtual double likelihood(const double * _values);
  /** Write property of int size. */
  static void setNdata( const unsigned int _newVal);
  /** Read property of int size. */
  static const unsigned int getNdata();
  /** Update data driven */
  //virtual void update_dataDriven(const vector<Image<double>*>&,const Image<double>*&){};
  virtual void update_dataDriven(const vector<Data*>&,const Data*)=0;
  /** Pointer on function use to decorrelate data */
  void (NoiseModel::*ptf_decorr) ();
  /* Use decorrelation choosed */
  void decorrelate();
  /** Decorrelate with cholesky algorithm */
  void cholesky();
  /** Independant Component Analysis for decorrelation */
  void ica();

  /** get one varcov value */
  const double getvarcov(const unsigned int line , const unsigned int col) const;
  /** get matrix of varcov */
  dMatrix& getvarcov();
	/** Read property of vector<double> corr. */
	const double getcorr(const unsigned int line, const unsigned int col) const;
	/** Read property of the determinant */
  double getdet() const;

  /** set corr */
  void setcorr(const dMatrix& c);
  /** set corr */
  void setcorr(const unsigned int line , const unsigned int col, const double _newVal);
  /** set varcov */
  void setvarcov(const unsigned int line, const unsigned int col, const double _newVal);
  /** set det */
  void setdet(const double& );
  /** Update correlations */
  void evalcorr(const vector<Data*>& data, const Data * w);
  /** Update correlations */
  void update_corr(const vector < Data * > & , const Data * );
  /** Initialise correlations */
  void init_corr();
  /** get m1 */
  const vector<double>& getm1()const;
  /** get m2 */
  const vector<double>& getm2()const;
  /** get one of the m1 vector
	* @param i : number of the image you want the first order moment
	* @return the 'i'eme first order moment
 	*/
  const double & getm1(unsigned int i) const;
  /** get one of the m1 vector
	* @param i : number of the image you want the second order moment
	* @return the 'i'eme second order moment
	*/
  const double & getm2(unsigned int i)const;
  
  /** getParam : get one parameter of this noise
	* @param _p : number corresponding to one parameter 
	*/
  virtual const vector<double> getParam(const unsigned short _p)=0;

    /** Variance, covariance matrix. */
  dMatrix varcov;
  dMatrix varcovInv;
  /** Correlation between 2 Images */
  dMatrix corr;
  /** Decorrelation matrix */
  dMatrix decorr;
  /** First order moment */
  vector<double> m1;
  /** Second order moment */
  vector<double> m2;
  /** Third order Moment */
  vector<double> m3;
  /** Forth order Moment */
  vector<double> m4;

  /** Evaluate and modify parameter varcov with Maximum Likelihood method. */
  void evalvarcov();
	/** Evaluate and modify Inverse matrix varcov */
	void evalvarcovInv();
  /** Evaluate correlation */
  double evalcorr(const unsigned int ndata1, const Data* data1, const unsigned int ndata2, const
	Data* data2, const Data*x);

protected: 
  /** Size of each noise parameter objects. */
  static unsigned int Ndata;
  /** Determinant of the varcov matrix. */
  double det;

  /** Evaluate First order moment */
  double M1(const Data*, const Data*);
  /** Evaluate Second order moment */
  double M2(const Data * , const Data *, const double );
};

#endif
