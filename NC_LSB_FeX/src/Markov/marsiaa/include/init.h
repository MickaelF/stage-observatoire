/***************************************************************************
                          init.h  -  description
                             -------------------
    begin                : ven ao� 16 2002
    email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/


#ifndef INIT_H
#define INIT_H

#include "DefineMarsiaa.h"

class SegmClass;
class NoiseModel;
class Data;
template <typename TType> class Image;


/**Different ways ti initialize.

  *@author Ana�s OBERTO
  */

class Init {
public: 
	Init();
	virtual ~Init();
  /** No descriptions */
  static void condi(vector<Data*> _data, vector<SegmClass*> classes);
  /** No descriptions */
  static void preset1(vector<Data*>,vector<SegmClass *>);
  /** Use of the k-mean method. */
  static void kmean(vector<Data*> _data,vector<SegmClass*>classes, const int a=4, const int b=4);
  /** Evaluate if the selected class fits more of each known NoiseModel.  */
  static NoiseModel* bestNoiseModel(vector<SegmClass*>);
  /** Initialize 'a priori' parameters (P_k, a_ij) */
  static void apriori(vector<SegmClass *> classes);
  /** Use of the k-mean method and estimate number of classes */
  static void FuzzyKmean(vector<Data*> , vector<SegmClass*>, const int a, const int b);
  /** Evaluate best number of classes for images 
  * @param	images		: images to study 
	*
	* @return 	number of classes evaluated
	*/
  static int bestNclass(vector<Data *>);
  /** HilbertPeano Chain scan for images */
  static Image<unsigned int> * HilbertPeano(const string _path, vector<Data*>& data);

private: // Private methods
//	void moyLocal(vector<double>, Image<double> *, const int, const int, const int);
	static double moyLocal(const Image<double>*, const int, const int, const int, const int);
	static double ecartLocal(const Image<double>*,  const double, const int, const int, const int, const int);
	static void createSmall(vector<Image<double>*> data,vector<Image<double>*>
	small_moy,vector<Image<double>*> small_ecart,Image<bool> *mask=NULL);
/** Search which centroide is the nearest of the local parameters */
	static int mindist(const vector<Image<double>*> imagetteM, const vector<Image<double>*> imagetteE,const dMatrix
	centroideM,const dMatrix centroideE, const int l, const int c);
	
};

#endif
