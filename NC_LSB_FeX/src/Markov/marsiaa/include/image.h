/***************************************************************************
 image.h  -  description
 -------------------
 begin                : ven ao� 16 2002
 email                : oberto@astro.u-strasbg.fr
 ***************************************************************************/

#ifndef IMAGE_H
#define IMAGE_H

#include "DefineMarsiaa.h"

#include <sstream>
#include <math.h>
#include <limits>
#include "io.h"

#include "data.h"

/**Data on 2 dimensions.
 *@author Ana�s OBERTO
 */

template<typename TType>
class Image: public Data {

private:
	// Private attributes
	/** Width of the Image (even after mirror effect) */
	unsigned int width;
	/** Height of the Image (even after mirror effect) */
	unsigned int height;
	/** Old width of the Image (before mirror effect (0 otherwise)) */
	unsigned int old_width;
	/** Old height of the Image (before mirror effect (0 otherwise)) */
	unsigned int old_height;

	/** Value for the shift (if data are < 0 within the image) */
	TType data_shift;
	/** Value for the scale (if (max-min) < DYNAMIC_RANGE) */
	TType data_scale;
	/** true if the image has been shifted*/
	bool has_been_shifted;
	/** true if the image has been scaled*/
	bool has_been_scaled;

	unsigned int LENGTH;
	/** Data of the Image */
	TType *data;
	/** Minimum value of data */
	TType min;
	/** Maximum value of data */
	TType max;

public:
	// Public methods
	Image() {
		//		cout << "-------Constructeur Image\n";
		data = NULL;
		width = height = old_width = old_height = 0;
		LENGTH = width * height;
		data_shift = (TType) 0;
		data_scale = (TType) 1;
		has_been_shifted = false;
		has_been_scaled = false;
		// initialisation a une valeur consideree comme pixel crame
		min = numeric_limits<TType>::max(), max = numeric_limits<TType>::min();
	}
	Image(const int w, const int h) {
		//		cout << "-------Constructeur Image "<<w<<" "<<h<<endl;
		this->createData(w, h);
		old_width = old_height = 0;
		data_shift = (TType) 0;
		data_scale = (TType) 1;
		has_been_shifted = false;
		has_been_scaled = false;
		// initialisation a une valeur consideree comme pixel crame
		min = numeric_limits<TType>::max(), max = numeric_limits<TType>::min();
	}

	/** Read property of unsigned int old_width. */
	unsigned int get_old_width() const {
		return old_width;
	}

	/** Read property of unsigned int old_height. */
	unsigned int get_old_height() const {
		return old_height;
	}

	/** Read property of TType shift. */
	double get_data_shift() const {
		return static_cast<double> (data_shift);
	}

	/** Read property of TType scale. */
	double get_data_scale() const {
		return static_cast<double> (data_scale);
	}

	bool is_shifted() const {
		return has_been_shifted;
	}

	bool is_scaled() const {
		return has_been_scaled;
	}

	template<typename TTypeCopie>
	Image(const Image<TTypeCopie> *_copie) {
		//		cout << "-------Constructeur Image par copie\n";
		this->createData(_copie->getwidth(), _copie->getheight());
		this->old_width = _copie->get_old_width();
		this->old_height = _copie->get_old_height();
		this->data_shift = _copie->get_data_shift();
		this->data_scale = _copie->get_data_scale();
		this->has_been_shifted = _copie->is_shifted();
		this->has_been_scaled = _copie->is_scaled();

		//		const unsigned int length = this->width*this->height;
		for (unsigned int i = 0; i < LENGTH; i++)
			this->set_pixel(i, static_cast<TType> (_copie->get_pixel(i)));
	}

	Image(const Data * _copie) {
		this->createData(_copie->getwidth(), _copie->getheight());
		// on ne copie que l'image
		this->old_width = _copie->get_old_width();
		this->old_height = _copie->get_old_height();
		this->data_shift = _copie->get_data_shift();
		this->data_scale = _copie->get_data_scale();
		this->has_been_shifted = _copie->is_shifted();
		this->has_been_scaled = _copie->is_scaled();
		for (unsigned int i = 0; i < LENGTH; i++)
			this->set_pixel(i, static_cast<TType> (_copie->getValue(i)));
	}

	~Image() {
		//		cout << "\n-------Destructeur Image\n";
		if (data != NULL) {
			delete[] this->data;
			data = NULL;
		}
	}

	/** Allocate data */
	void createData(const int w, const int h) {
		this->width = w;
		this->height = h;
		LENGTH = width * height;
		this->data = new TType[w * h];
	}

	/** Write/Read property of data as a matrix. */
	TType& get_pixel(const unsigned int _i, const unsigned int _j) const {
		if (_i >= height || _j >= width) {
			ERR("Error : read pixel out of object(size=" << width << ":" << height << ").\n");
		} else
			return data[_i * width + _j];
	}

	void set_pixel(const unsigned int _i, const unsigned int _j, const TType _newVal) {
		if (_i >= height || _j >= width) {
			ERR("Error : write pixel out of object(size=" << width << ":" << height << ").\n");
		} else {
			this->set_pixel(_i * width + _j, _newVal);
		}
	}

	void update_max() {
		searchmax();
	}

	void update_min() {
		searchmin();
	}

	/** Write/Read property of data as an array. */
	TType& get_pixel(const unsigned int _i) const {
		if (_i >= LENGTH) {
			ERR("Error : read pixel out of object(size=" << LENGTH << ").\n");
		} else
			return (data[_i]);
	}

	void set_pixel(const unsigned int _i, const TType _newVal) {
		if (_i >= LENGTH) {
			ERR("Error : write pixel out of object(size=" << LENGTH << ").\n");
		} else
			data[_i] = _newVal;
	}

	/** Evaluate size of x : 4^x=length */
	virtual const unsigned int getBaseScale() const {
		int power = 0;
		bool finished = ((int) pow(4., power) >= (int) this->length());

		// Finding the power of 4
		while (!finished) {
			power++;
			finished = ((int) pow(4., power) >= (int) this->length());
		}

		return power;
	}

	/** Read property of total number of values (mainly for using Chain) */
	virtual const unsigned int length() const {
		return LENGTH;
	}

	/** Read property of int width. */
	const int getwidth() const {
		return width;
	}

	/** Read property of int height. */
	const int getheight() const {
		return height;
	}

	/** Check is height is a power of 2 */
	bool height_power_of_2() {
		return ((height & (height - 1)) == 0) && (height != 0);
	}

	/** Check is width is a power of 2 */
	bool width_power_of_2() {
		return ((width & (width - 1)) == 0) && (width != 0);
	}

	/** Check if the image is square and its edges are power of two */
	bool check_dimensions() {
		return (height_power_of_2() && width_power_of_2() && width == height);
	}

	/** Compute the power of 2 greater than the width */
	int get_power_width() {
		int power = 0;
		bool finished = ((int) pow(2., power) >= (int) width);

		// Finding the power of 2
		while (!finished) {
			power++;
			finished = ((int) pow(2., power) >= (int) width);
		}

		return (int) pow(2., power);
	}

	/** Compute the power of 2 greater than the height */
	int get_power_height() {
		int power = 0;
		bool finished = ((int) pow(2., power) >= (int) height);

		// Finding the power of 2
		while (!finished) {
			power++;
			finished = ((int) pow(2., power) >= (int) height);
		}

		return (int) pow(2., power);
	}

	/** Mirror effect of the current image */
	Image<TType> * mirror_effect() {
		unsigned int new_size, new_size_width, new_size_height, cpti, cptj;
		unsigned int i, j;
		// Picture after resizing
		Image<TType> * new_image = NULL;

		// First we find the new size (power of 2)
		new_size_width = get_power_width();
		new_size_height = get_power_height();
		new_size = (new_size_width > new_size_height) ? new_size_width : new_size_height;
		// New image
		new_image = new Image<TType> (new_size, new_size);

		// Copy the current image
		for (i = 0; i < height; i++)
			for (j = 0; j < width; j++)
				new_image->set_pixel(i, j, get_pixel(i, j));

		// From width x height to new_size_width x new_size_height
		// eg : from 10x250 to 16x256 (or from 250x250 to 256x256)

		// left part
		for (j = width, cptj = 1; j < new_size_width; j++, cptj++)
			for (i = 0; i < height; i++)
				new_image->set_pixel(i, j, get_pixel(i, width - cptj));

		// bottom part
		for (i = height, cpti = 1; i < new_size_height; i++, cpti++)
			for (j = 0; j < width; j++)
				new_image->set_pixel(i, j, get_pixel(height - cpti, j));

		// left and bottom part
		for (i = height, cpti = 1; i < new_size_height; i++, cpti++)
			for (j = width, cptj = 1; j < new_size_width; j++, cptj++)
				new_image->set_pixel(i, j, get_pixel(height - cpti, width - cptj));

		// Now we duplicate (if needed) the image until we obtain the right size
		// eg : from 16x256 to 256x256 we successively obtain these images (with mirror effect):
		// 32x256 -> 64x256 -> 128x256 -> 256x256

		// left part
		while (new_size_width != new_size) {
			for (j = new_size_width, cptj = 1; j < new_size_width * 2; j++, cptj++)
				for (i = 0; i < new_size_height; i++)
					new_image->set_pixel(i, j, new_image->get_pixel(i, new_size_width - cptj));

			new_size_width = new_size_width * 2;
		}

		// bottom part
		while (new_size_height != new_size) {
			for (i = new_size_height, cpti = 1; i < new_size_height * 2; i++, cpti++)
				for (j = 0; j < new_size_width; j++)
					new_image->set_pixel(i, j, new_image->get_pixel(new_size_height - cpti, j));
			new_size_height = new_size_height * 2;
		}

		//IO::exportFits(new_image,"t.fits",0);

		return new_image;
	}

	/** Open a file on the Image object. (and use a mirror effect if needed) */
	void load(const string & _filename) {
		IO io;
		// Read the picture
		io.importFits(dynamic_cast<Image<double>*> (this), _filename);
		filename = _filename;

		// Check dimensions
		if (!check_dimensions()) {
			// Miror effect
			Image<TType> * temp = mirror_effect();

			// Updating new image data
			delete[] data;
			data = temp->data;
			old_width = width;
			old_height = height;
			width = temp->width;
			height = temp->height;
			LENGTH = temp->LENGTH;
			cout << "Resizing input file " << filename << " from " << old_height << "x" << old_width << " to " << height << "x" << width << "\n";
		}
	}

	/** Save the Image object in a file. (and cut the image if needed (resized image))*/
	void save(const string _filename) {
		filename = _filename;
		IO io;
		unsigned int i, j;

		// Check if the file has been mirrored
		if (old_width != 0 && old_height != 0) {
			// Pointer on the image to save
			Image<TType> * old_temp;

			// if a special scan has been used (like peano)
			if (io.scan != NULL) {
				// new picture
				old_temp = new Image<TType> (this);
				// copy picture according to the scan
				for (i = 0; i < LENGTH; i++)
					old_temp->set_pixel(i, get_pixel(io.scan_get_pixel(i)));
			} else {
				old_temp = this; // no special scan
			}

			// Creating new picture (with the right size (befrore mirror effect))
			Image<TType> * temp = new Image<TType> (old_width, old_height);
			// Copy the current image
			for (i = 0; i < old_height; i++) {
				for (j = 0; j < old_width; j++) {
					temp->set_pixel(i, j, old_temp->get_pixel(i, j));
				}
			}

			// Save
			io.exportFits(temp, (const string) (_filename), 0);
			delete temp;

			// We free the memory only if a special scan has been used (like peano)
			if (io.scan != NULL)
				delete old_temp;
		} else
			// Size is ok
			io.exportFits(this, (const string) (_filename));
	}

	/** Show with an external application the Image. */
	void show(const string _app) {
		if (_app.empty()) {
			cout << "Warning : no external application defined for displaying pictures \n";
			return;
		}

		stringstream file(""), cmd1(""), cmd2("");
		if (filename.empty()) {
			file << this;
			save(file.str());
			filename.clear();
			cmd1 << _app << " " << file.str() << " & \n";
		} else
			cmd1 << _app << " " << filename << " & \n";

		if (system(cmd1.str().c_str()) == -1)
			cerr << "probleme d'affichage\n";

	}
	;

	/** Return pointer on data array. */
	TType * & getdata() {
		return data;
	}

	/* OPERATORS */
	Image<TType> operator*(const Image<TType> & im) {
		Image<TType> mult(this);
		for (unsigned int i = 0; i < LENGTH; i++)
			mult.set_pixel(i, im.get_pixel(i) * this->get_pixel(i));
		return mult;
	}

	TType & operator[](const unsigned int _pos) {
		return (get_pixel(_pos));
	}

	double getValue(const unsigned int _pos) const {
		return (static_cast<double> (get_pixel(_pos)));
	}

	void setDouble(const unsigned int _pos, const double _newVal) {
		set_pixel(_pos, static_cast<TType> (_newVal));
	}

	/** operator* : Image * (Data*) */
	Data * operator*(Data * _op) {
		if (dynamic_cast<Image<TType>*> (_op))
			return (dynamic_cast<Data*> (this->operator*(dynamic_cast<Image<TType>*> (_op))));
else			ERR("Image::operator* : could not be done with object != Image<T>\n");
		}

		/** operator* : Image<T1> * Image<T2> */
		template <typename T2>
		Image<TType> operator*(const Image<T2> &_op) {
			Image<TType> mult(this);
			for (unsigned int i=0; i<LENGTH; i++)
			mult.set_pixel(i,this->get_pixel(i)*static_cast<TType>(_op.get_pixel(i)));
			return mult;
		}

		/** Verify if object is really an Image<double> object. */
		bool isImage() const {
			return true;
		}
		/** return a copy of object */
		Data * copy() const {
			return(new Image(this));
		}

		/** get Pointer on array containing data */
		TType * getPdata() const {
			return this->data;
		}

		/** get data[i] */
		Data * getdata(const unsigned int & i) const {
			if (i>1)
			{	ERR("Data::getdata : could not get data > 1 on an Image \n");}
			else
			return const_cast<Image<TType>*>(this);
		}

		/** Read property of TType min. */
		virtual const double getmin(const unsigned int & x=0) {
			searchmin();
			return static_cast<double>(min);
		}

		/** Read property of TType min. */
		virtual const double getmin(const unsigned int & x=0)const {
			return static_cast<double>(min);
		}

		virtual const double searchmin(const unsigned int & x=0) {
			if (Data::mask==NULL)
			return static_cast<double>(min);
			min=numeric_limits<TType>::max();
			for (unsigned int i=0; i<LENGTH; i++)
			if (Data::mask->get_pixel(i))
			if (data[i]<min)
			min=data[i];
			return static_cast<double>(min);
		}
		/** Write property of TType min. */
		template <typename T2>
		void setmin(const T2 & _newVal) {
			min = static_cast<TType>(_newVal);
		}

		/** Read property of TType max. */
		virtual const double getmax(const unsigned int & x=0) {
			searchmax();
			return static_cast<double>(max);
		}
		/** Read property of max. */
		const double getmax(const unsigned int & x=0) const {
			return static_cast<double>(max);
		}
		virtual const double searchmax(const unsigned int & x=0) {
			if (Data::mask==NULL)
			return static_cast<double>(max);
			max=numeric_limits<TType>::min();
			for (unsigned int i=0; i<LENGTH; i++)
			if (Data::mask->get_pixel(i))
			if (data[i]>max)
			max=data[i];
			return static_cast<double>(max);
		}
		/** Write property of TType max. */
		template <typename T2>
		void setmax(const T2 & _newVal) {
			max = static_cast<TType>(_newVal);
		}
		/** check if DynamicRange is sufficient */
		virtual void checkDynamicRange() {
			searchmin();
			searchmax();

			cout << "Pixel range : min = " << min << "\tmax = " << max << endl;

			// Verifie la dynamique
			if (fabs(static_cast<double>(max)-static_cast<double>(min))<DYNAMIC_RANGE) {
				cerr << "Warning : Pixel range too small (" << (max-min) << ").\nPixel range is automatically scaled to " << DYNAMIC_RANGE << "\n";
				data_scale = ((double)DYNAMIC_RANGE)/((double)(max-min));
				// Multiplie toutes les donnees par le facteur
				for (unsigned int i=0; i<LENGTH; i++)
				set_pixel(i,get_pixel(i)*data_scale);
				setmin(min * data_scale);
				setmax(max * data_scale);
				cout << "Rescaling achieved\n";
				cout << "Pixel range : min = " << min << "\tmax = " << max << endl;
				has_been_scaled = true;
			}

			// V�rification des donn�es n�gatives
			if (min < 0) {
				data_shift = -min;
				setmin(min + data_shift);
				setmax(max + data_shift);
				cout << "Negative values : new pixel range : min = " << min << "\tmax = " << max << endl;
				for (unsigned int i = 0; i < LENGTH; i++)
				set_pixel(i,get_pixel(i) + data_shift);
				has_been_shifted = true;
			}

			// Verifie qu'il n'y ait pas de donnees trop extremes
			if (fabs(static_cast<double>(min))>=OUT_OF_RANGE || fabs(static_cast<double>(max))>=OUT_OF_RANGE )
			ERR("Error : You have data out of range(" << OUT_OF_RANGE << "), use a mask.\n");
		}

	};

#endif

