#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## crop.py
#Usage : python compute_mask.py filename width height galaxy DIR
#Description : Crop a fits file. The croped image is the 
#+             width*height rectangle in the center of the image
#Author : Mickaël FERRERI
#Date : June 24, 2019

import sys
import os
from astropy.io import fits as F


def trunc_file_name(filename):
	i = len(filename)-1
	while filename[i] != '/' and i > 0:
		i-=1
	return(filename[(i+1):-5])


def crop(img,width,heigth):
	n = len(img)
	m = len(img[0])
	return(img[int((n-width)/2):int((n+width)/2),int((m-heigth)/2):int((m+heigth)/2)])

#Read the arguments 
filename = sys.argv[1]
width = sys.argv[2]
heigth = sys.argv[3]
galaxy = sys.argv[4]
DIR=sys.argv[5]

hdulist = F.open(filename)
hdu = hdulist[-1]
img = hdu.data

img2 = crop(img,int(width),int(heigth))

hdu = F.PrimaryHDU(img2)
outputname = trunc_file_name(filename)+"_croped_"+width+"_"+heigth+".fits"
os.chdir(DIR+"/Data/"+galaxy+"/croped")
#If the file already exists, remove it
os.system("rm "+outputname+" 2>/dev/null")
#Write the file
hdu.writeto(outputname)
