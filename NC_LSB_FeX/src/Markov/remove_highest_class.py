import os
import sys
from astropy.io import fits
import math

## remove_highest_class.py
# Usage : python remove_highest_class.py segmention_map original_file previous_file output_file it
# Description : Computes the image in wich all the pixels that are nan in original_file and previous_file are set to nan 
# It also removes the pixels corresponding to the 2 highest class in the segmentation map
# All the other pixels are set to their original value
# Then it saves the result in output_file
# it is the number of the iteration (this is optional, and used in loop_marsiaa)

# Initialisation of the list which will contain the segmentation map and the original image
hdul = []

# Opening of the segmentation map
hdulist = fits.open(sys.argv[1])
# We take the last hdu, which contain the data
hdu = hdulist[-1]
# We read the data
hdul.append(hdu.data)

# Opening of the original image
hdulist2 = fits.open(sys.argv[2])
hdu2 = hdulist2[-1]
hdul.append(hdu2.data)
imgseg = hdul[0]

if (len(sys.argv)==6):
	it = int(sys.argv[5])
else:
	it = -1	


if it > 1:
	# Opening of the original image
	hdulist3 = fits.open(sys.argv[3])
	hdu3 = hdulist3[-1]
	hdul.append(hdu3.data)




n = len(hdul[1])
m = len(hdul[1][0])

# M is the highest class in the segmentation map
M =  max(map(max, imgseg))

# Initialisation of the 2d array whiwh will contain the output data 
data = [ [0 for j in range(m)] for i in range(n)]

# Print iterations srcress
def printsrcressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal srcress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

# Initial call to print 0% srcress
printsrcressBar(0, n, prefix = 'Progress:', suffix = 'Complete', length = 50)

for i in range(n):
	for j in range(m):
		# If we are in the first iteration, there is no previous file
		if it > 1:
			# Set the unwanted pixels to nan
			if math.isnan(hdul[1][i][j]) or math.isnan(hdul[2][i][j]) or hdul[0][i][j]>M-2:
				data[i][j] = float("nan")
			# We keep the initial value for the others
			else:
				data[i][j] = hdul[1][i][j]
		else:
			# Set the unwanted pixels to nan
			if math.isnan(hdul[1][i][j]) or hdul[0][i][j]>M-2:
				data[i][j] = float("nan")
			# We keep the initial value for the others
			else:
				data[i][j] = hdul[1][i][j]
	printsrcressBar(i, n, prefix = 'Progress:', suffix = 'Complete', length = 50)

print("")
print("")

# Creation of the output hdu
ret = fits.PrimaryHDU(data)

try:
	# If the output file already exists (from a previous execution e.g.) it will remove it to avoid an error
	os.system("rm "+sys.argv[4])
except:
	# If the file doesn't exists, we don't print the error output
	pass

# Writing the hdu in the output_file
ret.writeto(sys.argv[4])

print("Python : File writed in",sys.argv[4])



