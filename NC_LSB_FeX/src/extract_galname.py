#!/usr/bin/env python
# -*- coding: utf-8 -*-

## extract_galname.py
#Usage : python extract_galname.py filename
#Description : Extract the galaxy name of a file name (format : dir/galname.* )
#Author : Mickaël FERRERI
#Date : July 18, 2019

import sys

filename = sys.argv[1]

def search_gal(filename):
	i = len(filename)-1
	while filename[i] != '/' and i > 0:
		i-=1
	f = filename[(i+1):-5]
	i = 0
	while f[i] != '.' and i < len(f):
		i+=1
	return(f[:i])


f = search_gal(filename)
print(f)





