#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## resampling.py
#Usage : python resampling.py filename factor outputdir
#Description : Resamples the image "filename" by the given factor fac,
#+ i.e. cuts the image in fac*fac squares, takes the median value, and computes and saves the new image 
#Author : Mickaël FERRERI
#Date : June 24, 2019

import sys
import os
import math
from astropy.io import fits as F
import matplotlib.pyplot as plt
import numpy as np
import statistics as stat

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal srcress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()


def neighbour(data,fac,x,y):
	"""
	Gives the values and indexes of the neighbouring pixels of the point (x,y)
	@params:
	    data   -Required  : the image
	    fac    -Required  : the depth at which we consider a point as a neighbour
	    x      -Required  : the x coordinate of the point
	    y      -Required  : the y coordinate of the point
	"""
	l = []
	n = len(data)
	if fac == 5:
		l = [(x+i,y+j) for i in [-2,-1,0,1,2] for j in [-2,-1,0,1,2]]
	else:
		l = [(x+i,y+j) for i in [-1,0,1] for j in [-1,0,1]]
	lval = []
	lvois = []
	for i in range(len(l)):
		if l[i][0] >= 0 and l[i][0] < n and l[i][1] >= 0 and l[i][1] < len(data[x]):
			lval.append(data[l[i][0]][l[i][1]])
			lvois.append((l[i][0],l[i][1]))
	return((lval,lvois))


def rebin(img,fac):
	"""
	Compute the rebind image
	@params:
	    img   -Required  : the image
	    fac    -Required  : the rebinnig factor
	"""
	n = len(img)
	m = len(img[0])
	#Initialize the srcress bar
	printProgressBar(0, n, prefix = 'Progress:', suffix = 'Complete', length = 50)
	img2 = [[0 for i in range(m//fac)] for j in range(n//fac)]
	for i in range(fac-1,n,fac):
		printProgressBar(i, n, prefix = 'Progress:', suffix = 'Complete', length = 50)
		for j in range(fac-1,m,fac):
			l = neighbour(img,fac,i,j)[0]
			img2[i//fac][j//fac] = stat.median(l)
	return(img2)

def trunc_file_name(filename):
	"""
	Gives the basenale of a fits file without the ".fits"
	@params:
	    filename   -Required  : the name of the file
	"""
	i = len(filename)-1
	while filename[i] != '/' and i > 0:
		i-=1
	return(filename[(i+1):-5])

#Reading of the arguments
filename = sys.argv[1]
fac = int(sys.argv[2])
outputdir = sys.argv[3]

#Reading of the data in the filemname file
hdulist = F.open(filename)
hdu = hdulist[-1]
img = hdu.data

n = len(img)
m = len(img[0])

img2 = rebin(img,fac)

#Creation of the output image
hdu = F.PrimaryHDU(img2)
outputname = trunc_file_name(filename)+"_reduce.fits"

os.chdir(outputdir)
#If the output file already exists, remove it
os.system("rm "+outputname)
print(" ")
print(outputname)
#Write the output image
hdu.writeto(outputname)






