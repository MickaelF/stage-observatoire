#!/bin/bash

######################################
# exec_NoiseChisel
# Description : Execute NoiseChisel using a set of predefined parameters. 
#+              Can also compute a rsampled (rebined) image of the galaxy, 
#+              and a mask to hide the undetected regions
# Usage : ./exec_NoiseChisel -f filename -o outputname [-rebin] [-mode mode] 
#+        [-mask outputname] -g galaxy name
# Author : Mickaël FERRERI 
# Date : July 17, 2019
######################################


if [ "$1" == "-h" ]; then
  	echo -e "Usage: `basename $0` -f filename -o outputname [-rebin] [-mode mode] [-mask outputname] -g galaxy name"
	echo -e "\033[34;1;4mParameters : \033[0m"
	echo -e "\033[34;1m-f     :\033[0m the name of the input file. Mandatory argument"
	echo -e "\033[34;1m-o     :\033[0m the name of the output file. Mandatory argument"
	echo -e "\033[34;1m-rebin :\033[0m if this option is given, compute the rebined image"
	echo -e "\033[34;1m-mode  :\033[0m the name of the mode used, must be : tail, coq, inter or inter2. Default is inter2"
	echo -e "\033[34;1m-mask  :\033[0m give this option if you want the script to compute the masked images of the galaxy, given the detection done by NoiseChisel"
	echo -e "\033[34;1m-g     :\033[0m name of the galaxy"
  	exit 0
fi

filenamegiven=false
outputnamegiven=false
galnamegiven=false
maskgiven=false
rebin=false
modegiven=false
mode=inter2
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT="$( cd ~ >/dev/null 2>&1 && pwd )"

# Testing the arguments given
while test $# -gt 0
do
    case "$1" in
        -f) filenamegiven=true
	    filename=$2
            ;;
        -o) outputnamegiven=true
	    outputname=$2
            ;;
	-g) galnamegiven=true
	    galname=$2
	    ;;
	-rebin) rebin=true
	    ;;
	-mode) modegiven=true
	    mode=$2
	    ;;
	-mask) maskgiven=true
	     mask=$2
	    ;;
        -*) echo -e "Execution of `basename $0` aborted : Bad option error"
	    echo -e "Bad option $1, type `basename $0` -h"
	    exit 1
            ;;
        *) 
            ;;
    esac
    shift
done

# Filename, outputname and galname are mandatory arguments.
#+ The script will return an error and abort if these arguments are not given
if [ "$filenamegiven" != true ] || [ "$outputnamegiven" != true ] || [ "$galnamegiven" != true ]; then
	clear
	echo -e "Execution of `basename $0` aborted : Bad usage error"
	echo -e "Usage: `basename $0` -f filename -o outputname [-rebin] [-mode mode] [-mask outputname] -g galaxy name"
  	exit 1
fi

#Gives the value corresponding to the chosen mode
if [ $mode == "tail" ]; then
	tilesize="20,20"	
	meanmedqdiff="0.2"
	dthresh="0.98"
	detgrowquant="0.75"
	detgrowmaxholesize="100"
	qthresh="0.01"
	minnumfalse="20"
	erode="7"
	opening="4"
fi

if [ $mode == "coq" ]; then
	tilesize="50,50"	
	meanmedqdiff="0.2"
	dthresh="0.7"
	detgrowquant="0.99"
	detgrowmaxholesize="10"
	qthresh="0.7"
	minnumfalse="20"
	erode="1"
	opening="1"
fi


if [ $mode == "inter" ]; then
	tilesize="25,25"	
	meanmedqdiff="0.2"
	dthresh="0.85"
	detgrowquant="0.85"
	detgrowmaxholesize="100"
	qthresh="0.2"
	minnumfalse="20"
	erode="3"
	opening="2"
fi

if [ $mode == "inter2" ]; then
	tilesize="25,25"	
	meanmedqdiff="0.2"
	dthresh="0.9"
	detgrowquant="0.8"
	detgrowmaxholesize="100"
	qthresh="0.1"
	minnumfalse="20"
	erode="4"
	opening="2"
fi

tilesize=" --tilesize="$tilesize	
meanmedqdiff=" --meanmedqdiff="$meanmedqdiff
dthresh=" --dthresh="$dthresh
detgrowquant=" --detgrowquant="$detgrowquant
detgrowmaxholesize=" --detgrowmaxholesize="$detgrowmaxholesize
qthresh=" --qthresh="$qthresh
minnumfalse=" --minnumfalse="$minnumfalse
erode=" --erode="$erode
opening=" --opening="$opening

#If the option is given, compute the rebined file
if [ $rebin == true ]; then
	echo -e "\033[34mRebinning...\033[0m"
	rebin="python resampling.py "$filename" 20 "$galname
	eval $rebin
	echo -e "\033[34mRebinning : Done\033[0m"
fi

filenametrunc=${filename##*/}
filenametrunc=${filenametrunc%.fits*}"_reduce.fits"

filenametrunc2="$ROOT/Work/Data/"$galname"/rebin/"${filenametrunc%.fits*}"_arith.fits"

#After rebinning, we change the 0 to nan, because 0 are meaningful values, 
#+ and it may cause problems to the detection
if [ $rebin == true ]; then
	astarithmetic $ROOT/Work/Data/$galname/fits/$filenametrunc $ROOT/Work/Data/$galname/fits/$filenametrunc 0.0 eq nan where -g0 --output=$filenametrunc2
fi


eval "astarithmetic $ROOT/Work/Data/"$galname"/rebin/"$filenametrunc" $ROOT/Work/Data/"$galname"/rebin/"$filenametrunc" 0.0 eq nan where -g0 --output="${filenametrunc%.fits*}"_arith.fits"
mv $CUR_DIR/../${filenametrunc%.fits*}"_arith.fits" "$ROOT/Work/Data/"$galname"/rebin/"

#Executing NoiseChisel
nc="astnoisechisel "$filenametrunc2" "$tilesize" "$meanmedqdiff" "$dthresh" "$detgrowquant" "$detgrowmaxholesize" "$qthresh" "$minnumfalse" "$erode" "$opening" --output="$outputname
eval $nc

#If the option is given, compute the mask (the original image without the sky)
if [ $maskgiven == true ]; then
	echo -e "\033[34mMask computation...\033[0m"
	astarithmetic $outputname $outputname not nan where -hINPUT-NO-SKY -hDETECTIONS --output=$mask
	echo -e "\033[34mMask : Done\033[0m"
fi


